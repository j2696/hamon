﻿/*
* uds_updown.c - contains uds upload download functions
*
*-------------------------------------------------------------------
*
*-------------------------------------------------------------------
*
*
*/

/********************************************************************/
/**
* \file uds_updown.c
* \brief main uds routines
*
*/

/* header of standard C - libraries
-----------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <gen_define.h>
#include <co_datatype.h>
#include <co_timer.h>
#include <iso_tp.h>

#include <uds_datatype.h>
#include <uds_uds.h>
#include <uds_indication.h>

#include "iuds_updown.h"
#include "iuds_core.h"
#include "iuds_comm.h"

/* header of project specific types
-----------------------------------------------------------------------------*/

/* constant definitions
-----------------------------------------------------------------------------*/
#ifndef UDS_MEMORYSIZE_PARA_MIN_SIZE
# define  UDS_MEMORYSIZE_PARA_MIN_SIZE 2u
#endif /* UDS_MEMORYSIZE_PARA_MIN_SIZE */

#ifndef UDS_MEMORYSIZE_PARA_MAX_SIZE
# define  UDS_MEMORYSIZE_PARA_MAX_SIZE 4u
#endif /* UDS_MEMORYSIZE_PARA_MAX_SIZE */

#ifndef UDS_ADDRSIZE_PARA_MIN_SIZE
# define  UDS_ADDRSIZE_PARA_MIN_SIZE 2u
#endif /* UDS_ADDRSIZE_PARA_MIN_SIZE */

#ifndef UDS_ADDRSIZE_PARA_MAX_SIZE
# define  UDS_ADDRSIZE_PARA_MAX_SIZE 4u
#endif /* UDS_ADDRSIZE_PARA_MAX_SIZE */

#ifndef UDS_MAX_DATA_BLOCK_SIZE
# define UDS_MAX_DATA_BLOCK_SIZE (2 * 1024)
#endif /* UDS_MAX_DATA_BLOCK_SIZE */

#if (UDS_MAX_DATA_BLOCK_SIZE <= 255)
# define UDS_BLOCKLENGTH_IDENT (uint8_t)1u
#endif /* (UDS_MAX_DATA_BLOCK_SIZE <= 255) */

#if (UDS_MAX_DATA_BLOCK_SIZE <= 65535)
# define UDS_BLOCKLENGTH_IDENT (uint8_t)2u
#endif /* (UDS_MAX_DATA_BLOCK_SIZE < 65535) */

#ifndef UDS_BLOCKLENGTH_IDENT
# define UDS_BLOCKLENGTH_IDENT (uint8_t)4u
#endif /* UDS_BLOCKLENGTH_IDENT */

/* local defined data types
-----------------------------------------------------------------------------*/
typedef enum  {
    UDS_UPDOWN_STATE_NONE = 0,
    UDS_UPDOWN_STATE_DOWNLOAD,
    UDS_UPDOWN_STATE_UPLOAD
} UDS_UPDOWNSATE_T;

/* list of external used functions, if not in headers
-----------------------------------------------------------------------------*/

/* list of global defined functions
-----------------------------------------------------------------------------*/

/* list of local defined functions
-----------------------------------------------------------------------------*/
static RET_T iudsUpDownDecodeAddr(uint8_t addr, uint8_t *pData, uint16_t dataSize);
static RET_T iudsUpDownEncodeBlock(uint8_t *pData, uint16_t *pDataSize);
static RET_T downloadData(
                uint8_t addr,
                uint8_t *pData,
                uint16_t *dataSize
        );
static RET_T iudsUpDownCheckUserInd(
		uint8_t srcAddr,
		UDS_DATA_REQ_TYPE_T transType,
		uint8_t dataFormat,
		uint32_t memoryAddress,
		uint32_t memorySize,
		uint8_t *pData,
		uint16_t size
	);

/* external variables
-----------------------------------------------------------------------------*/

/* global variables
-----------------------------------------------------------------------------*/

/* local defined variables
-----------------------------------------------------------------------------*/
uint8_t addrSizePara;  //$klg> original : static uint8_t addrSizePara;
uint8_t memorySizePara;  //$klg> original : static uint8_t memorySizePara;
uint32_t bak_memoryAddress = 0U;  //$klg> @ @ @ @ @
//uint32_t bak_memorySize = 0U;  //$klg> @ @ @ @ @

static uint32_t memoryAddress;
static uint32_t memorySize;

static uint8_t dataFormat;

static uint8_t blkSeqCounter = 1;

static UDS_EVENT_DATA_TRANSFER_T dataTransferUserInd = NULL;

/*****************************************************************************/
/*****************************************************************************/
RET_T iudsUpDownMsgHandler(
        uint8_t addr,
        uint8_t* pData,
        uint16_t dataSize
    )
{
RET_T retVal;
const uint8_t reqSessionTypes[1] = { UDS_SUB_SESSION_PRGS };
const uint8_t reqSecurity[5] = {UDS_SEC_ACCESS_KEY_LVL_1, UDS_SEC_ACCESS_KEY_LVL_2, UDS_SEC_ACCESS_KEY_LVL_3, UDS_SEC_ACCESS_KEY_LVL_4, UDS_SEC_ACCESS_KEY_LVL_5};

    if (iudsCheckRequiredSession(UDS_REQ_DOWNLOAD, &reqSessionTypes[0], 1) == CO_FALSE)  {
        iudsSendAbort(addr, pData,UDS_NRC_CNC, 0);
        return (RET_OK);
    }

    if (iudsCheckSecurityLevel(reqSecurity, 5) == CO_FALSE)  {
        iudsSendAbort(addr, pData,UDS_NRC_SAD, 0);
        return (RET_OK);
    }

    switch (pData[0u])  {
    /* request download */
    case UDS_REQ_DOWNLOAD:


        retVal = iudsUpDownDecodeAddr(addr, pData, dataSize);
        if (retVal != RET_OK)  {
            return (retVal);
        }

        iudsUpDownCheckUserInd(addr, UDS_REQUEST_WRITE, dataFormat, memoryAddress, memorySize, pData, dataSize);

        (void)iudsUpDownEncodeBlock(pData, &dataSize);

        blkSeqCounter = 1;

        break;

    /* request upload */
    case UDS_REQ_UPLOAD:

        retVal = iudsUpDownDecodeAddr(addr, pData, dataSize);
        if (retVal != RET_OK)  {
            return (retVal);
        }

        (void)iudsUpDownEncodeBlock(pData, &dataSize);

        break;

        /* transfer download */
    case UDS_TRANSFER_DOWNLOAD:
        retVal = downloadData(addr, pData, &dataSize);
        if (retVal != RET_OK)  {
            return (retVal);
        }
        break;

    case UDS_REQ_TRANSFER_EXIT:

		iudsUpDownCheckUserInd(addr, UDS_TRANSFER_EXIT, dataFormat, memoryAddress, memorySize, pData, dataSize);

        pData[0] = UDS_RESP_TRANSFER_EXIT;
        break;

    default:
        break;
    }

    iudsSendResponse(addr, pData, dataSize, 0u);

    return (RET_OK);
}


/*****************************************************************************/
static RET_T iudsUpDownDecodeAddr(
        uint8_t	addr,
        uint8_t	*pData,
        uint16_t	dataSize
    )
{
uint8_t cnt;

    /* test for minimal message lenght */
    if (dataSize < 5u)  {
        (void)iudsSendAbort(addr, pData, UDS_NRC_IMLOIF, 0u);
        return (RET_ABORTED);
    }

    /* extract data format */
    dataFormat = pData[1];

    /* extract parameter size */
    addrSizePara = (pData[2u] & 0x0f);
    memorySizePara = (pData[2u] >> 4u) & 0x0f;

    /* check addr parameter size */
    if ((addrSizePara > UDS_ADDRSIZE_PARA_MAX_SIZE)
     || (addrSizePara < UDS_ADDRSIZE_PARA_MIN_SIZE))  {

        (void)iudsSendAbort(addr, pData, UDS_NRC_ROOR, 0u);
        return (RET_ABORTED);
    }

    /* check memory parameter size */
    if ((memorySizePara > UDS_MEMORYSIZE_PARA_MAX_SIZE)
     || (memorySizePara < UDS_MEMORYSIZE_PARA_MIN_SIZE))  {

        (void)iudsSendAbort(addr, pData, UDS_NRC_ROOR, 0u);
        return (RET_ABORTED);
    }

    /* test for correct message lenght */
    if ((3u + +addrSizePara + memorySizePara) != dataSize)  {
        (void)iudsSendAbort(addr, pData, UDS_NRC_IMLOIF, 0u);
        return (RET_ABORTED);
    }

    /* extract parameter */
    memoryAddress = 0u;
    for (cnt = 0u; cnt < addrSizePara; cnt++)  {
        memoryAddress <<= 8u;
        memoryAddress += pData[3u + cnt];
    }
    bak_memoryAddress = memoryAddress;  //$klg>

    memorySize = 0u;
    for (cnt = 0u; cnt < memorySizePara; cnt++)  {
        memorySize <<= 8u;
        memorySize += pData[3u + addrSizePara + cnt];
    }
    //bak_memorySize = memorySize;  //$klg>

    return (RET_OK);
}


/*****************************************************************************/
static RET_T iudsUpDownEncodeBlock(
        uint8_t* pData,
        uint16_t* pDataSize
    )
{
    pData[1u] = (UDS_BLOCKLENGTH_IDENT << 4u);

    pData[1u + UDS_BLOCKLENGTH_IDENT] = (uint8_t)(UDS_MAX_DATA_BLOCK_SIZE & 0xff);
    if (UDS_BLOCKLENGTH_IDENT > 1u)  {
        pData[1u + UDS_BLOCKLENGTH_IDENT - 1u] = (uint8_t)((UDS_MAX_DATA_BLOCK_SIZE >> 8u) & 0xff);
    }
    if (UDS_BLOCKLENGTH_IDENT > 2u)  {
        pData[1u + UDS_BLOCKLENGTH_IDENT - 2u] = (uint8_t)((UDS_MAX_DATA_BLOCK_SIZE >> 16u) & 0xff);
    }
    if (UDS_BLOCKLENGTH_IDENT > 3u)  {
        pData[1u + UDS_BLOCKLENGTH_IDENT - 3u] = (uint8_t)((UDS_MAX_DATA_BLOCK_SIZE >> 24u) & 0xff);
    }

    *pDataSize = 2u + UDS_BLOCKLENGTH_IDENT;

    return (RET_OK);
}



/*****************************************************************************/
static RET_T downloadData(
                uint8_t addr,
                uint8_t *pData,
                uint16_t *dataSize
        )
{
RET_T retVal;

    /* check sequence counter */
    if (pData[1] != blkSeqCounter)  {
        (void)iudsSendAbort(addr, pData, UDS_NRC_WBSC, 0u);
        return (RET_ABORTED);
    }

    retVal = iudsUpDownCheckUserInd(addr, UDS_TRANSFER_WRITE, 0, 0, 0, pData, *dataSize);

	if (retVal == RET_OK)  {
    	blkSeqCounter++;
    	*dataSize = 2;
	}
    return (retVal);
}


static RET_T iudsUpDownCheckUserInd(
		uint8_t srcAddr,
		UDS_DATA_REQ_TYPE_T transType,
		uint8_t dataFormat,
		uint32_t memoryAddress,
		uint32_t memorySize,
		uint8_t *pData,
		uint16_t size
	)
{
RET_T retReason;
uint8_t *pLocalData;
uint16_t localSize;

	pLocalData = pData;
	localSize = size;

	if (transType == UDS_TRANSFER_WRITE)  {
		pLocalData += 2;
		localSize -= 2u;
	}

	if (transType == UDS_REQUEST_WRITE)  {
		pLocalData = NULL;
		localSize = 0u;
	}

    if (dataTransferUserInd != NULL)  {
        retReason = dataTransferUserInd(srcAddr, transType, dataFormat, memoryAddress, memorySize, pLocalData, localSize);
        if (retReason != RET_OK)  {
			iudsSendRetAbort(srcAddr, pData, retReason, 0u);
            return (RET_ABORTED);
        }
    }
	return (RET_OK);
}

RET_T udsDecodeAddress(
		uint8_t *pData,
		uint16_t dataSize,
		uint32_t *pAddress,
		uint32_t *pSize
	)
{
RET_T retVal;

	retVal = iudsUpDownDecodeAddr(0, pData, dataSize);
	if (retVal == RET_OK)  {
		*pAddress = memoryAddress;
		*pSize =  memorySize;
	}

	return (retVal);
}

/*****************************************************************************/
RET_T udsEventRegister_UDS_DATA_TRANSFER(
                UDS_EVENT_DATA_TRANSFER_T pFct
        )
{
    dataTransferUserInd = pFct;

    return (RET_OK);
}
