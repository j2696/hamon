/*
* bl_application.c
*
*-------------------------------------------------------------------
*
*-------------------------------------------------------------------
*
*
*/

/********************************************************************/
/**
* \file
* \brief bl_application.c - application related parts
*
* application related parts
*/



/* header of standard C - libraries
---------------------------------------------------------------------------*/
#include <stdio.h>

/* header of project specific types
---------------------------------------------------------------------------*/
#include <bl_config.h>
#include <bl_type.h>
#include <bl_application.h>
#include <bl_crc.h>
#include <bl_flash.h>
#include <bl_call.h>


/* constant definitions
---------------------------------------------------------------------------*/

/* local defined data types
---------------------------------------------------------------------------*/
/** Config Block address */
#define CONFIG_BLOCK_ADDRESS(x) FLASH_APPL_START((x))
/* Config Block size - moved to bl_config.h */
//#define CFG_CONFIG_BLOCK_SIZE	256

/* list of external used functions, if not in headers
---------------------------------------------------------------------------*/

/* list of global defined functions
---------------------------------------------------------------------------*/

/* list of local defined functions
---------------------------------------------------------------------------*/

/* external variables
---------------------------------------------------------------------------*/

/* global variables
---------------------------------------------------------------------------*/

/* local defined variables
---------------------------------------------------------------------------*/

/********************************************************************/
/**
 * \brief first flash address of the application
 * Often it is the address after the configuration block.
 *
 * \retval
 * first address of the application within the flash (Begin of the Vector table)
 */
FlashAddr_t applFlashStart(uint8_t nbr)
{
    (void)nbr; /* Remove Warning */

    FlashAddr_t address;

	address = FLASH_APPL_START(nbr) + CFG_CONFIG_BLOCK_SIZE(nbr); //$klg> 0x0800F800 + 0x800
	return (address);
}

/********************************************************************/
/**
 * \brief first flash address after the application
 *
 * \retval
 * address after last address of the application within the Flash
 */
FlashAddr_t applFlashEnd(uint8_t nbr)
{
const ConfigBlock_t * pConfig = applConfigBlock(nbr);
FlashAddr_t address;

	address = applFlashStart(nbr) + BL_REVERSE_U32(pConfig->applSize);
	return (address);
}

/********************************************************************/
/**
 * \brief checksum saved in flash
 *
 * \retval
 * 	saved checksum
 */
uint16_t applChecksum(uint8_t nbr)
{
const ConfigBlock_t * pConfig = applConfigBlock(nbr);
uint16_t retval;

	retval = BL_REVERSE_U16(pConfig->crcSum);
	return (retval);
}

/********************************************************************/
/**
* \brief check some general setting
 *
 * Check for a correct flashed application.
 * The function check also that the bootloader configuration make sense.
 *
 * \retval BL_STATE_OK
 * configuration seems to be ok
 * \retval BL_RET_ERROR
 * wrong configuration in application flash or bootloader configuration
 *
 */
BlRet_t applCheckConfiguration(uint8_t nbr)
{
FlashAddr_t flashStart = FLASH_APPL_START(nbr);
FlashAddr_t flashEnd   = FLASH_APPL_END(nbr);
FlashAddr_t configBlockStart = CONFIG_BLOCK_ADDRESS(nbr);
uint32_t maxApplSize = FLASH_APPL_END(nbr) - FLASH_APPL_START(nbr) + 1 - CFG_CONFIG_BLOCK_SIZE(nbr);
const ConfigBlock_t * pConfig = applConfigBlock(nbr);

#ifdef BL_ECC_FLASH
	if (bl_command[ECC_ERROR_IDX] == ECC_ERROR_VAL)  {
		return (BL_RET_ERROR);
	}
#endif

	if (configBlockStart < flashStart)  {
		return (BL_RET_ERROR);
	}

	if (configBlockStart > (flashEnd - CFG_CONFIG_BLOCK_SIZE(nbr)) )  {
		return (BL_RET_ERROR);
	}

	if (BL_REVERSE_U32(pConfig->applSize) > maxApplSize)  {
		return (BL_RET_ERROR);
	}

	if (BL_REVERSE_U32(pConfig->applSize) == 0)  {
		return (BL_RET_ERROR);
	}

	if (BL_REVERSE_U16(pConfig->crcSum) == 0)  {
		return (BL_RET_ERROR);
	}

	return (BL_RET_OK);
}


/******************************************************************************
* \brief applCheckChecksum - check the CRC sum of the application in flash
*
* \retval BL_RET_OK  checksum correct
*
* \retval BL_RET_ERROR  configuration wrong, e.g. no application
*
* \retval BL_RET_CRC_WRONG  error in flash
*
*/
BlRet_t applCheckChecksum(uint8_t nbr)
{
    BlRet_t ret;
    FlashAddr_t applStart;
    uint16_t crcVal;
    const ConfigBlock_t * pConfig = applConfigBlock(nbr);

    ret = applCheckConfiguration(nbr);
    if (ret != BL_RET_OK)  {
        return (ret);
    }

    applStart = applFlashStart(nbr);

    crcInitCalculation();
    crcVal = crcCalculation((const uint8_t *)applStart, CRC_START_VALUE, BL_REVERSE_U32(pConfig->applSize));
    if (crcVal != BL_REVERSE_U16(pConfig->crcSum))  {
        return (BL_RET_CRC_WRONG);
    }

    return (BL_RET_OK);
}


/******************************************************************************
* \brief applCheckCRC32 - check the CRC32 sum of the application in flash
*
* \retval BL_RET_OK  checksum correct
*
* \retval BL_RET_ERROR  configuration wrong, e.g. no application
*
* \retval BL_RET_CRC_WRONG  error in flash
*
*/
BlRet_t applCheckCRC32(uint8_t nbr, uint32_t crcSum32, uint32_t *calcSum32)
{
    BlRet_t ret;
    FlashAddr_t applStart;
    const ConfigBlock_t * pConfig = applConfigBlock(nbr);

    ret = applCheckConfiguration(nbr);
    if (ret != BL_RET_OK)  {
        return (ret);
    }

    applStart = applFlashStart(nbr);

    *calcSum32 = crc32Calculation((const uint8_t *)applStart, CRC32_START_VALUE, BL_REVERSE_U32(pConfig->applSize));
    if (*calcSum32 != crcSum32)  {
        return (BL_RET_CRC_WRONG);
    }

    return (BL_RET_OK);
}


#ifdef BL_CHECK_PRODUCTID
#  ifdef COBL_OD_1018_1_FCT
#  else
#error "user function required - COBL_OD_1018_1_FCT"
#  endif
#  ifdef COBL_OD_1018_2_FCT
#  else
#error "user function required - COBL_OD_1018_2_FCT"
#  endif

/********************************************************************/
/**
* \brief check 0x1018 parameter, if available
*
* \retval BL_RET_OK
* Image parameter ok
*
* \retval BL_RET_ERROR
* configuration wrong, e.g. no application
*
* \retval BL_RET_IMAGE_WRONG
* wrong image parameter
*/
BlRet_t applCheckImage(void)
{
BlRet_t ret;
const ConfigBlock_t * pConfig;
uint32_t val1018_1;
uint32_t val1018_2;

	ret = applCheckConfiguration(0);
	if (ret != BL_RET_OK)  {
		return (ret);
	}

	//ret = BL_RET_OK;
	val1018_1 = userOd1018_1();
	val1018_2 = userOd1018_2();

	pConfig = applConfigBlock(0);
	if ((BL_REVERSE_U32(pConfig->od1018Vendor) != val1018_1)
		|| (BL_REVERSE_U32(pConfig->od1018Product) != val1018_2))
	{
		ret = BL_RET_IMAGE_WRONG;
	}

	return (ret);
}

#endif /* BL_CHECK_PRODUCTID */

/********************************************************************/
const ConfigBlock_t * applConfigBlock(uint8_t nbr)
{
    (void)nbr; /* Remove Warning */

    ConfigBlock_t * pConfig = (ConfigBlock_t *)CONFIG_BLOCK_ADDRESS(nbr);

	return (pConfig);
}
