/*
* cobl_timer.c
*
*-------------------------------------------------------------------
*
*-------------------------------------------------------------------
*
*
*/

/********************************************************************/
/**
* \file
* \brief cobl_timer.c - Timer functionality
*
*/



/* header of standard C - libraries
---------------------------------------------------------------------------*/
#include <time.h>
#include <stddef.h>

/* header of project specific types
---------------------------------------------------------------------------*/
#include <bl_config.h>
#include <bl_type.h>
#include <bl_timer.h>

/* hardware header
---------------------------------------------------------------------------*/
#include <stm32g0xx.h>
#include <stm32g0xx_hal_rcc.h>

/* constant definitions
---------------------------------------------------------------------------*/

/* local defined data types
---------------------------------------------------------------------------*/

/* list of external used functions, if not in headers
---------------------------------------------------------------------------*/

/* list of global defined functions
---------------------------------------------------------------------------*/

/* list of local defined functions
---------------------------------------------------------------------------*/

/* external variables
---------------------------------------------------------------------------*/

/* global variables
---------------------------------------------------------------------------*/

/* local defined variables
---------------------------------------------------------------------------*/

/***************************************************************************/
/**
 * \brief initialialize the cyclic timer
 *
 * Note: This function is also used during the reinitialization of the device.
 */
void timerInit(void)
{
const uint32_t timerInterval = 1000ul; /* 1ms => 200x == 2s */

	#ifdef SysTick_CTRL_CLKSOURCE_Msk
		/* ST Peripherie Library v1 */

		// part of SysTick_Config() - without interrupts
		SysTick->CTRL  = SysTick_CTRL_CLKSOURCE_Msk;  //disable timer, AHB clock

		SysTick->LOAD  = (((HAL_RCC_GetHCLKFreq()/(1000ul*1000ul)) * timerInterval) & SysTick_LOAD_RELOAD_Msk) - 1;      /* set reload register */

		SysTick->VAL   = 0u;

		SysTick->CTRL  = SysTick_CTRL_CLKSOURCE_Msk |
						SysTick_CTRL_ENABLE_Msk;     // enable timer,  AHB clock
	#else /* SysTick_CTRL_CLKSOURCE_Msk */
		/*
		* disable Timer
		* clock = AHB
		* no exception
		*/
		SysTick->CTRL  = SysTick_CTRL_CLKSOURCE;

		/* set reload register */
		SysTick->LOAD  = (((HAL_RCC_GetHCLKFreq()/(1000ul*1000ul)) * timerInterval) & SysTick_LOAD_RELOAD) - 1;

		SysTick->VAL   = 0u;

		/*
		 * enable timer
		 * clock = AHB
		 */
		SysTick->CTRL  = SysTick_CTRL_CLKSOURCE |
						SysTick_CTRL_ENABLE;

	#endif /* SysTick_CTRL_CLKSOURCE_Msk */
}

/***************************************************************************/
/**
 * \brief check for the end of a period
 *
 * Note: It is possible, that more than one period is over.
 *
 *
 */
uint8_t timerTimeExpired(
		uint16_t timeVal /**< ms */
	)
{
static uint16_t periodCnt = 0u;

#ifdef SysTick_CTRL_CLKSOURCE_Msk
	/* read access reset count flag! */
	if ((SysTick->CTRL & SysTick_CTRL_COUNTFLAG_Msk) != 0u)  {
		periodCnt++;
	}

#else /* SysTick_CTRL_CLKSOURCE_Msk */
	/* read access reset count flag! */
	if ((SysTick->CTRL & SysTick_CTRL_COUNTFLAG) != 0u)  {
		periodCnt++;
	}

#endif /* SysTick_CTRL_CLKSOURCE_Msk */

	if (periodCnt >= (timeVal))  {
		periodCnt = 0u;
		return (1u); /* period is over */
	}

	return (0u);
}

/***************************************************************************/
/**
* \brief wait a specific time
*
* This function wait for 100ms.
*
*/
void timerWait100ms(void)
{
	timerInit();

	while(timerTimeExpired(100) == 0) {
		;
	}

	/* 100ms later ... */
	/* counter flag was reset on the last read! */
}
