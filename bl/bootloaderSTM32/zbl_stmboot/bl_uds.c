﻿/*
* bl_uds.c - uds routines
*
*-----------------------------------------------------------------------------
*
*-----------------------------------------------------------------------------
*
*/

/******************************************************************************
* \file
* \brief uds bootloader routines
*/

/* header of standard C - libraries
-----------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* header of project specific types
-----------------------------------------------------------------------------*/
/* boot loader */
#include <bl_config.h>
#include <bl_call.h>
#include <bl_timer.h>
#include <bl_flash.h>
#include <bl_flash_lowlvl.h>
#include <bl_application.h>
#include <bl_main.h>
#include <bl_uds.h>
#include <bl_user.h>

/* UDS */
#include <gen_define.h>
#include <co_commtask.h>
#include <co_drv.h>
#include <co_timer.h>
#include <ico_cobhandler.h>
#include <ico_queue.h>
#include <ico_commtask.h>
#include <iso_tp.h>
#include <uds_uds.h>
#include <uds_indication.h>

/* HAL */
#include <stm32g0xx.h>
#include <stm32g0xx_it.h>

/* constant definitions
-----------------------------------------------------------------------------*/
CO_CONST uint16_t recQueueCnt = CO_REC_BUFFER_COUNTS;
CO_CONST uint16_t trQueueCnt = CO_TR_BUFFER_COUNTS;

/* local defined data types
-----------------------------------------------------------------------------*/

/* list of external used functions, if not in headers
-----------------------------------------------------------------------------*/

/* list of global defined functions
-----------------------------------------------------------------------------*/

/* list of local defined functions
-----------------------------------------------------------------------------*/
static RET_T readWriteIdentifierInd (
        uint8_t   srcAddr,
        uint8_t   rw,
        uint16_t  identifier,
        uint8_t  *pData,
        uint16_t *pDataLen          )   ;

static RET_T udsCommManagementInd (
        BOOL_T    execute,
        uint8_t   srcAddr,
        uint8_t   reqType,
        uint8_t   subFct,
        uint8_t  *pData,
        uint16_t  dataLen         )     ;

static RET_T dataDownloadInd (
        uint8_t    srcAddr,
        UDS_DATA_REQ_TYPE_T  transType,
        uint8_t    dataFormat,
        uint32_t   memoryAddress,
        uint32_t   memorySize,
        uint8_t   *pData,
        uint16_t   size      )          ;

static RET_T secAccessReqSeed (
        uint8_t   srcAddr,
        uint8_t   accessType,
        uint8_t  *pKey,
        uint16_t *pKeyLen     )         ;

static RET_T routineControl (
        uint8_t   srcAddr,
        uint8_t   ctrltype,
        uint16_t  identifier,
        uint8_t  *pData,
        uint16_t *pDataSize )           ;

static void checkSecurityAccess (
        void                    )       ;

static RET_T isSecurityAccessUnlocked (
        void                          ) ;

/* external variables
-----------------------------------------------------------------------------*/

/* global variables
-----------------------------------------------------------------------------*/
uint8_t tmpFingerPrint[APPL_FINGERPRINT_LENGTH] = { 0u, };
uint8_t tmpFingerPrintLength = FLASH_EMPTY;
static BOOL_T securityAccessUnlocked = CO_FALSE;
uint8_t sharedm_ECU_HW_PN[BL_SHAREDM_LEN_ECU_HW_PN] = {0};
uint8_t sharedm_ECU_SN[BL_SHAREDM_LEN_ECU_SN] = {0};
uint8_t sharedm_ECU_HW_VERSION[BL_SHAREDM_LEN_ECU_HW_VERSION] = {0};
uint8_t sharedm_ECU_MainPartNb[BL_SHAREDM_LEN_ECU_MainPartNb] = {0};
uint8_t sharedm_ECU_CHECKSUM[BL_SHAREDM_LEN_ECU_CHECKSUM] = {0};

/* local defined variables
-----------------------------------------------------------------------------*/

static uint8_t udsCurrentSession = UDS_SUB_SESSION_DS;

static uint8_t savedSecurityLevel;
static uint8_t u32_udsSavedKey[UDS_SECURITY_SEED_KEY_LEN] = { 0xBA, 0xDC, 0x0D, 0xED };
static BOOL_T bFlashAfterResetTimeout = CO_FALSE;
static BOOL_T bUds_F5_18_Requested = CO_FALSE;
static uint8_t fblping_cnt = 0U;

/******************************************************************************
* \brief udsInit - init uds module
*
*/
BlRet_t udsInit (
        void            )
{
    /* This function initializes the CPU, the CAN Hardware
     * and the GPIOs needed for CAN.
     * Then it initializes the UDS communication stack,
     * registers useful UDS application indications,
     * that allow to react to certain UDS service requests */

    /* initialize hardware / clock / GPIO pins */
    codrvHardwareInit();

    /* initialize CAN controller */
    if (codrvCanInit(250u) != RET_OK) {
        return (BL_RET_ERROR);
    }

    timerInit();

    /* initialize dependencies */
    coTimerInit(CO_TIMER_INTERVAL);
    icoCommTaskVarInit();
    icoQueueVarInit(&recQueueCnt, &trQueueCnt);
    icoCobHandlerVarInit();
    coQueueInit();

    /* initialize UDS stack */
    if (udsStackInit() != RET_OK) {
        return (BL_RET_ERROR);
    }

    /* register event functions */
    if (udsEventRegister_UDS_REQ_COMMGT(udsCommManagementInd) != RET_OK) {
        return (BL_RET_ERROR);
    }
    if (udsEventRegister_UDS_DATA_TRANSFER(dataDownloadInd) != RET_OK) {
        return (BL_RET_ERROR);
    }
    if (udsEventRegister_UDS_READ_WRITE_ID(readWriteIdentifierInd) != RET_OK) {
        return (BL_RET_ERROR);
    }
    if (udsEventRegister_UDS_SECURITY_ACCESS_REQ_KEY(secAccessReqSeed) != RET_OK) {
        return (BL_RET_ERROR);
    }
    if (udsEventRegister_UDS_ROUTINE_CTRL(routineControl) != RET_OK) {
        return (BL_RET_ERROR);
    }

    /* set the requested diagnostic session type
    * saved by the application in bl_command[],
    * before jumping back to the boot loader */
    switch (bl_command[COMMAND_IDX_UDS_SES]) {
    case UDS_SUB_SESSION_DS:
        /* fall through */
    case UDS_SUB_SESSION_PRGS:
        /* fall through */
    case UDS_SUB_SESSION_EXTDS:
        /* fall through */
    case UDS_SUB_SESSION_SSDS:
        /* valid diagnostic session type
        * switch UDS session */
        udsCommSetDiagnosticSessionType(bl_command[COMMAND_IDX_UDS_SES]);
        udsCurrentSession = bl_command[COMMAND_IDX_UDS_SES];
        break;
    default:
        break;
    }

    /* enable CAN communication */
    if (codrvCanEnable() != RET_OK) {
        return (BL_RET_ERROR);
    }

    return (BL_RET_OK);
}


/******************************************************************************
* \brief udsSetFARstatus     -
*
*/
void udsSetFARstatus(BOOL_T bVal)
{
    bFlashAfterResetTimeout = bVal;
}

/******************************************************************************
* \brief udsIsFARTimeoutElapsed     -
*
*/
BOOL_T udsIsFARtimeElapsed(void)
{
    return bFlashAfterResetTimeout;
}



/******************************************************************************
* \brief udsIsFblPingTimeoutElapsed     -
*
*/
BOOL_T udsIsFblPingTimeoutElapsed(void)
{
    return fblping_cnt > 1000U ? CO_TRUE : CO_FALSE;
}

/******************************************************************************
* \brief udsResetFblPingTimeout     -
*
*/
static void udsResetFblPingTimeout(void)
{
    fblping_cnt = 0U;
}





/******************************************************************************
* \brief udsCyclic -
*
*/
void udsCyclic(void)
{
    /* UDS timer */
    if (timerTimeExpired(CO_TIMER_INTERVAL/1000) == 1) {
        
        fblping_cnt++;

        static uint16_t udsCyclic_Counter_Wd_Ext = 0U;

        if (udsCyclic_Counter_Wd_Ext++ == WDEXT_TIMEOUT) {
            udsCyclic_Counter_Wd_Ext = 0U;
            HAL_GPIO_TogglePin(GPIOC, GPIO_PIN_1);
        }

        static uint8_t udsCyclic_CountFAR = 0U;

        if (udsCyclic_CountFAR < FLASH_AFTER_RST_WINDOW) {
            udsCyclic_CountFAR++;
            if (udsCyclic_CountFAR == FLASH_AFTER_RST_WINDOW) {
                /* udsSetFARstatus depends on the UDS_RID_FLASH_AFTER_RESET service request */
                udsSetFARstatus(bUds_F5_18_Requested == CO_TRUE ? CO_FALSE : CO_TRUE);
            }
        }

        coTimerTick();
    }

    /* UDS handler */
    while (coCommTask() == CO_TRUE) {
        /* more things to do
         * call coCommTask() again... */
    };

    /* monitor security access */
    checkSecurityAccess();
}


/******************************************************************************
* \brief readWriteIdentifierInd -
*
*/
static RET_T readWriteIdentifierInd (
        uint8_t   srcAddr,
        uint8_t   rw,
        uint16_t  identifier,
        uint8_t  *pData,
        uint16_t *pDataLen                  )
{
    (void)srcAddr; /* Remove Warning */

    RET_T            retVal = RET_OK;
    static uint8_t   opMode = 0;
    ConfigBlock_t   *pConfig = (ConfigBlock_t *)FLASH_APPL_START(0);
    uint8_t          u8str[] = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ";
    uint16_t         offset = 0U;
    volatile uint8_t *bl_sharedm = (uint8_t *) 0x20023EE0;

    /* This function handles read/write by identifier requests.
     * Supported identifiers have to be added and handled here.
     * Some example identifiers are already implemented. */

    /* read identifier */
    if (rw == 0) {
        switch (identifier) {

        case 0xf100:    /* ECU pin */
            pData[0U] = 0x02;
            pData[1U] = 0x20;
            pData[2U] = 0x01;
            pData[3U] = 0x56;
            *pDataLen = 4;
            break;
            
        case 0xf103:    // New identifier definition after support from Claas 
            pData[0U] = 0x01;
            pData[1U] = 0x00;
            *pDataLen = 2;
            break;

        case 0xf105:    /* Supplier code */
            pData[0U] = 0x00;
            pData[1U] = 0x88;
            pData[2U] = 0x49;
            *pDataLen = 3;
            break;

        case 0xf180:    // read boot software identification 
            pData[0U] = 0x02;
            pData[1U] = 0x00;
            *pDataLen = 2;
            break;

        case 0xf191:    // read 4 bytes : F191 
            //memcpy(pData, sharedm_ECU_HW_PN, BL_SHAREDM_LEN_ECU_HW_PN);
            pData[0U] = 0;
            pData[1U] = 0;
            pData[2U] = 0;
            pData[3U] = 0;
            pData[4U] = bl_sharedm[BL_SHAREDM_OFFSET_ECU_HW_PN + 0U];
            pData[5U] = bl_sharedm[BL_SHAREDM_OFFSET_ECU_HW_PN + 1U];
            pData[6U] = bl_sharedm[BL_SHAREDM_OFFSET_ECU_HW_PN + 2U];
            pData[7U] = bl_sharedm[BL_SHAREDM_OFFSET_ECU_HW_PN + 3U];
            *pDataLen = 8;
            break;
            
        case 0xf193:    //hardware version
            //memcpy(pData, sharedm_ECU_HW_VERSION, BL_SHAREDM_LEN_ECU_HW_VERSION);
            pData[0U] = bl_sharedm[BL_SHAREDM_OFFSET_ECU_HW_VERSION + 0U];
            pData[1U] = bl_sharedm[BL_SHAREDM_OFFSET_ECU_HW_VERSION + 1U];
            *pDataLen = 2;
            /*
            offset = identifier - 0xF182;
            memcpy(&pData[0], &u8str[4U*offset], 4U);
            *pDataLen = 4;
            */
            break;
       
       /* case 0xf101:    // New identifier definition for ECU_CHECKSUM 
            memcpy(pData, sharedm_ECU_CHECKSUM, BL_SHAREDM_LEN_ECU_CHECKSUM);
            *pDataLen = 4;
            break;
            
        case 0xf176:    // active diagnostic session 
            pData[0] = udsCurrentSession;
            *pDataLen = 1;
            break;

        case 0xf199:    // read 8 bytes : F199 
            pData[0U] = 0x01;
            pData[1U] = 0x02;
            pData[2U] = 0x03;
            pData[3U] = 0x04;
            *pDataLen = 4;
            break;

        case 0xf181:    // read application software identification 
            offset = identifier - 0xF181;
            memcpy(&pData[0], &u8str[4U*offset], 4U);
            *pDataLen = 4;
            break;
        case 0xf182:    // read application software identification 
            offset = identifier - 0xF181;
            memcpy(&pData[0], &u8str[4U*offset], 4U);
            *pDataLen = 4;
            break;
        case 0xf183:    // read application software identification 
            offset = identifier - 0xF181;
            memcpy(&pData[0], &u8str[4U*offset], 4U);
            *pDataLen = 4;
            break;
        case 0xf184:    // read application software identification 
            offset = identifier - 0xF181;
            memcpy(&pData[0], &u8str[4U*offset], 4U);
            *pDataLen = 4;
            break;
        case 0xf185:    // read application software identification 
            offset = identifier - 0xF181;
            memcpy(&pData[0], &u8str[4U*offset], 4U);
            *pDataLen = 4;
            break;
        case 0xf186:    // read application software identification 
            offset = identifier - 0xF181;
            memcpy(&pData[0], &u8str[4U*offset], 4U);
            *pDataLen = 4;
            break;
        case 0xf187:    // read application software identification 
            memcpy(pData, sharedm_ECU_MainPartNb, BL_SHAREDM_LEN_ECU_MainPartNb);
            *pDataLen = 4;
            break;
        case 0xf188:    // read application software identification 
            offset = identifier - 0xF181;
            memcpy(&pData[0], &u8str[4U*offset], 4U);
            *pDataLen = 4;
            break;
        case 0xf189:    // read application software identification 
            offset = identifier - 0xF181;
            memcpy(&pData[0], &u8str[4U*offset], 4U);
            *pDataLen = 4;
            break;
        case 0xf18a:    // read application software identification 
            offset = identifier - 0xF181;
            memcpy(&pData[0], &u8str[4U*offset], 4U);
            *pDataLen = 4;
            break;
        case 0xf18b:    // read application software identification 
            offset = identifier - 0xF181;
            memcpy(&pData[0], &u8str[4U*offset], 4U);
            *pDataLen = 4;
            break;
        case 0xf18c:    // read application software identification 
            memcpy(pData, sharedm_ECU_SN, BL_SHAREDM_LEN_ECU_SN);
            *pDataLen = 4;
            break;
        case 0xf18d:    // read application software identification 
            offset = identifier - 0xF181;
            memcpy(&pData[0], &u8str[4U*offset], 4U);
            *pDataLen = 4;
            break;
        case 0xf18e:    // read application software identification 
            offset = identifier - 0xF181;
            memcpy(&pData[0], &u8str[4U*offset], 4U);
            *pDataLen = 4;
            break;
        case 0xf18f:    // read application software identification 
            offset = identifier - 0xF181;
            memcpy(&pData[0], &u8str[4U*offset], 4U);
            *pDataLen = 4;
            break;
        case 0xf190:    // read application software identification 
            offset = identifier - 0xF181;
            memcpy(&pData[0], &u8str[4U*offset], 4U);
            *pDataLen = 4;
            break;

        case 0xf192:    // read application software identification 
            offset = identifier - 0xF182;
            memcpy(&pData[0], &u8str[4U*offset], 4U);
            *pDataLen = 4;
            break;

        case 0xf194:    // read application software identification 
            offset = identifier - 0xF182;
            memcpy(&pData[0], &u8str[4U*offset], 4U);
            *pDataLen = 4;
            break;
        case 0xf195:    // read application software identification 
            offset = identifier - 0xF182;
            memcpy(&pData[0], &u8str[4U*offset], 4U);
            *pDataLen = 4;
            break;
        case 0xf196:    // read application software identification 
            offset = identifier - 0xF182;
            memcpy(&pData[0], &u8str[4U*offset], 4U);
            *pDataLen = 4;
            break;
        case 0xf197:    // read application software identification 
            offset = identifier - 0xF182;
            memcpy(&pData[0], &u8str[4U*offset], 4U);
            *pDataLen = 4;
            break;
        case 0xf198:    // read application software identification 
            offset = identifier - 0xF182;
            memcpy(&pData[0], &u8str[4U*offset], 4U);
            *pDataLen = 4;
            break;
        case 0xf19a:    // read application software identification 
            offset = identifier - 0xF183;
            memcpy(&pData[0], &u8str[4U*offset], 4U);
            *pDataLen = 4;
            break;
        case 0xf19b:    // read application software identification 
            offset = identifier - 0xF183;
            memcpy(&pData[0], &u8str[4U*offset], 4U);
            *pDataLen = 4;
            break;
        case 0xf19c:    // read application software identification 
            offset = identifier - 0xF183;
            memcpy(&pData[0], &u8str[4U*offset], 4U);
            *pDataLen = 4;
            break;
        case 0xf19d:    // read application software identification 
            offset = identifier - 0xF183;
            memcpy(&pData[0], &u8str[4U*offset], 4U);
            *pDataLen = 4;
            break;
        case 0xf19e:    // read application software identification 
            offset = identifier - 0xF183;
            memcpy(&pData[0], &u8str[4U*offset], 4U);
            *pDataLen = 4;
            break;
        case 0xf19f:    // read application software identification 
            offset = identifier - 0xF183;
            memcpy(&pData[0], &u8str[4U*offset], 4U);
            *pDataLen = 4;
            break;
        case 0xf1a0:    // read application software identification 
            offset = identifier - 0xF183;
            memcpy(&pData[0], &u8str[4U*offset], 4U);
            *pDataLen = 4;
            break;
        case 0xf1a1:    // read application software identification 
            offset = identifier - 0xF183;
            memcpy(&pData[0], &u8str[4U*offset], 4U);
            *pDataLen = 4;
            break;
        case 0xf1a2:    // read 35 bytes : F1A2 
            pData[0U]  = 0xA1;
            pData[1U]  = 0xA2;
            pData[2U]  = 0xB3;
            pData[3U]  = 0xB4;
            pData[4U]  = 0xC5;
            pData[5U]  = 0xC6;
            pData[6U]  = 0xD7;
            pData[7U]  = 0xD8;
            pData[8U]  = 0x11;
            pData[9U]  = 0x12;
            pData[10U] = 0x13;
            pData[11U] = 0x14;
            pData[12U] = 0x15;
            pData[13U] = 0x16;
            pData[14U] = 0x17;
            pData[15U] = 0x18;
            pData[16U] = 0x31;
            pData[17U] = 0x32;
            pData[18U] = 0x33;
            pData[19U] = 0x34;
            pData[20U] = 0x35;
            pData[21U] = 0x36;
            pData[22U] = 0x37;
            pData[23U] = 0x38;
            pData[24U] = 0xD1;
            pData[25U] = 0xD2;
            pData[26U] = 0xD3;
            pData[27U] = 0xD4;
            pData[28U] = 0xE5;
            pData[29U] = 0xE6;
            pData[30U] = 0xE7;
            pData[31U] = 0xE8;
            pData[32U] = 0xA6;
            pData[33U] = 0xA7;
            pData[34U] = 0xA8;
            *pDataLen = 35;
            break;
        case 0xfd0c:    // operation mode 
            pData[0] = opMode;
            *pDataLen = 1;
            break;
        case 0xf15a:	// fingerprint 
			if (pConfig->applFingerprintLength == 0xFF)  {
				retVal = RET_UDS_NRC_CNC;
				break;
			}
			memcpy(&pData[0], &pConfig->applFingerprint[0], pConfig->applFingerprintLength);
			*pDataLen = pConfig->applFingerprintLength;
			break;

        case 0xf16a:	// fingerprint 
			if (tmpFingerPrintLength == 0xFF)  {
				retVal = RET_UDS_NRC_CNC;
				break;
			}
			memcpy(&pData[0], &tmpFingerPrint[0], tmpFingerPrintLength);
			*pDataLen = tmpFingerPrintLength;
			break;   

        case 0xf16b:	// fingerprint 
            pData[0] = tmpFingerPrintLength;
			*pDataLen = 1U;
			break; */                  

        default:
            /* unsupported identifier */
            retVal = RET_UDS_NRC_ROOR;
            break;
        }

    } else {  /* write identifier */
        switch (identifier)  {

        case 0xfd0c:    /* operation mode */
            if (*pDataLen < 1) {              /* check for invalid data length */
                retVal = RET_UDS_NRC_IMLOIF;  /* incorrect message length or invalid format */
            } else {
                opMode = pData[0];
            }
            break;

		case 0xf15a:	/* fingerprint */
			if (*pDataLen <= APPL_FINGERPRINT_LENGTH)  {
				/* save fingerprint localy */
				memcpy(&tmpFingerPrint[0], &pData[0], *pDataLen);
				/* save fingerprint length localy */
				memcpy(&tmpFingerPrintLength, pDataLen, 1);
			} else {
				/* incorrect message length or invalid format */
				retVal = RET_UDS_NRC_IMLOIF;
			}
			break;
        default:
            /* unsupported identifier */
            retVal = RET_UDS_NRC_ROOR;
            break;
        }
    }
    return (retVal);
}


/******************************************************************************
* \brief isSecurityAccessUnlocked -
*
*/
static RET_T isSecurityAccessUnlocked(void)
{
RET_T retVal = RET_UDS_NRC_SAD;

	/* security access unlocked?
	 * and fingerprint written */
	if ((securityAccessUnlocked == CO_TRUE) &&
		(tmpFingerPrintLength != FLASH_EMPTY)) {
		retVal = RET_OK;
	}

	/* TODO: Add additional fingerprint checks
	 * to determine if the fingerprint is valid
	 */

	return(retVal);
}

/******************************************************************************
* \brief dataDownloadInd -
*
*/
static RET_T dataDownloadInd (
        uint8_t              srcAddr,
        UDS_DATA_REQ_TYPE_T  transType,
        uint8_t              dataFormat,
        uint32_t             memoryAddress,
        uint32_t             memorySize,
        uint8_t             *pData,
        uint16_t             size    )
{
    (void)srcAddr;       /* Remove Warning */
    (void)dataFormat;    /* Remove Warning */
    (void)memoryAddress; /* Remove Warning */

    RET_T retVal = RET_UDS_NRC_GPF;
    BlRet_t flashRet;

    /* This function hands over the received firmware data,
     * to the internal flash buffer */

    /* security access unlocked? */
    if (isSecurityAccessUnlocked() == RET_OK) {

        /* handle data transfers */
        switch (transType) {
        case UDS_REQUEST_READ:
            /* not supported right now */
            break;
        case UDS_REQUEST_WRITE:
            /* check for sufficient memory */
            if (memorySize <= CFG_MAX_DOMAINSIZE(0)) {
                retVal = RET_OK;
            }
            break;
        case UDS_TRANSFER_READ:
            /* not supported right now */
            break;
        case UDS_TRANSFER_WRITE:
            /* copy data to internal flash buffer */
            flashRet = flashPage(&pData[0], size, FLASH_BLOCK_FUNCTION);
            if (flashRet != BL_RET_OK) {
                /* general programming error */
                retVal = RET_UDS_NRC_GPF;
            } else {
                /* positive response */
                retVal = RET_OK;
            }
            break;
        case UDS_TRANSFER_EXIT:
			/* wait for flashing to be finishec */
			flashRet = flashWaitEnd();
			if (flashRet == BL_RET_OK)  {
				/* positive response */
				retVal = RET_OK;
			}
            break;
        default:
            break;
        }

    } else {
        /* security access denied */
        retVal = RET_UDS_NRC_SAD;
    }

    return (retVal);
}


/******************************************************************************
* \brief checkSecurityAccess -
*
*/
void checkSecurityAccess(void)
{
    /* check if security access has to be locked */
    if ((securityAccessUnlocked == CO_TRUE) &&
        (udsCommGetDiagnosticSessionType() != UDS_SUB_SESSION_PRGS)) {
        /* lock security access for non programming sessions */
        //securityAccessUnlocked = CO_FALSE;
    }
    //securityAccessUnlocked = CO_TRUE; /* $klg> whatever the case dont wanna get ourselves locked */
}


/******************************************************************************
* \brief secAccessReqSeed -
*
*/
static RET_T secAccessReqSeed (
        uint8_t   srcAddr,
        uint8_t   accessType,
        uint8_t  *pSeedKey,
        uint16_t *pSeedKeyLen         )
{
    (void)srcAddr; /* Remove Warning */

    int i;
    uint8_t fixedseed[UDS_SECURITY_SEED_KEY_LEN] = { 0xBA, 0xDC, 0x0D, 0xED };
    RET_T retVal = RET_UDS_NRC_SAD;
    static uint8_t cnt = 0;
    static uint8_t tryings = 0;
    static uint32_t mySeed;
    static uint32_t myKey;
    static uint16_t myseed_high;
    static uint16_t myseed_low;
    static uint16_t key_high;
    static uint16_t key_low;

    /* This function provides a requested seed to the tool,
     * and compares the key sent by the tool,
     * with an internal saved self generated key.
     * The key generation algorithm has to be implemented by the user.
     * This is only a simple NON SECURE example! */

    /* request seed */
    if (accessType & 1) {

        /* save key len */
        *pSeedKeyLen = UDS_SECURITY_SEED_KEY_LEN;

        /* security access already unlocked? */
        if (securityAccessUnlocked == CO_TRUE)  {
            /* don't generate new key - return zero */
            for (i = 0; i < UDS_SECURITY_SEED_KEY_LEN; i++) {
                pSeedKey[i] = 0u;
            }
            udsCommSetSecurityLevel(savedSecurityLevel);
        } else {
            /* security access locked - generate new seed */
            for (i = 0; i < UDS_SECURITY_SEED_KEY_LEN; i++)
                pSeedKey[i] = fixedseed[i] + (cnt*3);

            cnt++;  
            /* save requested security level */
            savedSecurityLevel = accessType;
            /* save key for later comparison */
            mySeed = pSeedKey[0] << 24 | pSeedKey[1] << 16 | pSeedKey[2] << 8 | pSeedKey[3];
             switch (savedSecurityLevel) {
                case  REQ_SEED0_ID:                                   
                    myKey = ((((mySeed ^ PASSWORD_DIAG) << 16) & 0xFFFFFFFF) + (mySeed ^ PASSWORD_DIAG) + PASSWORD_DIAG) & 0xFFFFFFFF;
                    break;
                case  REQ_SEED1_ID:
                    myKey = ((((mySeed ^ PASSWORD_DOWN) << 12) & 0xFFFFFFFF) + (mySeed ^ PASSWORD_DOWN) + PASSWORD_DOWN) & 0xFFFFFFFF;
                    break;
                case  REQ_SEED2_ID:
                    myKey = ((((mySeed ^ PASSWORD_LOW) << 10) & 0xFFFFFFFF) + (mySeed ^ PASSWORD_LOW) + PASSWORD_LOW) & 0xFFFFFFFF;
                    break;
                case  REQ_SEED3_ID:
                    myKey = ((((mySeed ^ PASSWORD_THIRD) << 8) & 0xFFFFFFFF) + (mySeed ^ PASSWORD_THIRD) + PASSWORD_THIRD) & 0xFFFFFFFF;
                    break;
                case  REQ_SEED4_ID:
                    myKey = ((((mySeed ^ PASSWORD_DOWN_THIRD) << 4) & 0xFFFFFFFF) + (mySeed ^ PASSWORD_DOWN_THIRD) + PASSWORD_DOWN_THIRD) & 0xFFFFFFFF;
                    break;
                case  REQ_SEED5_ID:
                    myKey = ((((mySeed ^ PASSWORD_MAXI) << 16) & 0xFFFFFFFF) + (mySeed ^ PASSWORD_MAXI) + PASSWORD_MAXI) & 0xFFFFFFFF;
                    break;
            }
            u32_udsSavedKey[0] = myKey >> 24;
            u32_udsSavedKey[1] = myKey >> 16;
            u32_udsSavedKey[2] = myKey >> 8;
            u32_udsSavedKey[3] = myKey;            
        }
        /* positive response */
        retVal = RET_OK;
    } else {
        if (((u32_udsSavedKey[0] == pSeedKey[0]) &&
            (u32_udsSavedKey[1] == pSeedKey[1]) &&
            (u32_udsSavedKey[2] == pSeedKey[2]) &&
            (u32_udsSavedKey[3] == pSeedKey[3]) &&
            (tryings < 3U)) || (securityAccessUnlocked == CO_TRUE))
        {
            *pSeedKeyLen = 0;
            /* positive response */
            retVal = RET_OK;
            /* security access is unlocked now */
            securityAccessUnlocked = CO_TRUE;
            /* compare key sent by the tool with saved key */
        }else{
            /* wrong key */
            retVal = RET_UDS_NRC_IK;
            securityAccessUnlocked = CO_FALSE;
            tryings++;
        }
    }
    return (retVal);
}

/******************************************************************************
* \brief routineControl -
*
*/
RET_T routineControl (
        uint8_t   srcAddr,
        uint8_t   ctrltype,
        uint16_t  identifier,
        uint8_t  *pData,
        uint16_t *pDataSize  )
{
    (void)srcAddr; /* Remove Warning */
    uint16_t routineControl_Counter_Wd_Ext = 0U;

    RET_T retVal = RET_OK;

    /* This function provides the possibility,
     * to react to routine control requests  */

    /* reset pDataSize for response */
    *pDataSize = 0u;

    if (ctrltype == UDS_ROUTINE_CTRL_START) {

        switch (identifier) {

        case UDS_RID_ERASE_MEMORY:
            /* security access unlocked? */
            if (isSecurityAccessUnlocked() == RET_OK) {
                /* erase flash */
                flashEraseRequired();

                if (flashErase(0) == BL_RET_OK) {

                    /* wait for erase to be finished */
                    while (flashGetState() == FLASH_USERSTATE_RUNNING) {

                        /* this could take some time, so send an response pending */
                        (void)udsSendResponsePending(1, UDS_REQ_ROUTINE_CTRL);

                        /* flash handler */
                        flashCyclic();

                        if (routineControl_Counter_Wd_Ext++ == 0x10) {
                            routineControl_Counter_Wd_Ext = 0U;
                            HAL_GPIO_TogglePin(GPIOC, GPIO_PIN_1);
                        }
                    }

                    /* WA flash re-init after erasing WA */

                    uint32_t cntblreinit = 0U;
                    do {
                            flashCyclic();
                            if (cntblreinit == 32U) {
                                flashAbort(); /* Flash cycle abort */
                            }

                    } while (cntblreinit++ < 1024);

                    setFlash_custom_valFlashAddress(0x0800F800);

                    if (flashInit(0u) != BL_RET_OK)  {
                        while(1);
                    }

                    pData[4] = 0;
                    *pDataSize = 1U;

                } else {
                    /* general programming error */
                    retVal = RET_UDS_NRC_GPF;
                    pData[4] = 1;
                    *pDataSize = 1U;
                }
            } else {
                /* security access denied */
                retVal = RET_UDS_NRC_SAD;
            }

            break;


        case UDS_RID_FLASH_AFTER_RESET:

            *pDataSize = 0U;
            bUds_F5_18_Requested = CO_TRUE;
            udsResetFblPingTimeout();

            break;


        case UDS_RID_CHK_PROG_DEPENDS:
            /* fall through */
        case UDS_RID_CHK_MEMORY:

            /* this could take some time, so send an response pending */
            (void)udsSendResponsePending(1, UDS_REQ_ROUTINE_CTRL);

            /* check application CRC16 sum */
            BlRet_t resCRC16 = applCheckChecksum(0), resCRC32 = BL_RET_CRC_WRONG;

            if (identifier == UDS_RID_CHK_MEMORY)
            {
                uint32_t calcSum32, crcSum32 = 0U;
                uint8_t *pu8 = (uint8_t *)(&crcSum32);
                memcpy(pu8, &pData[4U], 4U);
                /* $klg> extra checking CRC32 sum */
                resCRC32 = resCRC16 == BL_RET_OK ? applCheckCRC32(0, crcSum32, &calcSum32) : resCRC32;

                *pDataSize = 1U;
                pData[4] = resCRC32 == BL_RET_OK ? 0U: 1U;

            }
            if (identifier == UDS_RID_CHK_PROG_DEPENDS)
            {

                *pDataSize = 1U;
                pData[4] = resCRC16 == BL_RET_OK ? 0U: 1U;

            }

            break;


        default:
            /* routine not implemented - return general reject */
            retVal = RET_UDS_NRC_GR;

            break;

        }
    }

    return (retVal);
}


/******************************************************************************
* \brief udsCommManagementInd -
*
*/
static RET_T udsCommManagementInd (
        BOOL_T    execute,
        uint8_t   srcAddr,
        uint8_t   reqType,
        uint8_t   subFct,
        uint8_t  *pData,
        uint16_t  dataLen                 )
{
    (void)srcAddr; /* Remove Warning */
    (void)pData; /* Remove Warning */
    (void)dataLen; /* Remove Warning */

    RET_T retVal = RET_SERVICE_NOT_INITIALIZED;

    /* This function provides information
     * about certain service request,
     * that can be accepted or declined.
     * UDS parameters like timings
     * or response codes can be changed in here */

    /* check before real execution */
    if (execute == 0u) {
        /* check if an ECU reset is possible
         * setup and send out time needed to reset. */
        if (reqType == UDS_REQ_ECU_RESET) {
            if (subFct == UDS_SUB_RESET_HR) {
                udsCommSetPowerDownTime(5);
            }
            if (subFct == UDS_SUB_RESET_SR) {
                udsCommSetPowerDownTime(2);
            }
        }
        /* accept reset request */
        retVal = RET_OK;

        /* if you want to deny ECU reset */
        ///* security access denied */
        //retVal = RET_UDS_NRC_SAD;
    } else
        switch (reqType) {
            /* session control requests */
        case UDS_REQ_SESSION_CONTROL:
            switch(subFct) {
            case UDS_SUB_SESSION_DS:
                /* request default session */
                /* setup session timings */
                udsCommSetSessionTime(2000ul, 10000ul);
                /* save current session */
                udsCurrentSession = subFct;
                /* (re)lock security access */
                /* securityAccessUnlocked = CO_FALSE; //$klg> whatever the case dont wanna get ourselves locked */
                /* allow session request */
                retVal = RET_OK;
                break;
            case UDS_SUB_SESSION_PRGS:
                /* request programming session */
                /* setup session timings */
                udsCommSetSessionTime(2000ul, 10000ul);
                /* save current session */
                udsCurrentSession = subFct;
                /* allow session request */
                retVal = RET_OK;
                break;
            case UDS_SUB_SESSION_EXTDS:
                /* request extended diagnostic session */
                /* setup session timings */
                udsCommSetSessionTime(2000ul, 10000ul);
                /* save current session */
                udsCurrentSession = subFct;
                /* allow session request */
                retVal = RET_OK;
                break;
            default:
                /* sub function not supported */
                retVal = RET_UDS_NRC_SFNS;
                break;
            }
            break;
        /* reset request */
        case UDS_RESP_ECU_RESET:
            /* ECU reset response transmitted
             * do the reset now */
            USER_RESET();
            break;
        default:
            /* service not supported */
            retVal = RET_UDS_NRC_SNS;
            break;
        }

    return (retVal);
}


/******************************************************************************
* \brief Reset - reset system
*
*/
void softwareReset(void)
{
    codrvCanDisable();
    NVIC_SystemReset();
    while(1);
}
