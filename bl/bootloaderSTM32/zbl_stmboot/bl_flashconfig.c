/*
* bl_flashconfig.c
*
*-------------------------------------------------------------------
*
*-------------------------------------------------------------------
*
*
*/

/********************************************************************/
/**
* \file
* \brief bl_flashconfig.c
*
* Flash config, like different image regions
*/



/* header of standard C - libraries
---------------------------------------------------------------------------*/
#include <stddef.h>

/* header of project specific types
---------------------------------------------------------------------------*/
#include <bl_config.h>
#ifdef BL_DOMAIN_COUNT

#include <bl_type.h>
#include <bl_user.h>
#include <bl_application.h>
#include <bl_call.h>
#include <bl_flash.h>



/* constant definitions
---------------------------------------------------------------------------*/

/* local defined data types
---------------------------------------------------------------------------*/
typedef struct {
	FlashAddr_t domainStartAddr;
	U32 domainSize;
	U32 configBlockSize;
} flashImageConfiguration_t;

/* list of external used functions, if not in headers
---------------------------------------------------------------------------*/

/* list of global defined functions
---------------------------------------------------------------------------*/

/* list of local defined functions
---------------------------------------------------------------------------*/

/* external variables
---------------------------------------------------------------------------*/

/* global variables
---------------------------------------------------------------------------*/

/* local defined variables
---------------------------------------------------------------------------*/
#error "First adapt the image settings!"
static const flashImageConfiguration_t flashImage[2] = {
		{ 0xFD0000ul, 0xFFA000ul - 0xFD0000ul, 0x210 },
		{ 0xFFA000ul, 0xFFC000ul - 0xFFA000ul, 6 }
};



/*
* memory map
*     0xFD_0000.. 0xFF_9FFF Image 1
*     0xFF_A000.. 0xFF_BFFF Image 2
*     0xFF_C000.. 0xFF_FFFF Bootloader
*/



#if BL_DOMAIN_COUNT > 2
#error "Only 2 domains adapted!"
#endif

/***************************************************************************/
/**
 * \brief userGetApplStartAdr
 *
 * start address of a specific region
 */
FlashAddr_t userGetApplStartAdr(
		U8 nbr /**< region 0..n */
	)
{
FlashAddr_t retVal = 0;

	if (nbr < BL_DOMAIN_COUNT)  {
		retVal = flashImage[nbr].domainStartAddr;
	}
	return (retVal);
}

/***************************************************************************/
/**
 * \brief userGetDomainSize
 *
 * size of a specific region (Byte)
 *
 */
U32 userGetDomainSize(
		U8 nbr /**< region 0..n */
	)
{
U32 retVal = 0u;

	if (nbr < BL_DOMAIN_COUNT)  {
		retVal = flashImage[nbr].domainSize;
	}
	return (retVal);
}

/***************************************************************************/
/**
 * \brief userGetConfigSize
 *
 * size of the CRC Config block of a specific region
 */
U32 userGetConfigSize(
		U8 nbr /**< region 0..n */
	)
{
U32 retVal = 0u;

	if (nbr < BL_DOMAIN_COUNT)  {
		retVal = flashImage[nbr].configBlockSize;
	}
	return (retVal);
}


/***************************************************************************/
/**
 * \brief userGetImageNr
 *
 * get the image number of a specific flash address
 */
U8 userGetImageNr(
		FlashAddr_t address,	/**< flash address */
		U8* imgNr				/**< number of image */
	)
{
U8 imageNr = 0u;
U8 nrOfImages;
BlRet_t retVal = BL_RET_IMAGE_WRONG;

	/* how many images ? */
	nrOfImages = (sizeof(flashImage) / sizeof(flashImageConfiguration_t));

	/* find image of address */
	for (imageNr = 0u; imageNr < nrOfImages; imageNr++)  {
		if ((address >= flashImage[imageNr].domainStartAddr) &&
			(address < flashImage[imageNr].domainStartAddr + flashImage[imageNr].domainSize))  {
			/*save image number */
			*imgNr = imageNr;
			retVal = BL_RET_OK;
			break;
		}
	}
	return(retVal);
}


#endif /* BL_DOMAIN_COUNT */
