/*
* bl_crc.h
*
*-------------------------------------------------------------------
*
*-------------------------------------------------------------------
*
*
*/

/********************************************************************/
/**
* \file
* \brief bl_crc.h - CRC calculation
*
*/

#ifndef BL_CRC_H
#define BL_CRC_H 1

/* constant definitions
---------------------------------------------------------------------------*/
/** startvalue like sdo block transfer */
#ifndef CRC_START_VALUE	
#define CRC_START_VALUE	0
#endif

#define CCITT_MASK 0x1021

#define CRC32_START_VALUE 0xFFFFFFFF

/* external Prototypes */
/* ------------------------------------------------------------------*/
#ifdef CRC_USER
extern void crcUserInit(void);
extern uint16_t crcUser( const uint8_t * pBuffer, uint16_t u16StartValue, uint32_t u32Count);
#endif


/* Prototypes */
/* ------------------------------------------------------------------*/
void crcInitCalculation(void);
uint32_t crc32Calculation(const uint8_t *pBuffer, uint32_t u32StartValue, uint32_t u32Count);
uint16_t crcCalculation(const uint8_t * pBuffer, uint16_t u16StartValue, uint32_t u32Count);

#endif /* BL_CRC_H */
