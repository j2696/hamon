/*
* bl_flash_lowlvl.h
*
*-------------------------------------------------------------------
*
*-------------------------------------------------------------------
*
*
*/

/********************************************************************/
/**
* \file
* \brief Low Level functionality of the Flash 
*
*/

#ifndef BL_FLASH_LOWLVL_H
#define BL_FLASH_LOWLVL_H 1

/* Macros
* --------------------------------------------------------------------*/
/* number of Byte that are flashed at the same time */
#define FLASH_ONE_CALL_SIZE 8u

/* Type definitions
* --------------------------------------------------------------------*/

/* flash sector definition */
typedef struct {
	FlashAddr_t baseAddress;
	uint32_t sectorSize;
	//Fapi_FlashSectorType sectorNumber;
	//Fapi_FlashBankType bankNumber;
} FLASH_SECTOR_T;


/* Constants
* --------------------------------------------------------------------*/

/* Variables
* --------------------------------------------------------------------*/

/* Prototypes
* --------------------------------------------------------------------*/
#ifdef FLASH_ERASE_SIZE
#else /* FLASH_ERASE_SIZE */
const FLASH_SECTOR_T * llsectorParams(FlashAddr_t address);
uint32_t llsectorSize(FlashAddr_t address);
#endif /* FLASH_ERASE_SIZE */

BlRet_t llflashInit(uint8_t nbr);
BlRet_t lleraseOnePage(FlashAddr_t address);
BlRet_t llflashOnePage(FlashAddr_t address, const uint8_t * pSrc);
uint16_t llflashOneCallSize(void);


#endif /* BL_FLASH_LOWLVL_H */
