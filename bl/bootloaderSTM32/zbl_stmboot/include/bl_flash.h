/*
* bl_flash.h
*
*-------------------------------------------------------------------
*
*-------------------------------------------------------------------
*
*
*/

/********************************************************************/
/**
* \file
* \brief bl_flash.h
*
*/

#ifndef BL_FLASH_H
#define BL_FLASH_H 1

/** \brief Flash driver states */
typedef enum {

	FLASH_STATE_INIT, /**< ready for commands */
	FLASH_STATE_WAIT, /**< wait for new flash pages */
	FLASH_STATE_ERASE, /**< erase a page */
	FLASH_STATE_FLASH, /**< flash a page */
	FLASH_STATE_ERROR, /**< error during flash/erase */
	FLASH_STATE_END		/**< last page flashed */
} FlashState_t;

#ifdef BL_DEBUG
static const char* FlashState_txt[] = {
		"FLASH_STATE_INIT",
		"FLASH_STATE_WAIT",
		"FLASH_STATE_ERASE",
		"FLASH_STATE_FLASH",
		"FLASH_STATE_ERROR",
		"FLASH_STATE_END"
};
#endif

/** \brief reduced Flash driver states for user */
typedef enum {

	FLASH_USERSTATE_OK, /**< OK, nothing to do */
	FLASH_USERSTATE_RUNNING, /**< doing anything (erase, program) */
	FLASH_USERSTATE_ERROR /**< error during flash/erase */
} FlashUserState_t;


/* flash page definition */
typedef struct {
	FlashAddr_t baseAddress;
	uint32_t pageSize;
	uint16_t pageMask;
} pageStructure_t;


#define FLASH_NONBLOCKING 0u
#define FLASH_BLOCK_FUNCTION 1u

/* Variables
* --------------------------------------------------------------------*/
extern Flag_t fFullEraseRequired;
extern Flag_t fFlashErased[];
extern uint8_t activeDomain;

/* Prototypes
* --------------------------------------------------------------------*/

void setFlash_custom_valFlashAddress(uint32_t address);

BlRet_t flashInit(uint8_t nbr);
BlRet_t flashErase(uint8_t nbr);
BlRet_t flashPage(const void * const pBuffer, uint32_t size, Flag_t fBlocking);
BlRet_t flashCyclic(void);
BlRet_t flashAbort(void);
BlRet_t flashWaitEnd(void);
void flashEraseRequired(void);

FlashUserState_t flashGetState(void);


#endif /* BL_FLASH_H_ */
