﻿/*
* bl_uds.h
*
*-------------------------------------------------------------------
*
*-------------------------------------------------------------------
*
*
*/
/* SECURITY ACCES*/
#define SECURITY_ACCESS_SID     0x27
#define REQ_SEED0_ID		    0x01
#define SEND_KEY0_ID 		    0x02
#define REQ_SEED1_ID			0x03
#define SEND_KEY1_ID    		0x04
#define REQ_SEED2_ID			0x05
#define SEND_KEY2_ID    		0x06
#define REQ_SEED3_ID			0x07
#define SEND_KEY3_ID    		0x08
#define REQ_SEED4_ID			0x09
#define SEND_KEY4_ID    		0x0A
#define REQ_SEED5_ID			0x0B
#define SEND_KEY5_ID    		0x0C

#define SECURITY_ACCES_LOCKED   0x0
#define SECURITY_ACCES_DIAGNOSTIC   0x11
#define SECURITY_ACCES_DOWNLOAD    0x12
#define SECURITY_ACCES_LOW_LEVEL   0x13
#define SECURITY_ACCES_THIRD_PARTY    0x14
#define SECURITY_ACCES_DOWNLOAD_THIRD_PARTY   0x15
#define SECURITY_ACCES_MAXIMATEC  0x16
#define PASSWORD_DIAG           0xEF253970  //diagnostic
#define PASSWORD_DOWN           0xF5B01E5C  //download
#define PASSWORD_LOW            0xB48D5E32  //low level
#define PASSWORD_THIRD          0x7C66A60  //third party
#define PASSWORD_DOWN_THIRD     0x88F192BC  //download third party
#define PASSWORD_MAXI           0xE189ABE1  //Maximatecc
/********************************************************************/
/**
* \file
* \brief Low Level functionality of the Flash 
*
*/

#ifndef BL_UDS_H
#define BL_UDS_H 1

#include <bl_type.h>
#include <co_datatype.h>


/* Macros
* --------------------------------------------------------------------*/

/* Type definitions
* --------------------------------------------------------------------*/

/* Constants
* --------------------------------------------------------------------*/

/* Variables
* --------------------------------------------------------------------*/

/* Prototypes
* --------------------------------------------------------------------*/
BlRet_t udsInit(void);
void udsCyclic(void);
void udsSetFARstatus(BOOL_T bVal);
BOOL_T udsIsFARtimeElapsed(void);
BOOL_T udsIsFblPingTimeoutElapsed(void);

#endif /* BL_UDS_H */
