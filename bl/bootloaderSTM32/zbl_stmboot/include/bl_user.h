/*
* bl_user.h
*
*-----------------------------------------------------------------------------
*
*-----------------------------------------------------------------------------
*
*/

/**
* \file
* \brief User specific functionality
*/

#ifndef BL_USER_H
#define BL_USER_H 1

#include <bl_type.h>


/* datatypes
-----------------------------------------------------------------------------*/

/* function prototypes
-----------------------------------------------------------------------------*/

/* Keep in mind that when using BL_DBG_RAM feature once calling DBG_RAM_Init */
/* the valid data stored in dbgvect_ram section (if any) will be overwritten */

 #define BL_DBG_RAM  1 

#ifdef BL_DBG_RAM
#define DBG_RAM_MAX_BYTES    240U
extern uint8_t bl_dbgvect[DBG_RAM_MAX_BYTES];
extern uint8_t bl_dbg_cnt;
#endif

#define SHAREDM_MAX_BYTES     32U
extern uint8_t bl_sharedm[SHAREDM_MAX_BYTES];

#define BL_SHAREDM_OFFSET_ECU_HW_PN        0U
#define BL_SHAREDM_LEN_ECU_HW_PN           4U

#define BL_SHAREDM_OFFSET_ECU_SN           4U
#define BL_SHAREDM_LEN_ECU_SN              4U

#define BL_SHAREDM_OFFSET_ECU_HW_VERSION   8U
#define BL_SHAREDM_LEN_ECU_HW_VERSION      2U

#define BL_SHAREDM_OFFSET_ECU_MainPartNb  10U
#define BL_SHAREDM_LEN_ECU_MainPartNb      4U

#define BL_SHAREDM_OFFSET_ECU_CHECKSUM    14U
#define BL_SHAREDM_LEN_ECU_CHECKSUM        4U



void DBG_RAM_Init(void);
void DBG_RAM_Trace_1(uint16_t a1);
void DBG_RAM_Trace_2(uint16_t a1, uint16_t a2);
void DBG_RAM_Trace_3(uint16_t a1, uint16_t a2, uint16_t a3);
void DBG_RAM_Trace_4(uint16_t a1, uint16_t a2, uint16_t a3, uint16_t a4);


uint8_t userGetNodeId(void);
uint16_t userGetBitrate(void);

uint32_t userOd1000(void);
uint32_t userOd1018_1(void);
uint32_t userOd1018_2(void);
uint32_t userOd1018_3(void);
uint32_t userOd1018_4(void);
uint32_t userOd5F00_0(void);

uint32_t userOdU32ReadFct(uint16_t index, uint8_t subIndex);
CoblObjInfo_t userSegReadFct(uint16_t index, uint8_t subIndex);

void userLssSetNodeId(CoblLssService_t service, uint16_t para, uint8_t *errorCode, uint8_t *errorSpec);

#endif /* BL_USER_H */

