/*
* co_drv.h - contains defines for driver
*
*-------------------------------------------------------------------
*
*-------------------------------------------------------------------
*
*
*
*/

/**
* \brief defines for driver
*
* \file co_drv.h - contains defines for driver
*
*/

#ifndef CO_DRV_H
#define CO_DRV_H 1

# define CO_CAN_MAX_DATA_LEN	8u


typedef	void *		LIBDRV_HANDLE_T;

#ifndef CO_CAN_TIMESTAMP_T
# define CO_CAN_TIMESTAMP_T uint32_t
#endif /* CO_CAN_TIMESTAMP_T */


/** flags for CAN Messages from/to driver */
#define	CO_COBFLAG_NONE			((uint8_t)(0u))		/**< none */
#define	CO_COBFLAG_ENABLED		((uint8_t)(1u))		/**< cob enabled/disabled */
#define CO_COBFLAG_EXTENDED		((uint8_t)(1u << 1))	/**< extended id  */
#define	CO_COBFLAG_FD			((uint8_t)(1u << 2))	/**< fd frame */
#define	CO_COBFLAG_RTR			((uint8_t)(1u << 3))	/**< rtr */
#define CO_COBFLAG_IND			((uint8_t)(1u << 4))	/**< msg creates a indication, only valid for TX */


/* datatypes 
-----------------------------------------------------------*/
/** CAN cob structure */
typedef struct {
	uint32_t			canId;			/**< can identifier */
	uint32_t			ignore;			/**< ignore mask for id */
	uint16_t			canChan;		/**< reserved for driver */
	uint8_t			flags;			/**< flags (rtr, extended, enabled, ... */
} CO_CAN_COB_T;


/** CAN receive message structure */
typedef struct {
	uint32_t			canId;			/**< can identifier */
#ifdef CO_CAN_TIMESTAMP_SUPPORTED
	CO_CAN_TIMESTAMP_T	timestamp;		/**< timestamp */
#endif /* CO_CAN_TIMESTAMP_SUPPORTED */
	uint8_t			flags;			/**< flags (rtr, extended, enabled, ... */
	uint8_t			len;			/**< msg len */
	uint8_t			data[CO_CAN_MAX_DATA_LEN];	/**< data */
} CO_CAN_REC_MSG_T;


/** CAN transmit message structure */
typedef struct {
	LIBDRV_HANDLE_T		handle;			/**< library internal handle */
	uint32_t			canId;			/**< can identifier */
	uint16_t			canChan;		/**< reserved for driver */
	uint8_t			flags;			/**< flags (rtr, extended, enabled, ... */
	uint8_t			len;			/**< msg len */
	uint8_t			data[CO_CAN_MAX_DATA_LEN];	/**< data */
} CO_CAN_TR_MSG_T;


/** bittiming table entries */
typedef struct {
	uint16_t bitRate; 	/**< bitrate in kbit/s */
	uint16_t pre; 		/**< prescaler */
	uint8_t prop;			/**< propagation segment */
	uint8_t seg1; 		/**< segment 1 */
	uint8_t seg2; 		/**< segment 2 */
} CO_NV_STORAGE CODRV_BTR_T;



/* function prototypes */

EXTERN_DECL void codrvWaitForEvent(uint32_t	msecTimeout);
EXTERN_DECL void codrvWaitForCanEvent(uint32_t	waitTimeOut);
EXTERN_DECL void codrvHardwareInit(void);

/* CAN
-----------------------------------------------------------*/
EXTERN_DECL RET_T	codrvCanInit(uint16_t bitRate);
EXTERN_DECL RET_T	codrvCanInitWithDev(const char *devName,
						uint16_t bitRate);
EXTERN_DECL RET_T	codrvCanReInit(uint16_t	bitRate);
EXTERN_DECL RET_T	codrvCanSetBitRate(uint16_t	bitRate);
EXTERN_DECL RET_T	codrvCanStartTransmission(void);
EXTERN_DECL void	codrvCanDriverHandler(void);
EXTERN_DECL RET_T	codrvCanEnable(void);
EXTERN_DECL RET_T	codrvCanDisable(void);
EXTERN_DECL RET_T	codrvCanDeinit(void);
EXTERN_DECL int		codrvGetCanFd(void);


/* QUEUE
-----------------------------------------------------------*/
EXTERN_DECL uint8_t *coQueueGetReceiveBuffer(
			uint32_t canId, uint8_t dataLen, uint8_t flags
#ifdef CO_CAN_TIMESTAMP_SUPPORTED
			, CO_CAN_TIMESTAMP_T	timestamp
#endif /* CO_CAN_TIMESTAMP_SUPPORTED */
			);
EXTERN_DECL void	coQueueReceiveBufferIsFilled(void);
EXTERN_DECL BOOL_T	coQueueReceiveMessageAvailable(void);
EXTERN_DECL CO_CAN_TR_MSG_T *coQueueGetNextTransmitMessage(void);
EXTERN_DECL void	coQueueMsgTransmitted(const CO_CAN_TR_MSG_T *pBuf);

/* GATEWAY
-----------------------------------------------------------*/
EXTERN_DECL void coGatewayTransmitMessage(const CO_CAN_TR_MSG_T *pMsg);
EXTERN_DECL void coQueueRecMsgFromGw(CO_CAN_REC_MSG_T *pMsg);

/* CAN IRQ
-----------------------------------------------------------*/
EXTERN_DECL void codrvCanEnableInterrupt(void);
EXTERN_DECL void codrvCanDisableInterrupt(void);

/* CAN - FullCAN functionality (Acceptance filter)
-----------------------------------------------------------*/
EXTERN_DECL RET_T codrvCanSetFilter(CO_CAN_COB_T *pCanCob);

/* timer
-----------------------------------------------------------*/
EXTERN_DECL RET_T codrvTimerSetup(uint32_t timerInterval);

#endif /* CO_DRV_H */

