/*
* co_timer.h - contains defines for timer
*
*-------------------------------------------------------------------
*
*-------------------------------------------------------------------
*
*
*
*/

/**
* \brief defines for timer
*
* \file co_timer.h - contains defines for timer
*/

#ifndef CO_TIMER_H
#define CO_TIMER_H 1


/* datatypes */

/**
* timer attributes
*/
typedef enum {
	CO_TIMER_ATTR_ROUNDUP,			/**< round up given timer value */
	CO_TIMER_ATTR_ROUNDUP_CYCLIC,	/**< round up and start timer again */
	CO_TIMER_ATTR_ROUNDDOWN,		/**< round down given timer value */
	CO_TIMER_ATTR_ROUNDDOWN_CYCLIC	/**< round down and start timer again */
} CO_TIMER_ATTR_T;


/** \brief function pointer to Timer indication
 * \param pFct - pointer to timer up function
 * 
 */
typedef void (* CO_TIMER_FCT_T)(void *); /*lint !e960 customer specific parameter names */


/**
* timer structure 
*/
typedef struct co_timer {
	struct co_timer	*pNext;			/**< pointer to next timer */
	uint32_t		actTicks;		/**< actual timer ticks */
	uint32_t		ticks;			/**< calculated timer ticks */
	CO_TIMER_FCT_T pFct;			/**< pointer to own function */
	void			*pData;			/**< pointer for own data */
	CO_TIMER_ATTR_T	attr;			/**< timer attributes */
} xTimer;
typedef struct co_timer	CO_TIMER_T;


/* function prototypes */

EXTERN_DECL void	coTimerInit(uint32_t timerVal);
EXTERN_DECL RET_T	coTimerStart(CO_TIMER_T *pTimer,
				uint32_t timerTime, CO_TIMER_FCT_T pFct, void *pData,
				CO_TIMER_ATTR_T timerAttributes);
EXTERN_DECL RET_T	coTimerStop(CO_CONST CO_TIMER_T *pTimer);
EXTERN_DECL BOOL_T	coTimerIsActive(CO_CONST CO_TIMER_T *pTimer);
EXTERN_DECL void	coTimerTick(void);
EXTERN_DECL void	coTimerAttrChange(CO_TIMER_T *pTimer,
				CO_TIMER_ATTR_T timerAttributes);

#endif /* CO_TIMER_H */
