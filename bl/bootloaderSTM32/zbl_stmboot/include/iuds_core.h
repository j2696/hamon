﻿/*
* iuds_core.h - contains internal defines for uds core
*
*-------------------------------------------------------------------
*
*-------------------------------------------------------------------
*
*
*/

/**
* \file
* \brief internal defines for uds core
*/

#ifndef IUDS_CORE_H
#define IUDS_CORE_H 1


#ifndef UDS_BUFFER_SIZE
# define UDS_BUFFER_SIZE (4u * 1024u)
#endif /* UDS_BUFFER_SIZE */

/* function prototypes */

RET_T iudsSendResponseWA(uint8_t addr, uint8_t* pData, uint16_t dataSize,
		uint8_t flags);
RET_T iudsSendResponse(uint8_t addr, uint8_t* pData, uint16_t dataSize,
		uint8_t flags);
RET_T iudsSendAbort(uint8_t addr, uint8_t* pData, uint8_t reason,
		uint8_t flags);
RET_T iudsSendRetAbort(uint8_t addr, uint8_t* pData, RET_T retReason,
		uint8_t flags);
RET_T iudsMsgHandler(uint8_t addr, uint8_t* pData, uint16_t dataSize);

#endif /* IUDS_CORE_H */
