/*
* iuds_stored.h - contains internal defines for uds stored
*
*-------------------------------------------------------------------
*
*-------------------------------------------------------------------
*
*
*/

/**
* \file
* \brief internal defines for uds stored
*/

#ifndef IUDS_STOREDE_H
#define IUDS_STOREDE_H 1



/* function prototypes */

RET_T iudsStoreMsgRecv(uint8_t addr, uint8_t* pData);
RET_T iudsStoredMsgHandler(uint8_t addr, uint8_t* pData, uint16_t dataSize);


#endif /* IUDS_STOREDE_H */
