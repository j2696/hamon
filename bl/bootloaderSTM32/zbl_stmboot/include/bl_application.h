/*
* bl_application.h
*
*-------------------------------------------------------------------
*
*-------------------------------------------------------------------
*
*
*/

/********************************************************************/
/**
* \file
* \brief bl_application.h - application relatetd parts
*
*/



#ifndef BL_APPLICATION_H
#define BL_APPLICATION_H 1

/* constant definitions
---------------------------------------------------------------------------*/
#define APPL_FINGERPRINT_LENGTH 32u

/* data type definition
---------------------------------------------------------------------------*/

/** \brief application header */
typedef struct {
	FlashAddr_t applSize; 	/**< filled application flash size */
	uint16_t crcSum;				/**< crc sum of application flash without config block */
	uint16_t reserved1;
	uint32_t od1018Vendor;		/**< application - 0x1018:1 */
	uint32_t od1018Product;		/**< application - 0x1018:2 */
	uint32_t od1018Version;		/**< application - 0x1018:3 */
	uint32_t swVersion;			/**< software version - company specific, possible different from 0x1018:3 */
	uint8_t applFingerprintLength;
	uint8_t applFingerprint[APPL_FINGERPRINT_LENGTH];
} ConfigBlock_t;

/* prototypes 
*---------------------------------------------------------------------*/
FlashAddr_t applFlashStart(uint8_t nbr);
FlashAddr_t applFlashEnd(uint8_t nbr);
uint16_t applChecksum(uint8_t nbr);
BlRet_t applCheckConfiguration(uint8_t nbr);
BlRet_t applCheckChecksum(uint8_t nbr);
BlRet_t applCheckCRC32(uint8_t nbr, uint32_t crcSum32, uint32_t *calcSum32);
BlRet_t applCheckImage(void);
const ConfigBlock_t * applConfigBlock(uint8_t nbr);

#endif /* BL_APPLICATION_H */
