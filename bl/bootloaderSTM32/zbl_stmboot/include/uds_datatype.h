/*
* j1939_indication.h - contains defines for indications
*
*-------------------------------------------------------------------
*
*-------------------------------------------------------------------
*
*
*
*/

/**
* \brief data types
*
* \file j1939_indication.h - contains defines for indication
*
*/

#ifndef UDS_DATATYPE_H
#define UDS_DATATYPE_H 1


/* datatypes */



typedef enum  {
	UDS_SESSION_DEFAULT	= 0,
	UDS_SESSION_OTHER
}UDS_SESSION_T;



/* register functions */


#endif /* UDS_DATATYPE_H */

