/*
* bl_main.h
*
*-------------------------------------------------------------------
*
*-------------------------------------------------------------------
*
*
*/

/********************************************************************/
/**
* \file
* \brief customer specific Boot loader functionality
* - typical implemented in main.c
*
*/

#ifndef BL_MAIN_H
#define BL_MAIN_H 1

#include "stm32g0xx_hal.h"

#include <bl_type.h>
#include <co_datatype.h>

#define WDEXT_TIMEOUT          500U  /* $Klg> added refresh for External Watchdog every 500 ms */
#define FLASH_AFTER_RST_WINDOW  16U  /* $Klg> added small window (16 ms) when restarting bootloader */

void mainResumeLowPower(void);

void mainFlashTag(uint32_t tagAddress, uint8_t byte);

BlRet_t mainChecksumCheck(void);

void jump2Appl(void) ATTR_NORETURN;
void softwareReset(void) ATTR_NORETURN;

int bl_main(void);

#endif /* BL_MAIN_H */

