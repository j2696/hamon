﻿/*
* iuds_datatrans.h - contains internal defines for uds data transfer
*
*-----------------------------------------------------------------------------
*
*-----------------------------------------------------------------------------
*
*/

/**
* \file
* \brief internal defines for uds datatrans
*/

#ifndef IUDS_DATATRANS_H
#define IUDS_DATATRANS_H 1

/* function prototypes */

RET_T iudsDataTransMsgHandler(uint8_t addr, uint8_t *pData, uint16_t dataSize);

#endif /* IUDS_DATATRANS_H */
