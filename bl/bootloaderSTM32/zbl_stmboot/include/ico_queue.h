/*
* ico_queue.h - contains defines for internal queue handling
*
*-------------------------------------------------------------------
*
*-------------------------------------------------------------------
*
*
*/

/**
* \file
* \brief dataty type header
*/

#ifndef ICO_QUEUE_H
#define ICO_QUEUE_H 1


/* datatypes */

typedef struct {
	CO_SERVICE_T 	service;		/* canopen service */
	uint16_t		spec;			/* service specification */
	CO_CAN_REC_MSG_T	msg;		/* can message */
} CO_REC_DATA_T;


/* function prototypes */

BOOL_T	icoQueueGetReceiveMessage(CO_REC_DATA_T *pRecData);
RET_T	icoTransmitMessage(COB_REFERENZ_T cobRef,
			CO_CONST uint8_t *pData, uint8_t flags);
void	icoQueueHandler(void);
void	icoQueueDisable(BOOL_T on);
void	icoQueueDeleteInhibit(COB_REFERENZ_T cobRef);
void	icoQueueVarInit(CO_CONST uint16_t *recQueueCnt, CO_CONST uint16_t *trQueueCnt);
BOOL_T	icoQueueInhibitActive(COB_REFERENZ_T cobRef);
void	icoQueueSetSyncId(uint32_t syncId, uint8_t flags, BOOL_T enable);
uint32_t icoQueueGetTransBufFillState(void);

#endif /* ICO_QUEUE_H */

