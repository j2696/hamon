﻿/*
* j1939_indication.h - contains defines for indications
*
*-------------------------------------------------------------------
*
*-------------------------------------------------------------------
*
*
*
*/

/**
* \brief data types
*
* \file j1939_indication.h - contains defines for indication
*
*/

#ifndef UDS_INDICATION_H
#define UDS_INDICATION_H 1


/* datatypes */


/** \brief function pointer to data transfer related functions
 *
 * This function is called if new transfer download data are requested, received
 * or finished.
 *
 * \param actual
 *      pointer to data
 *      data size
 * \return
 *		UDS return code (ok = UDS_NRC_PR)
 */
typedef RET_T (* UDS_EVENT_DATA_TRANSFER_T)(uint8_t srcAddr, UDS_DATA_REQ_TYPE_T transType, uint8_t dataFormat, uint32_t memoryAddress, uint32_t memorySize, uint8_t *pData, uint16_t size);


/** \brief function pointer to security acess request seed
 *
 * This function is called if an security access seed request was received.
 *
 * \param actual
 *      address
 *      requested access type
 *      pointer for algorithm version
 *      pointer for key
 *      pointer for keylen
 * \return
 *		UDS response code (ok = UDS_NRC_PR)
 */
typedef RET_T (* UDS_EVENT_SECURITY_ACCESS_REQ_KEY_T)(uint8_t srcAddr, uint8_t accessType, uint8_t *pKey, uint16_t *pKeyLen);


/** \brief function pointer to check required session type
 *
 * This function is called to check if the requested action is allowed in current session.
 *
 * \param actual
 *      current session
 *      requested action
 * \return
 *		CO_TRUE (action is allowed)
 *      CO_FALSE (action is not allowed)
 */
typedef BOOL_T (* UDS_EVENT_CHECK_REQUIRED_SESSION_TYPE_T)(uint8_t currentSession, uint8_t requestAction);


/** \brief function pointer to routine control
 *
 * This function is called to execute the routine control.
 *
 * \param actual
 *      control type
 *      control identifier
 *		ppointer to additional data
 *		pointer to data size
 * \return
 *		UDS response code (ok = UDS_NRC_PR)
 */
typedef RET_T (* UDS_EVENT_ROUTINE_CTRL_T)(uint8_t srcAddr, uint8_t ctrltype, uint16_t identifier, uint8_t *pData, uint16_t *pDataSize);

/** \brief function pointer to data transmit
 *
 * This function is called to execute the data transmit functionality
 * (Read data by identifier, ...)
 *
 * \param actual
 *      source address
 *      read = 0, write = 1
 *      identifier
 *      pointer for response data
 *      pointer for response data len/max buffer length at calling
 * \return
 *		UDS response code (ok = RET_OK)
 */
typedef RET_T (* UDS_EVENT_DATA_TRANSMIT_T)(uint8_t srcAddr, uint8_t rw, uint16_t identifier, uint8_t *pData, uint16_t *pDataLen);


/** \brief function pointer to communication management
 *
 * This function is called to execute communication management requests
 *
 * Function can be called twice - for check and execute (see parameter execute)
 *
 * \param actual
 *      check = 0, execute = 1 - check only or execute command
 *      source address
 *      request type
 *      sub function
 *      pointer to optional data
 *      optional data size
 * \return
 *		UDS response code (ok = UDS_NRC_PR)
 */
typedef RET_T (* UDS_EVENT_REQ_COMMGT_T)(BOOL_T execute, uint8_t srcAddr, uint8_t reqType, uint8_t subFct, uint8_t *pData, uint16_t dataLen);


typedef RET_T (* UDS_EVENT_REQ_STORED_T)(uint8_t srcAddr, uint8_t reqType, uint8_t subFct);




/* register functions */

RET_T udsEventRegister_UDS_DATA_TRANSFER(UDS_EVENT_DATA_TRANSFER_T pFct);
RET_T udsEventRegister_UDS_SECURITY_ACCESS_REQ_KEY(UDS_EVENT_SECURITY_ACCESS_REQ_KEY_T pFct);
RET_T udsEventRegister_UDS_STORE_MSG(UDS_EVENT_REQ_STORED_T pFct);
RET_T udsEventRegister_UDS_REQ_COMMGT(UDS_EVENT_REQ_COMMGT_T pFct);
RET_T udsEventRegister_UDS_CHECK_REQUIRED_SESSION(UDS_EVENT_CHECK_REQUIRED_SESSION_TYPE_T pFct);
RET_T udsEventRegister_UDS_ROUTINE_CTRL(UDS_EVENT_ROUTINE_CTRL_T pFct);
RET_T udsEventRegister_UDS_READ_WRITE_ID(UDS_EVENT_DATA_TRANSMIT_T pFct);

#endif /* UDS_INDICATION_H */

