/*
* BL_DEBUG.c
*
*-------------------------------------------------------------------
*
*-------------------------------------------------------------------
*
*
*/

/********************************************************************/
/**
* \file
* \brief additional debug functionality
*
*/



/* header of standard C - libraries
---------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdint.h>


/* header of project specific types
---------------------------------------------------------------------------*/
#include <bl_config.h>

#ifdef BL_DEBUG
#include <bl_type.h>
#include <bl_can.h>
//#include <bl_debug.h>

/* constant definitions
---------------------------------------------------------------------------*/

/* local defined data types
---------------------------------------------------------------------------*/
//typedef unsigned long uintptr_t;


/* list of external used functions, if not in headers
---------------------------------------------------------------------------*/

/* list of global defined functions
---------------------------------------------------------------------------*/

/* list of local defined functions
---------------------------------------------------------------------------*/

/* external variables
---------------------------------------------------------------------------*/

/* global variables
---------------------------------------------------------------------------*/

/* local defined variables
---------------------------------------------------------------------------*/


/***************************************************************************/
/**
* \brief print a string
*
*/

void bl_puts(
		const char * s
	)
{
	(void)s;
	printf("%s", s);
}

/***************************************************************************/
/**
* \brief print a 8bit hex
*
*/

void bl_hex8(
		U8 c
	)
{
	(void)c;
	printf("%02x", (unsigned int)c);
}

/***************************************************************************/
/**
* \brief print a 16bit hex
*
*/

void bl_hex16(
		U16 i
	)
{
	(void)i;
	printf("%04x", (unsigned int)i);
}

/***************************************************************************/
/**
* \brief print a 32bit hex
*
*/

void bl_hex32(
		U32 l
	)
{
	(void)l;
	printf("%08lx", (unsigned long int)l);
}

/***************************************************************************/
/**
* \brief print a pointer hex
*
*/

void bl_ptr(
		const void * const p
	)
{
	(void)p;
	printf("%p", p);
}


/***************************************************************************/
/**
* \brief print a memory dump
*
*/
void bl_dump(
		const U8 * pStart, /**< start pointer */
		U16 iCnt /**< number of bytes */
	)
{
U16 i;
U8 fAddr = 1; /* print Address */

	for( i = 0; i < iCnt; i++)  {
		/* new line after every 16 Bytes */
		if (((uintptr_t)&pStart[i] & 0x0F) == 0)  {
			fAddr = 1;
		}

		if (fAddr != 0)  {
			bl_puts("\n");
			bl_ptr(&pStart[i]);
			bl_puts(": ");
			fAddr = 0;
		}

		bl_hex8(pStart[i]);
		bl_puts(" ");
	}

	bl_puts("\n");
}

/***************************************************************************/
/**
* \brief print size of datatypes
*
*/
void bl_printDatatypes(void)
{
	printf("Test\n");
	printf("U8  - %d\n", (int)sizeof(U8));
	printf("U16 - %d\n", (int)sizeof(U16));
	printf("U32 - %d\n", (int)sizeof(U32));

	printf("CanData_t - %d\n", (int)sizeof(CanData_t));
	printf("CoOd_t - %d\n", (int)sizeof(CoOd_t));

	/* printTable();*/

}

#endif


