﻿/*
* uds_routinectrl.c - contains uds routine control functions
*
*-------------------------------------------------------------------
*
*-------------------------------------------------------------------
*
*
*/

/********************************************************************/
/**
* \file uds_routinectrl.c
* \brief uds routine control handling
*
*/

/* header of standard C - libraries
-----------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <gen_define.h>
#include <co_datatype.h>
#include <co_timer.h>
#include <iso_tp.h>

#include <uds_datatype.h>
#include <uds_uds.h>
#include <uds_indication.h>

#include "iuds_core.h"
#include "iuds_comm.h"
#include "iuds_routinectrl.h"

/* header of project specific types
-----------------------------------------------------------------------------*/

/* constant definitions
-----------------------------------------------------------------------------*/

/* local defined data types
-----------------------------------------------------------------------------*/

/* list of external used functions, if not in headers
-----------------------------------------------------------------------------*/

/* list of global defined functions
-----------------------------------------------------------------------------*/

/* list of local defined functions
-----------------------------------------------------------------------------*/
static uint8_t routineCtrlStart(
	uint8_t addr,
	uint8_t *pData,
	uint16_t *pDataSize
    );

/* external variables
-----------------------------------------------------------------------------*/

/* global variables
-----------------------------------------------------------------------------*/

/* local defined variables
-----------------------------------------------------------------------------*/
static UDS_EVENT_ROUTINE_CTRL_T pRoutineCtrlFct = NULL;

/*****************************************************************************/
/*****************************************************************************/
/* SID = 31 - routine control start */
RET_T iudsRoutineControlHandler(
		uint8_t addr,
		uint8_t* pData,
		uint16_t dataSize
	)
{
uint8_t retCode;

    /* errors goto default ... */
    switch (pData[1u]) {
    case UDS_ROUTINE_CTRL_START:
    	retCode = routineCtrlStart(addr, pData, &dataSize);
    	if (retCode == UDS_NRC_PR)  {
    		// pos ACQ
            pData[0U] = UDS_RESP_ROUTINE_CTRL;
            dataSize += 4U;
    	} else {
        // neg ACQ
//	    pData[0] = UDS_NRC_SID;
//	    pData[1] = UDS_ROUTINE_CTRL_START;
//	    pData[2] = retCode;
//	    dataSize = 3u;
            iudsSendAbort(addr, pData, retCode, 0u);
            return (RET_OK);
    	}
    	break;

    default:
    	break;
    }

    iudsSendResponse(addr, pData, dataSize, 0u);

    return (RET_OK);
}


static uint8_t routineCtrlStart(
		uint8_t addr,
		uint8_t *pData,
		uint16_t *pDataSize
    )
{
const uint8_t requiredSession[3] = {UDS_SUB_SESSION_PRGS, UDS_SUB_SESSION_EXTDS, UDS_SUB_SESSION_SSDS};
const uint8_t requiredSecurity[5] = {UDS_SEC_ACCESS_KEY_LVL_1, UDS_SEC_ACCESS_KEY_LVL_2, UDS_SEC_ACCESS_KEY_LVL_3, UDS_SEC_ACCESS_KEY_LVL_4, UDS_SEC_ACCESS_KEY_LVL_5};
uint8_t retCode = UDS_NRC_SFNSIAS;
uint16_t routineIdent;
uint16_t dataSize;

	dataSize = *pDataSize;

	if (dataSize < 4U)  {
		/* should never happen */
		return (UDS_NRC_GR);
	}

    routineIdent = ((uint16_t) pData[2U]) << 8U;
    routineIdent |= pData[3U];

    if (routineIdent != UDS_RID_FLASH_AFTER_RESET)
    {
        if (iudsCheckRequiredSession(pData[1] & 0x7f, &requiredSession[0], 3) == CO_FALSE)  {
            // neg ACQ security access denied
            return (UDS_NRC_SAD);
        }
    }

    if (pRoutineCtrlFct != NULL)
    {
    	/* check security level */
    	if (routineIdent == UDS_RID_ERASE_MEMORY)  {
    	    if (iudsCheckSecurityLevel(requiredSecurity, 5) == CO_FALSE)  {
        		return (UDS_NRC_SAD);
    	    }
    	}

    	if (routineIdent == UDS_RID_CHK_PROG_DEPENDS)  {
    	    if (iudsCheckSecurityLevel(requiredSecurity, 5) == CO_FALSE)  {
        		return (UDS_NRC_SAD);
    	    }
    	}

		dataSize = dataSize - 4u;

		retCode = pRoutineCtrlFct(addr, UDS_ROUTINE_CTRL_START, routineIdent, &pData[0], (uint16_t *)&dataSize);

		*pDataSize = dataSize;
    }

    return (retCode);
}


RET_T udsEventRegister_UDS_ROUTINE_CTRL(UDS_EVENT_ROUTINE_CTRL_T pFct)
{
    pRoutineCtrlFct = pFct;
    return (RET_OK);
}
