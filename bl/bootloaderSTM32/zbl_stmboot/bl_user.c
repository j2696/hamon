/*
* bl_user.c
*
*-----------------------------------------------------------------------------
*
*-----------------------------------------------------------------------------
*
*/

/******************************************************************************
* \file bl_user.c
* \brief User specific functionality
*/

/* header of standard C - libraries
-----------------------------------------------------------------------------*/
#include <stddef.h>
#include <string.h>

/* header of project specific types
-----------------------------------------------------------------------------*/
#include <bl_config.h>
#include <bl_type.h>
#include <bl_user.h>
#include <bl_application.h>
#include <bl_call.h>

/* constant definitions
-----------------------------------------------------------------------------*/

/* local defined data types
-----------------------------------------------------------------------------*/

/* list of external used functions, if not in headers
-----------------------------------------------------------------------------*/

/* list of global defined functions
-----------------------------------------------------------------------------*/

/* list of local defined functions
-----------------------------------------------------------------------------*/

/* external variables
-----------------------------------------------------------------------------*/

/* global variables
-----------------------------------------------------------------------------*/

#ifdef BL_DBG_RAM
uint8_t bl_dbgvect[DBG_RAM_MAX_BYTES] __attribute__((section("dbgvect")));	/* GCC */
uint8_t bl_dbg_cnt = 0U;
#endif

uint8_t bl_sharedm[SHAREDM_MAX_BYTES] __attribute__((section("sharedm")));	/* GCC */


/* local defined variables
-----------------------------------------------------------------------------*/



/* debug in ram
-----------------------------------------------------------------------------*/
void DBG_RAM_Init(void)
{
#ifdef BL_DBG_RAM
    memset(bl_dbgvect, 0xCC, DBG_RAM_MAX_BYTES);
#endif
}
void DBG_RAM_Trace_1(uint16_t a1)
{
#ifdef BL_DBG_RAM
    if (bl_dbg_cnt < DBG_RAM_MAX_BYTES) {
        bl_dbgvect[bl_dbg_cnt] = (uint8_t) (0x00FF & a1);
        bl_dbg_cnt++;
    }
#else
    (void)a1;
#endif
}
void DBG_RAM_Trace_2(uint16_t a1, uint16_t a2)
{
    DBG_RAM_Trace_1(a1);
    DBG_RAM_Trace_1(a2);
}
void DBG_RAM_Trace_3(uint16_t a1, uint16_t a2, uint16_t a3)
{
    DBG_RAM_Trace_2(a1, a2);
    DBG_RAM_Trace_1(a3);
}
void DBG_RAM_Trace_4(uint16_t a1, uint16_t a2, uint16_t a3, uint16_t a4)
{
    DBG_RAM_Trace_2(a1, a2);
    DBG_RAM_Trace_2(a3, a4);
}





/***************************************************************************
 * \brief userGetBitrate - returns a valid bitrate
 *
 * valid values are:
 * 		125 .. 125kbit/s
 *      250 .. 250kbit/s
 *      500 .. 500kbit/s
 *     1000 .. 1Mbit/s
 * \retval
 *  bitrate in kbit/s
 */
uint16_t userGetBitrate(void)
{
#ifdef CFG_CAN_BACKDOOR
    BlBackdoor_t backdoorState;

	backdoorState = getBackdoorCommand();
	if ( (backdoorState == BL_BACKDOOR_ACTIVATE) /* backdoor active */
		 || (backdoorState == BL_BACKDOOR_USED) ) /* backdoor was used */
	{
		return (250u); /* should be fix */
	}
#endif

	/* user bitrate, e.g. from eeprom */
	return (250u);
}


/***************************************************************************
* Object dictionary values                                               
*/

/***************************************************************************
* Object 0x1018:1 vendor id
* Configure your vendor id. 
*/
#ifdef COBL_OD_1018_1_FCT
uint32_t userOd1018_1(void)
{
	return (0x12345678); // $klg> customized vendor id
}
#endif /* COBL_OD_1018_1_FCT */

/***************************************************************************
* object 1018:2 productcode
* The productcode is vendorspecific - configure 0x1018:1, too.
* If the feature BL_CHECK_PRODUCTID is used this value is used to compare 
* with the Config header information.
*/
#ifdef COBL_OD_1018_2_FCT
uint32_t userOd1018_2(void)
{
	return (0xAABBCCDD); // $klg> customized product id
}
#endif /* COBL_OD_1018_2_FCT */


/***************************************************************************
* object 0x1018:3 revision 
*/
#ifdef COBL_OD_1018_3_FCT
uint32_t userOd1018_3(void)
{
	return (1ul << 16) | 1ul; /* e.g. version 1.01 */
}
#endif /* COBL_OD_1018_3_FCT */


/***************************************************************************
* object 0x1018:4 serial number
*/
#ifdef COBL_OD_1018_4_FCT
uint32_t userOd1018_4(void)
{
	return (0u);
}
#endif /* COBL_OD_1018_4_FCT */
