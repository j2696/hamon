/*
* uds_stored.c - contains uds stored data transmission functions
*
*-------------------------------------------------------------------
*
*-------------------------------------------------------------------
*
*
*/

/********************************************************************/
/**
* \file uds_stored.c
* \brief main uds routines
*
*/

/* header of standard C - libraries
-----------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <gen_define.h>
#include <co_datatype.h>
#include <co_timer.h>
#include <iso_tp.h>

#include <uds_datatype.h>
#include <uds_uds.h>
#include <uds_indication.h>

#include "iuds_stored.h"
#include "iuds_core.h"

/* header of project specific types
-----------------------------------------------------------------------------*/

/* constant definitions
-----------------------------------------------------------------------------*/

/* local defined data types
-----------------------------------------------------------------------------*/

/* list of external used functions, if not in headers
-----------------------------------------------------------------------------*/

/* list of global defined functions
-----------------------------------------------------------------------------*/

/* list of local defined functions
-----------------------------------------------------------------------------*/

/* external variables
-----------------------------------------------------------------------------*/

/* global variables
-----------------------------------------------------------------------------*/

/* local defined variables
-----------------------------------------------------------------------------*/
static UDS_EVENT_REQ_STORED_T pUdsStoreMsgFct = NULL;

/*****************************************************************************/
/*****************************************************************************/
RET_T iudsStoredMsgHandler(
		uint8_t addr,
		uint8_t* pData,
		uint16_t dataSize
	)
{

	switch (pData[0u]) {
	case UDS_REQ_CDTCI:
		if (dataSize != 4)  {
			// TODO abort NRC 13
		}
		//TODO session abort
		iudsStoreMsgRecv(addr, pData);
		dataSize = 1u;
		break;
	default:
		break;
	}

	iudsSendResponse(addr, pData, dataSize, 0u);

	return (RET_OK);
}



RET_T iudsStoreMsgRecv(
		uint8_t addr,
		uint8_t* pData
	)
{
uint8_t sub;

	sub = pData[1];
	/* remove no respond bit */
	sub = sub & 0x7f;

	if (pUdsStoreMsgFct != NULL) {
		pUdsStoreMsgFct(addr, pData[0], sub);
	}

	return (RET_OK);
}







/**
*
* \brief udsEventRegister_UDS_MSG_RECV - register uds callback
*
* This function registered the uds function.
* It is called, after a uds request was received.
*
* Please note - only one uds function can be registered.
*
* return RET_T
*/
RET_T udsEventRegister_UDS_STORE_MSG(
		UDS_EVENT_REQ_STORED_T pFct
	)
{
	pUdsStoreMsgFct = pFct;

	return (RET_OK);
}

