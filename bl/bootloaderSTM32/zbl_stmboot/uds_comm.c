﻿/*
* uds_comm.c - contains main uds functions
*
*-----------------------------------------------------------------------------
*
*-----------------------------------------------------------------------------
*
*/

/******************************************************************************
* \file uds_comm.c
* \brief main uds routines
*/

/* header of standard C - libraries
-----------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <gen_define.h>
#include <co_datatype.h>
#include <co_timer.h>
#include <iso_tp.h>

#include <uds_datatype.h>
#include <uds_uds.h>
#include <uds_indication.h>
#include "iuds_core.h"
#include "iuds_comm.h"
#include "iuds_routinectrl.h"

/* header of project specific types
-----------------------------------------------------------------------------*/

/* constant definitions
-----------------------------------------------------------------------------*/
#ifndef UDS_SESSION_TIMEOUT
#define UDS_SESSION_TIMEOUT 0ul
#endif /* UDS_SESSION_TIMEOUT */

/* local defined data types
-----------------------------------------------------------------------------*/

/* list of external used functions, if not in headers
-----------------------------------------------------------------------------*/

/* list of global defined functions
-----------------------------------------------------------------------------*/

/* list of local defined functions
-----------------------------------------------------------------------------*/
static RET_T reqSecAccessKey(uint8_t addr, uint8_t *pData, uint16_t dataSize);
static RET_T commMsgInd(BOOL_T execute, uint8_t addr, uint8_t* pData, uint16_t dataSize);
static RET_T ctrlDtcSettings(uint8_t addr, uint8_t *pData, uint16_t dataSize);
static RET_T testerPresent(uint8_t addr, uint8_t *pData);
static RET_T reqEcuReset(uint8_t addr, uint8_t *pData);
static RET_T communicationControl(uint8_t addr, uint8_t *pData, uint16_t dataSize);

static void sessionTimeOutFct(void *pData);

/* external variables
-----------------------------------------------------------------------------*/

/* global variables
-----------------------------------------------------------------------------*/

/* local defined variables
-----------------------------------------------------------------------------*/

static uint16_t p2ServerMax;
static uint16_t p2StarServerMax;

static uint8_t powerDownTime = 0xff;
static uint8_t diagnosticSessionType = UDS_SUB_SESSION_DS;
static uint16_t secAccessSeedDataLen;

static uint8_t securityAccess = 0u;

static UDS_EVENT_REQ_COMMGT_T pUdsReqCommFct = NULL;
static UDS_EVENT_SECURITY_ACCESS_REQ_KEY_T secAccessKeyUserFct = NULL;
static UDS_EVENT_CHECK_REQUIRED_SESSION_TYPE_T pCheckRequiredSessionUserFct = NULL;

static CO_TIMER_T sessionTimer;


/*****************************************************************************/
/*****************************************************************************/
RET_T iudsCommMsgHandler(
        uint8_t addr,
        uint8_t* pData,
        uint16_t dataSize
    )
{
RET_T retVal = RET_OK;
uint8_t subFct;

    switch (pData[0u]) {
    case UDS_REQ_SESSION_CONTROL:
        retVal = commMsgInd(CO_TRUE, addr, pData, 2u);
        if (retVal == RET_OK)  {
            /* save session control state */
            diagnosticSessionType = pData[1] & 0x7fu;
            (void)iudsCommRestartTimeOut();

            /* pos ACQ */
            pData[2] = ((p2ServerMax >> 8) & 0xff);
            pData[3] = (p2ServerMax & 0xff);
            pData[4] = ((p2StarServerMax >> 8) & 0xff);
            pData[5] = (p2StarServerMax & 0xff);
            dataSize = 6u;
            (void)iudsSendResponse(addr, pData, dataSize, MSG_INDICATION);

        } else {
            /* neg ACQ */
            (void)iudsSendRetAbort(addr, pData, retVal, 0u);
        }
        break;

    case UDS_REQ_SEC_ACCESS_SEED:
        retVal = reqSecAccessKey(addr, pData, dataSize);
        if (retVal == RET_OK)  {
            /* pos ACQ */
            pData[0] = UDS_RESP_SEC_ACCESS_SEED;
            pData[1] = pData[1] & 0x7f;
            if (secAccessSeedDataLen > UDS_SECURITY_SEED_KEY_LEN)  {
                secAccessSeedDataLen = UDS_SECURITY_SEED_KEY_LEN;
            }
            dataSize = 2u + secAccessSeedDataLen;
        } else {
            /* neg ACQ */
            (void)iudsSendRetAbort(addr, pData, retVal, 0u);
        }
        break;

    case UDS_REQ_ECU_RESET:
        subFct = pData[1];
        pData[1] &= 0x7f;
        retVal = reqEcuReset(addr, pData);
        if (retVal == RET_OK)  {
            pData[2] = powerDownTime;
            dataSize = 2u;
            if ((subFct & 0x7f) == 0x04)  {
                dataSize = 3u;
            }
            if ((subFct & UDS_SUB_NO_RESP_BIT) == 0u) {
                iudsSendResponse(addr, pData, dataSize, MSG_INDICATION);
            } else {
                pData[0] = UDS_RESP_ECU_RESET;
                (void)iudsCommMsgAck(addr, pData);
            }
        } else {
            (void)iudsSendRetAbort(addr, pData, retVal, 0u);
        }
        break;

    case UDS_REQ_TESTER_PRESENT:
        testerPresent(addr, pData);
        break;

    case UDS_REQ_ControlDTCSetting:
        ctrlDtcSettings(addr, pData, dataSize);
        break;

    case UDS_REQ_COMM_CONTROL:
        communicationControl(addr, pData, dataSize);
        break;

    default:
        break;
    }

    iudsSendResponse(addr, pData, dataSize, 0u);

    return (retVal);
}


RET_T iudsCommRestartTimeOut(
        void
    )
{
    if (diagnosticSessionType != UDS_SUB_SESSION_DS)  {
        (void)coTimerStart(&sessionTimer, UDS_SESSION_TIMEOUT * 1000ul, sessionTimeOutFct, NULL, CO_TIMER_ATTR_ROUNDDOWN);
    }

    return (RET_OK);
}



void iudsCommEventSession(
        void* pData
    )
{
    if (pUdsReqCommFct != NULL) {
        (void)pUdsReqCommFct(CO_TRUE, 0u, UDS_REQ_SESSION_CONTROL, diagnosticSessionType, NULL, 0u);
    }
    if (diagnosticSessionType != UDS_SUB_SESSION_DS)  {
        (void)coTimerStart(&sessionTimer, UDS_SESSION_TIMEOUT * 1000ul, sessionTimeOutFct, NULL, CO_TIMER_ATTR_ROUNDDOWN);
    }

    (void)pData;
}


static void sessionTimeOutFct(void *pData)
{
    (void)pData; /* Remove Warning */

    /* //$klg> avoiding the change of session type on timeout event.
    diagnosticSessionType = UDS_SUB_SESSION_DS;
    if (pUdsReqCommFct != NULL) {
        (void)pUdsReqCommFct(CO_TRUE, 0u, UDS_REQ_SESSION_CONTROL, diagnosticSessionType, NULL, 0u);
    }
    */
}


static RET_T commMsgInd(
        BOOL_T execute,
        uint8_t addr,
        uint8_t* pData,
        uint16_t dataSize
    )
{
uint8_t sub;
RET_T retVal = RET_OK;

    sub = pData[1];
    /* remove no respond bit */
    sub = sub & 0x7f;

    if (pUdsReqCommFct == NULL) {
        return (retVal);
    }

    if (dataSize > 2)  {
        retVal = pUdsReqCommFct(execute, addr, pData[0], pData[1] & 0x7fu, &pData[2], (uint16_t)(dataSize - 2u));
    } else {
        retVal = pUdsReqCommFct(execute, addr, pData[0], pData[1] & 0x7fu, NULL, 0);
    }

    return (retVal);
}



static RET_T reqEcuReset(
        uint8_t addr,
        uint8_t* pData
    )
{
uint8_t sub;
RET_T retVal = RET_OK;
const uint8_t hardResetRequiredSession[3] = {UDS_SUB_SESSION_PRGS, UDS_SUB_SESSION_EXTDS, UDS_SUB_SESSION_SSDS};
const uint8_t onOffResetRequiredSession[3] = {UDS_SUB_SESSION_EXTDS, UDS_SUB_SESSION_SSDS};
const uint8_t hardResetRequiredSecurity[4] = {UDS_SEC_ACCESS_KEY_LVL_1, UDS_SEC_ACCESS_KEY_LVL_3, UDS_SEC_ACCESS_KEY_LVL_4, UDS_SEC_ACCESS_KEY_LVL_5};
const uint8_t onOffResetRequiredSecurity[4] = {UDS_SEC_ACCESS_KEY_LVL_1, UDS_SEC_ACCESS_KEY_LVL_3, UDS_SEC_ACCESS_KEY_LVL_4, UDS_SEC_ACCESS_KEY_LVL_5};

    /*sub = pData[1];
    // remove no respond bit
    sub = sub & 0x7f;

    if (sub == UDS_SUB_RESET_HR)  {  // hard reset
        if (iudsCheckRequiredSession(pData[0], &hardResetRequiredSession[0], 3) == CO_FALSE)  {
            return (RET_UDS_NRC_CNC);
        }
        if (iudsCheckSecurityLevel(hardResetRequiredSecurity, 4)  == CO_FALSE)  {
            return (RET_UDS_NRC_SAD);
        }
    }

    // on off reset
    if (sub == UDS_SUB_RESET_KOFFONR)  {
        if (iudsCheckRequiredSession(pData[0], &onOffResetRequiredSession[0], 2) == CO_FALSE)  {
            return (RET_UDS_NRC_CNC);
        }
        if (iudsCheckSecurityLevel(onOffResetRequiredSecurity, 4)  == CO_FALSE)  {
            return (RET_UDS_NRC_SAD);
        }
    }*/

    retVal = commMsgInd(CO_FALSE, addr, pData, 2u);

    return (retVal);
}


RET_T ctrlDtcSettings(
        uint8_t addr,
        uint8_t *pData,
        uint16_t dataSize
    )
{
const uint8_t requiredSession[2] = {UDS_SUB_SESSION_EXTDS, UDS_SUB_SESSION_SSDS};
RET_T retVal;

    if (iudsCheckRequiredSession(pData[0], &requiredSession[0], 2) == CO_FALSE)  {
         return (RET_ABORTED);
    }

    retVal = commMsgInd(CO_TRUE, addr, pData, dataSize);
    return (retVal);
}


RET_T communicationControl(
        uint8_t addr,
        uint8_t *pData,
        uint16_t dataSize
    )
{
const uint8_t requiredSession[2] = {UDS_SUB_SESSION_EXTDS, UDS_SUB_SESSION_SSDS};
RET_T retVal;

    if (iudsCheckRequiredSession(pData[0], &requiredSession[0], 2) == CO_FALSE)  {
         return (RET_ABORTED);
    }

    retVal = commMsgInd(CO_TRUE, addr, pData, dataSize);
    return (retVal);
}


RET_T testerPresent(
        uint8_t addr,
        uint8_t *pData
    )
{
const uint8_t requiredSession[4] = {UDS_SUB_SESSION_DS, UDS_SUB_SESSION_PRGS, UDS_SUB_SESSION_EXTDS, UDS_SUB_SESSION_SSDS};
RET_T retVal;

    if (iudsCheckRequiredSession(pData[0], &requiredSession[0], 4) == CO_FALSE)  {
         return (RET_ABORTED);
    }

    retVal = commMsgInd(CO_TRUE, addr, pData, 2u);
    return (retVal);
}


RET_T iudsCommMsgAck(uint8_t addr, uint8_t* pData)
{
    switch (pData[0])
    {
    case UDS_RESP_SESSION_CONTROL:
    case UDS_RESP_ECU_RESET:
        commMsgInd(CO_TRUE, addr, pData, 2u);
        break;
    default:
        return (RET_INTERNAL_ERROR);
    }

    return (RET_OK);
}


/***************************************************************************/
RET_T udsCommSetSessionTime(
        uint16_t p2ServerMaxMs,
        uint16_t p2StarServerMaxMs
    )
{
    p2ServerMax = p2ServerMaxMs;
    p2StarServerMax = p2StarServerMaxMs / 10u;

    return (RET_OK);
}


/***************************************************************************/
void udsCommSetPowerDownTime(
        uint8_t time        /**< Powerdown Time in s*/
    )
{
    powerDownTime = time;
}


/***************************************************************************/
static RET_T reqSecAccessKey(
        uint8_t addr,
        uint8_t *pData,
        uint16_t dataSize
    )
{
RET_T retVal = RET_OK;
static uint8_t lastAccessType;
uint8_t accessType;
const uint8_t requiredSession[3] = {UDS_SUB_SESSION_PRGS, UDS_SUB_SESSION_EXTDS, UDS_SUB_SESSION_SSDS};

    securityAccess = 0u;

    accessType = pData[1] & 0x7f;

    if (iudsCheckRequiredSession(accessType, &requiredSession[0], 3) == CO_FALSE)  {
        return (RET_UDS_NRC_SAD);
    }

    if (secAccessKeyUserFct == NULL)  {
        return (RET_UDS_NRC_SFNS);
    } else {
        switch (pData[1] & 0x7f)  {
        case UDS_SEC_ACCESS_SEED_LVL_1: /* Req key */
        case UDS_SEC_ACCESS_SEED_LVL_2:
        case UDS_SEC_ACCESS_SEED_LVL_3:
        case UDS_SEC_ACCESS_SEED_LVL_4:
        case UDS_SEC_ACCESS_SEED_LVL_5:
            retVal = secAccessKeyUserFct(addr, pData[1] & 0x7f, &pData[2u], &secAccessSeedDataLen);
            break;

        case UDS_SEC_ACCESS_KEY_LVL_1:  /* send key */
        case UDS_SEC_ACCESS_KEY_LVL_2:
        case UDS_SEC_ACCESS_KEY_LVL_3:
        case UDS_SEC_ACCESS_KEY_LVL_4:
        case UDS_SEC_ACCESS_KEY_LVL_5:
        /* expect 2 or more data.. */
            if (dataSize < 4)  {
                retVal = RET_UDS_NRC_IMLOIF;
            } else {
                if (((accessType == UDS_SEC_ACCESS_KEY_LVL_1) && (lastAccessType == UDS_SEC_ACCESS_SEED_LVL_1))
                 || ((accessType == UDS_SEC_ACCESS_KEY_LVL_2) && (lastAccessType == UDS_SEC_ACCESS_SEED_LVL_2))
                 || ((accessType == UDS_SEC_ACCESS_KEY_LVL_3) && (lastAccessType == UDS_SEC_ACCESS_SEED_LVL_3))
                 || ((accessType == UDS_SEC_ACCESS_KEY_LVL_4) && (lastAccessType == UDS_SEC_ACCESS_SEED_LVL_4))
                 || ((accessType == UDS_SEC_ACCESS_KEY_LVL_5) && (lastAccessType == UDS_SEC_ACCESS_SEED_LVL_5)))  {
                    retVal = secAccessKeyUserFct(addr, pData[1] & 0x7f, &pData[2u], &secAccessSeedDataLen);
                } else {
                    retVal = RET_UDS_NRC_CNC;
                }

                if (retVal == RET_OK)  {
                    /* save validated state */
                    securityAccess = pData[1] & 0x7f;
                }
            }
            break;

        default:
            retVal = RET_UDS_NRC_ROOR;
            break;
        }
    }

    lastAccessType = pData[1] & 0x7f;

    return (retVal);
}


BOOL_T iudsCheckSecurityLevel(const uint8_t *reqSesurityTypes, uint8_t cnt)
{
int i;

    for (i = 0; i < cnt; i++)  {
        if (securityAccess == reqSesurityTypes[i])  {
            return (CO_TRUE);
        }
    }

    return (CO_TRUE); //$Klg> original ::: return (CO_FALSE);
}


BOOL_T iudsCheckRequiredSession(uint8_t accessType, const uint8_t *reqSessionTypes, uint8_t cnt)
{
int i;

    for (i = 0; i < cnt; i++)  {
        if (diagnosticSessionType == reqSessionTypes[i])  {
            return (CO_TRUE);
        }
    }

    /* ask application */
    if (pCheckRequiredSessionUserFct != NULL)  {
        if (pCheckRequiredSessionUserFct(diagnosticSessionType, accessType))  {
            return (CO_TRUE);
        }
    }
    return (CO_FALSE);
}


RET_T udsCommSetDiagnosticSessionType(
        uint8_t sessionType
    )
{
    diagnosticSessionType = sessionType;
    (void)iudsCommRestartTimeOut();
    return (RET_OK);
}


uint8_t udsCommGetDiagnosticSessionType(
        void
    )
{
    return (diagnosticSessionType);
}


RET_T udsCommSetSecurityLevel(
        uint8_t securityLevel
    )
{
RET_T retVal;

    retVal = RET_OK;
    securityLevel *= 2u;

    switch(securityLevel)  {
    case UDS_SEC_ACCESS_KEY_LVL_1:
    case UDS_SEC_ACCESS_KEY_LVL_2:
    case UDS_SEC_ACCESS_KEY_LVL_3:
    case UDS_SEC_ACCESS_KEY_LVL_4:
    case UDS_SEC_ACCESS_KEY_LVL_5:
        securityAccess = securityLevel;
        break;
    default:
        retVal = RET_PARAMETER_INCOMPATIBLE;
        break;
    }

    return (retVal);
}

/**
*
* \brief udsEventRegister_UDS_MSG_RECV - register uds callback
*
* This function registered the uds function.
* It is called, after a uds request was received.
*
* Please note - only one uds function can be registered.
*
* return RET_T
*/
RET_T udsEventRegister_UDS_REQ_COMMGT(
        UDS_EVENT_REQ_COMMGT_T pFct
    )
{
    pUdsReqCommFct = pFct;

    return (RET_OK);
}


/**
*
* \brief udsEventRegister_UDS_SECURITY_ACCESS_REQ_KEY - register uds callback
*
* This function registered the uds function.
* It is called, after a uds security access was received.
*
* Please note - only one uds function can be registered.
*
* return RET_T
*/
RET_T udsEventRegister_UDS_SECURITY_ACCESS_REQ_KEY(
        UDS_EVENT_SECURITY_ACCESS_REQ_KEY_T pFct
    )
{
    secAccessKeyUserFct = pFct;

    return (RET_OK);
}


/**
*
* \brief udsEventRegister_UDS_CHECK_REQUIRED_SESSION - register uds callback
*
* This function registered the uds function.
* It is called to check if the action is supported in a session
*
* Please note - only one uds function can be registered.
*
* return RET_T
*/
RET_T udsEventRegister_UDS_CHECK_REQUIRED_SESSION(
        UDS_EVENT_CHECK_REQUIRED_SESSION_TYPE_T pFct
    )
{
    pCheckRequiredSessionUserFct = pFct;

    return (RET_OK);
}

