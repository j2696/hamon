/*
*  bootloader Low Power Mode Standby
*
*-------------------------------------------------------------------------------
*
*-------------------------------------------------------------------------------
*
*/

#ifndef BL_LPMS_H
#define BL_LPMS_H 1

/* BL__LPMS - default: off
 * If selected, when COMMAND_SLEEP is present in RAM command memory area, enables
 * the low power mode entering feature (Standby + awaking on PC5 rising edge)
*/
#define BL__LPMS 1

#define BL_AWAKE   1
#define BL_ASLEEP  0

int isCommandUcuSleepUSLE(void);

int enterLowPowerStandby(void);

#endif /* BL_LPMS_H */
