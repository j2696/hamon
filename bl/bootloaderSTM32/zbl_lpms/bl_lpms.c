/*
* bl_lpms.c
*
*-------------------------------------------------------------------------------
*
*-------------------------------------------------------------------------------
*
*/

/******************************************************************************
* \brief Low power mode specific functionality
*
* \file bl_lpms.c
*
*/

/* hardware specific includes and settings
------------------------------------------------------------------------------*/

/* standard includes
------------------------------------------------------------------------------*/
#include <string.h>

/* header of project specific types
------------------------------------------------------------------------------*/
#include <bl_type.h>
#include <bl_call.h>
#include <bl_lpms.h>

#include <stm32g0xx.h>
#include <stm32g0xx_hal_pwr.h>
#include <stm32g0xx_hal_pwr_ex.h>
#include <stm32g0xx_hal_rtc.h>
#include <stm32g0xx_hal_rtc_ex.h>

/* constant definitions
------------------------------------------------------------------------------*/
#define COMMAND_SLEEP "USLE"
#define COMMAND_DELSLEEP "NNNN"
#define REQLPMS_ON   0
#define REQLPMS_OFF  1
#define TRTC_SLEEP_STBY  0x05D0

/* local defined data types
------------------------------------------------------------------------------*/

/* list of external used functions, if not in headers
------------------------------------------------------------------------------*/

/* list of global defined functions
------------------------------------------------------------------------------*/

/* list of local defined functions
------------------------------------------------------------------------------*/

/* external variables
------------------------------------------------------------------------------*/

extern RTC_HandleTypeDef hrtc;

/* global variables
------------------------------------------------------------------------------*/

/* local defined variables
------------------------------------------------------------------------------*/

/******************************************************************************
* \brief isCommandUcuSleepUSLE -
*
* \params
*       nothing
* \results
*       REQLPMS_ON when low power requested, REQLPMS_OFF otherwise.
*/

int isCommandUcuSleepUSLE(void)
{
    bl_command[10] = 0x02; //$klg> testing bootloader session default
    bl_command[11] = 0x10; //$klg> testing bootloader session default
    return (memcmp(&bl_command[0], COMMAND_SLEEP, COMMAND_SIZE) == 0) ? REQLPMS_ON : REQLPMS_OFF;
}

/******************************************************************************
* \brief enterLowPowerStandby -
*
* \params
*       nothing
* \results
*       BL_ASLEEP, but should never return since HAL_PWR_EnterSTANDBYMode'll trigger a reset
*/

int enterLowPowerStandby(void)
{
    memcpy(&bl_command[0], COMMAND_DELSLEEP, COMMAND_SIZE); // Delete command for entering low power mode

    RCC->APBENR1 |= RCC_APBENR1_PWREN; // Set bit 28 PWREN: Power interface clock enable

    /* PWR_CR3_EWUP5:: When this bit is set, the WKUP5 external wakeup pin is enabled and triggers a wakeup from Standby mode when a */
    /* rising or a falling edge occurs. The active edge is configured via the WP5 bit. Since the reset value for WP5 is 0, it follows */
    /* that the selected behavior for this pin is High level or rising edge. */
    PWR->CR3 |= PWR_CR3_EWUP5;      /* # # # Enable WKUP5 wakeup pin... # # # */

    /* PWR_SCR_CWUF5:: Setting this bit clears the WUF5 wakeup flag in the PWR_SR1 register. */
    PWR->SCR |= PWR_SCR_CWUF5;      /* # # # Clear the WUF5 flag after 3 clock cycles... # # # */

    /* PWR_CR3_ENB_ULP:: When set, the supply voltage is sampled for PDR/BOR reset condition only periodically and not continuously. */
    PWR->CR3 |= PWR_CR3_ENB_ULP;    /* # # # Ultra Low Power enabling... # # # */

    HAL_RTCEx_SetWakeUpTimer_IT(&hrtc, TRTC_SLEEP_STBY, RTC_WAKEUPCLOCK_RTCCLK_DIV16);


    while (1)  { HAL_PWR_EnterSTANDBYMode(); }

    return BL_ASLEEP;
}
