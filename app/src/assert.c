/**
 * @file   assert.h
 * @author Rubén Guijarro
 * @date   January 2022
 * @brief  Public APIs and resources for ASSERT helper.
 */

/* header of project specific types
---------------------------------------------------------------------------*/
#include "assert.h"

/* Private macro
---------------------------------------------------------------------------*/
LOG_MODULE_REGISTER(assert);

/* Function Prototypes */
/* ------------------------------------------------------------------*/
/** @brief Assertion check. Brief version */
void S_assert_brief(const bool condition, S_assert_code assert_code)
{
    if(!condition)
    {
        LOG_DBG("Fatal error %d. Reboot required\n", assert_code);
        k_panic();
    }
}
/** @brief Assertion check
 * This should indicate clearly what and where the problem is, specially in
 * development stage */
void S_assert(const bool condition, const char* func, const int line,
                S_assert_code assert_code, const char *message)
{
    if(!condition)
    {
        LOG_DBG("Fatal error[%s:%d] (%d)%s. Reboot required\n", func, line,
               assert_code, message);
        k_panic();
    }
}
