/**
 * @file   eeprom.c
 * @author Rubén Guijarro
 * @date   April 2022
 * @brief  Public APIs and resources for eeprom module.
 */

/* Header of project specific types
---------------------------------------------------------------------------*/
#include "eeprom.h"
#include "assert.h"
#include "util.h"
#include "i2c.h"

/* Constant definitions
---------------------------------------------------------------------------*/
#define SIZE_1            1U         /* in bytes */
#define SIZE_2            2U         /* in bytes */
#define SIZE_4            4U         /* in bytes */
#define SIZE_8            8U         /* in bytes */
#define PSIZE_BYTES_MAX   8U
#define EEPROM_FL_BUSY    1U

/* Private macro
---------------------------------------------------------------------------*/
LOG_MODULE_REGISTER(eeprom);
#define EEPROM_ST_DEF(id_, size_, offset_) \
{ \
    .data = 0U, \
    .ID = id_, \
    .size = size_, \
    .offset = offset_, \
}

/* Project specific types
---------------------------------------------------------------------------*/
typedef struct eeprom_st_t
{
    uint16_t ID;
    uint16_t size;
    uint16_t offset;
    uint8_t *data;
} eeprom_st;

/* General EEPROM Offset Table  */
typedef enum eeprom_offset_t
{
    OFFSET_DTC_ID_1  =  0U,
    OFFSET_DTC_ID_2  =  16U,
    OFFSET_DTC_ID_3  =  32U,
    OFFSET_DTC_ID_4  =  48U,
    OFFSET_DTC_ID_5  =  64U,
    OFFSET_DTC_ID_6  =  80U,
    OFFSET_DTC_ID_7  =  96U,
    OFFSET_DTC_ID_8  =  112U,
    OFFSET_DTC_ID_9  =  128U,
    OFFSET_DTC_ID_10  =  144U,
    OFFSET_DTC_OC_1  =  160U,
    OFFSET_DTC_OC_2  =  176U,
    OFFSET_DTC_OC_3  =  192U,
    OFFSET_DTC_OC_4  =  208U,
    OFFSET_DTC_OC_5  =  224U,
    OFFSET_DTC_OC_6  =  240U,
    OFFSET_DTC_OC_7  =  256U,
    OFFSET_DTC_OC_8  =  272U,
    OFFSET_DTC_OC_9  =  288U,
    OFFSET_DTC_OC_10  =  304U,
    OFFSET_DTC_WH_1  =  320U,
    OFFSET_DTC_WH_2  =  352U,
    OFFSET_DTC_WH_3  =  384U,
    OFFSET_DTC_WH_4  =  416U,
    OFFSET_DTC_WH_5  =  448U,
    OFFSET_DTC_WH_6  =  480U,
    OFFSET_DTC_WH_7  =  512U,
    OFFSET_DTC_WH_8  =  544U,
    OFFSET_DTC_WH_9  =  576U,
    OFFSET_DTC_WH_10  =  608U,
    OFFSET_ECU_SN  =  640U,
    OFFSET_ECU_HW_PN  =  672U,
    OFFSET_ECU_HW_VERSION  =  704U,
    OFFSET_ECU_MainPartNumber  =  720U,
    OFFSET_RESET_TIME  =  752U,
    OFFSET_MAX_PUMP_TIME  =  768U,
    OFFSET_PAUSE_PUMP_RATIO  =  784U,
    OFFSET_PRESENT_INTERVAL_TIME  =  800U,
    OFFSET_MIN_INTERVAL_TIME  =  816U,
    OFFSET_MAX_INTERVAL_TIME  =  832U,
    OFFSET_WIPER_RUN_TIME  =  848U,
    OFFSET_WIPER_PULSE_TIME  =  880U,
    OFFSET_C_OUTPUT_ENABLE  =  912U,
    OFFSET_WASHER_OL  =  920U,
    OFFSET_WASHER_SCGND  =  936U,
    OFFSET_ECU_CONFIG1  =  952U,
    OFFSET_ECU_CONFIG2  =  960U,
    OFFSET_ECU_CONFIG3  =  968U,
    OFFSET_PWM_MAX_PERCENTAGE  =  976U,
    OFFSET_PWM_MIN_PERCENTAGE  =  984U,
} eeprom_offset;

/* Local variables
-----------------------------------------------------------------------------*/
RETStatus ret = STATUS_ERROR;
uint16_t pos = 0;
uint16_t MemAddress = 0;
bool erase_eeprom = false;
static uint8_t eeprom_internal_bff[PSIZE_BYTES_MAX] = {0};
static uint8_t eeprom_internal_offset = 0U, eeprom_internal_length = 0U;
static uint16_t eeprom_caddr = 0U;
atomic_t eeprombusy_atom_var;
static eeprom_st eeprom_filter[EEPROM_ID_MAX] = {
    EEPROM_ST_DEF(ECU_HW_PN, SIZE_4, OFFSET_ECU_HW_PN),
    EEPROM_ST_DEF(ECU_SN, SIZE_4, OFFSET_ECU_SN),
    EEPROM_ST_DEF(ECU_HW_VERSION, SIZE_2, OFFSET_ECU_HW_VERSION),
    EEPROM_ST_DEF(ECU_MainPartNumber, SIZE_4, OFFSET_ECU_MainPartNumber),
    EEPROM_ST_DEF(DTC_ID_1, SIZE_2, OFFSET_DTC_ID_1),
    EEPROM_ST_DEF(DTC_ID_2, SIZE_2, OFFSET_DTC_ID_2),
    EEPROM_ST_DEF(DTC_ID_3, SIZE_2, OFFSET_DTC_ID_3),
    EEPROM_ST_DEF(DTC_ID_4, SIZE_2, OFFSET_DTC_ID_4),
    EEPROM_ST_DEF(DTC_ID_5, SIZE_2, OFFSET_DTC_ID_5),
    EEPROM_ST_DEF(DTC_ID_6, SIZE_2, OFFSET_DTC_ID_6),
    EEPROM_ST_DEF(DTC_ID_7, SIZE_2, OFFSET_DTC_ID_7),
    EEPROM_ST_DEF(DTC_ID_8, SIZE_2, OFFSET_DTC_ID_8),
    EEPROM_ST_DEF(DTC_ID_9, SIZE_2, OFFSET_DTC_ID_9),
    EEPROM_ST_DEF(DTC_ID_10, SIZE_2, OFFSET_DTC_ID_10),
    EEPROM_ST_DEF(DTC_OC_1, SIZE_2, OFFSET_DTC_OC_1),
    EEPROM_ST_DEF(DTC_OC_2, SIZE_2, OFFSET_DTC_OC_2),
    EEPROM_ST_DEF(DTC_OC_3, SIZE_2, OFFSET_DTC_OC_3),
    EEPROM_ST_DEF(DTC_OC_4, SIZE_2, OFFSET_DTC_OC_4),
    EEPROM_ST_DEF(DTC_OC_5, SIZE_2, OFFSET_DTC_OC_5),
    EEPROM_ST_DEF(DTC_OC_6, SIZE_2, OFFSET_DTC_OC_6),
    EEPROM_ST_DEF(DTC_OC_7, SIZE_2, OFFSET_DTC_OC_7),
    EEPROM_ST_DEF(DTC_OC_8, SIZE_2, OFFSET_DTC_OC_8),
    EEPROM_ST_DEF(DTC_OC_9, SIZE_2, OFFSET_DTC_OC_9),
    EEPROM_ST_DEF(DTC_OC_10, SIZE_2, OFFSET_DTC_OC_10),
    EEPROM_ST_DEF(DTC_WH_1, SIZE_4, OFFSET_DTC_WH_1),
    EEPROM_ST_DEF(DTC_WH_2, SIZE_4, OFFSET_DTC_WH_2),
    EEPROM_ST_DEF(DTC_WH_3, SIZE_4, OFFSET_DTC_WH_3),
    EEPROM_ST_DEF(DTC_WH_4, SIZE_4, OFFSET_DTC_WH_4),
    EEPROM_ST_DEF(DTC_WH_5, SIZE_4, OFFSET_DTC_WH_5),
    EEPROM_ST_DEF(DTC_WH_6, SIZE_4, OFFSET_DTC_WH_6),
    EEPROM_ST_DEF(DTC_WH_7, SIZE_4, OFFSET_DTC_WH_7),
    EEPROM_ST_DEF(DTC_WH_8, SIZE_4, OFFSET_DTC_WH_8),
    EEPROM_ST_DEF(DTC_WH_9, SIZE_4, OFFSET_DTC_WH_9),
    EEPROM_ST_DEF(DTC_WH_10, SIZE_4, OFFSET_DTC_WH_10),
    EEPROM_ST_DEF(RESET_TIME, SIZE_2, OFFSET_RESET_TIME),
    EEPROM_ST_DEF(MAX_PUMP_TIME, SIZE_2, OFFSET_MAX_PUMP_TIME),
    EEPROM_ST_DEF(PAUSE_PUMP_RATIO, SIZE_2, OFFSET_PAUSE_PUMP_RATIO),
    EEPROM_ST_DEF(PRESENT_INTERVAL_TIME, SIZE_2, OFFSET_PRESENT_INTERVAL_TIME),
    EEPROM_ST_DEF(MIN_INTERVAL_TIME, SIZE_2, OFFSET_MIN_INTERVAL_TIME),
    EEPROM_ST_DEF(MAX_INTERVAL_TIME, SIZE_2, OFFSET_MAX_INTERVAL_TIME),
    EEPROM_ST_DEF(WIPER_RUN_TIME, SIZE_4, OFFSET_WIPER_RUN_TIME),
    EEPROM_ST_DEF(WIPER_PULSE_TIME, SIZE_4, OFFSET_WIPER_PULSE_TIME),
    EEPROM_ST_DEF(C_OUTPUT_ENABLE, SIZE_1, OFFSET_C_OUTPUT_ENABLE),
    EEPROM_ST_DEF(WASHER_OL, SIZE_2, OFFSET_WASHER_OL),
    EEPROM_ST_DEF(WASHER_SCGND, SIZE_2, OFFSET_WASHER_SCGND),
    EEPROM_ST_DEF(ECU_CONFIG1, SIZE_1, OFFSET_ECU_CONFIG1),
    EEPROM_ST_DEF(ECU_CONFIG2, SIZE_1, OFFSET_ECU_CONFIG2),
    EEPROM_ST_DEF(ECU_CONFIG3, SIZE_1, OFFSET_ECU_CONFIG3),
    EEPROM_ST_DEF(PWM_MAX_PERCENTAGE, SIZE_1, OFFSET_PWM_MAX_PERCENTAGE),
    EEPROM_ST_DEF(PWM_MIN_PERCENTAGE, SIZE_1, OFFSET_PWM_MIN_PERCENTAGE)
};



/* Global functions
-----------------------------------------------------------------------------*/
bool eeprom_nowait_command_completed(void)
{
    bool retvalue = false;

    if (atomic_cas(&(eeprombusy_atom_var), 0U, EEPROM_FL_BUSY))
    {
        retvalue = true;
        atomic_clear(&(eeprombusy_atom_var));
    }

    return retvalue;
}
bool get_erase_eeprom(void){ return erase_eeprom;}
void set_erase_eeprom(bool value){ erase_eeprom = value;}
/** @brief Initialization function for EEPROM module
 */
void eeprom_init(void)
{
    atomic_clear(&(eeprombusy_atom_var));
}
/** @brief Main function for storage and load EEPROM parameters
 */
void eeprom_step(void)
{
    RETStatus retval = STATUS_ERROR;
    static eeprom_id eeprom_erase_param = DTC_ID_1;

    if (atomic_cas(&(eeprombusy_atom_var), EEPROM_FL_BUSY, EEPROM_FL_BUSY))
    {
        if (eeprom_internal_length)
        {
            retval = i2c_write_func(eeprom_caddr, &eeprom_internal_bff[eeprom_internal_offset], 1U);
            if (retval == STATUS_ERROR)
                LOG_DBG("i2c_write_func failing \n");

            eeprom_internal_offset++;
            eeprom_internal_length--;

            eeprom_caddr++;
        }
        else
        {
            eeprom_internal_offset = 0U;
            atomic_clear(&(eeprombusy_atom_var));
        }
    }

    if((erase_eeprom) && (eeprom_nowait_command_completed()))
    {
        eeprom_erase_nowait(eeprom_erase_param);
        eeprom_erase_param++;
        if(eeprom_erase_param == EEPROM_ID_MAX){
            eeprom_erase_param = DTC_ID_1;
            erase_eeprom = false;
        }
    }
}
/** @brief Get detection status for eeprom parameter
 * @param eeprom_id - param id
 * @param data - valid pointer to data
 * @return Status
 */
RETStatus eeprom_get(eeprom_id eeprom_id, uint8_t *data)
{
    S_ASSERT(eeprom_id < EEPROM_ID_MAX, ASSERT_BOUNDS, "Invalid eeprom parameter");
    S_ASSERT(data != NULL, ASSERT_BOUNDS, "Invalid pointer");
    
    data = eeprom_filter[eeprom_id].data;
    
    return STATUS_OK;
}
/** @brief write paramter to eeprom
 * @param eeprom_parameter - param id
 * @param data - valid pointer to data
 * @return Status
 */
RETStatus eeprom_write (eeprom_id eeprom_parameter, uint8_t *data)
{
    S_ASSERT(eeprom_parameter < EEPROM_ID_MAX, ASSERT_BOUNDS, "Invalid eeprom parameter");
    
    ret = STATUS_ERROR;
    pos=0;
    MemAddress = eeprom_filter[eeprom_parameter].offset;
    
    for (int i=0; i < eeprom_filter[eeprom_parameter].size; i++)
    {
        ret = i2c_write_func(MemAddress, &data[pos], 1);
        
        if(ret != 0)
        {
            ret = STATUS_ERROR;
            break;
        }

        MemAddress += 1;
        pos += 1;
        ret = STATUS_OK;

        k_msleep(4);
    }
    return ret;
}
/** @brief erase a eeprom page
 * @param eeprom_parameter - param id
 * @return Status
 */
RETStatus eeprom_erase (eeprom_id eeprom_parameter)
{   
    S_ASSERT(eeprom_parameter < EEPROM_ID_MAX, ASSERT_BOUNDS, "Invalid eeprom parameter");
    
    ret = STATUS_ERROR;
    pos=0;
    uint8_t data[SIZE_1];
    memset(data,0xff,SIZE_1);
    MemAddress = eeprom_filter[eeprom_parameter].offset;

    for (int i=0; i < eeprom_filter[eeprom_parameter].size; i++)
    {
        ret = i2c_write_func(MemAddress, &data[0], 1);
        
        if(ret != 0)
        {
            ret = STATUS_ERROR;
            break;
        }
        
        MemAddress += 1;
        pos += 1;
        ret = STATUS_OK;

        k_msleep(4);
    }
    return ret;
}

/** @brief read paramter from eeprom
 * @param eeprom_parameter - param id
 * @param data - valid pointer to data
 * @return Status
 */
RETStatus eeprom_read(eeprom_id eeprom_parameter, uint8_t *data)
{
    S_ASSERT(eeprom_parameter < EEPROM_ID_MAX, ASSERT_BOUNDS, "Invalid eeprom parameter");

    RETStatus retval = STATUS_ERROR;

    if (atomic_cas(&(eeprombusy_atom_var), 0U, EEPROM_FL_BUSY))
    {    
        uint8_t u8pos = 0U;
        eeprom_caddr = eeprom_filter[eeprom_parameter].offset;
        
        for (uint8_t i = 0U; i < eeprom_filter[eeprom_parameter].size; i++)
        {
            retval = i2c_read_func(eeprom_caddr, &data[u8pos], 1U);

            eeprom_caddr += 1U;
            u8pos += 1U;

            if ((retval != STATUS_OK) || (u8pos == PSIZE_BYTES_MAX)) break;
        }

        atomic_clear(&(eeprombusy_atom_var));
    }
    else
        LOG_DBG("eeprom_read has failed, the device is busy \n");

    return retval;
}
/** @brief write paramter to eeprom with no wait
 * @param eeprom_parameter - param id
 * @param data - valid pointer to data
 * @return Status
 */
RETStatus eeprom_write_nowait(eeprom_id eeprom_parameter, uint8_t *data)
{   
    S_ASSERT(eeprom_parameter < EEPROM_ID_MAX, ASSERT_BOUNDS, "Invalid eeprom parameter");

    RETStatus retval = STATUS_ERROR;    

    if (atomic_cas(&(eeprombusy_atom_var), 0U, EEPROM_FL_BUSY))
    {
        eeprom_caddr = eeprom_filter[eeprom_parameter].offset;
        eeprom_internal_length = eeprom_filter[eeprom_parameter].size;

        memcpy(eeprom_internal_bff, data, eeprom_internal_length);

        retval = STATUS_OK;
    }

    return retval;
}
/** @brief erase paramter to eeprom with no wait
 * @param eeprom_parameter - param id
 * @return Status
 */
RETStatus eeprom_erase_nowait(eeprom_id eeprom_parameter)
{   
    S_ASSERT(eeprom_parameter < EEPROM_ID_MAX, ASSERT_BOUNDS, "Invalid eeprom parameter");

    RETStatus retval = STATUS_ERROR;    

    if (atomic_cas(&(eeprombusy_atom_var), 0U, EEPROM_FL_BUSY))
    {
        eeprom_caddr = eeprom_filter[eeprom_parameter].offset;
        eeprom_internal_length = eeprom_filter[eeprom_parameter].size;

        memset(eeprom_internal_bff, 0xFF, PSIZE_BYTES_MAX);

        retval = STATUS_OK;
    }

    return retval;
}
