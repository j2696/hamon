/**
 * @file   gpio.c
 * @author Rubén Guijarro
 * @date   January 2022
 * @brief  Public APIs and resources for GPIO module.
 */

/* header of project specific types
---------------------------------------------------------------------------*/
#include "gpio.h"
#include "assert.h"
#include "util.h"
#include "debounce.h"

/* Constant definitions
---------------------------------------------------------------------------*/
/** @brief Custom flag avoids gpio-pin initialization
 * Shall be reviewed its consistency with upcoming zephyr updates
 */
#define GPIO_INIT_DISABLED      (1U << 30)

/* Private macro
---------------------------------------------------------------------------*/
LOG_MODULE_REGISTER(gpio);
#define U_MUX_DEF(muxa_, muxb_, muxc_, muxd_) \
{ \
    .individual.FG_OUT_MUX_A = muxa_, \
    .individual.FG_OUT_MUX_B = muxb_, \
    .individual.FG_OUT_MUX_C = muxc_, \
    .individual.FG_OUT_MUX_D = muxd_, \
    .individual.FLAGS_UNUSED = 0U, \
}

/* Project specific types
---------------------------------------------------------------------------*/
typedef union {
    uint8_t Byte;
    struct {
        uint8_t  FG_OUT_MUX_A      :1;
        uint8_t  FG_OUT_MUX_B      :1;
        uint8_t  FG_OUT_MUX_C      :1;
        uint8_t  FG_OUT_MUX_D      :1;
        uint8_t  FLAGS_UNUSED      :4;
    } individual;
} U_MUX_PINS;

/* Local variables
-----------------------------------------------------------------------------*/
/** @brief zephyr device for control gpios
 * Shall follow same order as ids declared in @E_ctrlgpio_id
 */
const static struct device *GPIO_ctrlpins[E_CTRLGPIO_ID_MAX] = {0};
bool gpio_toggle_external_watchdog_flag = false;
static const struct pin_config pinconf_alt_SCK[] = {
	{STM32_PIN_PB3, STM32_PINMUX_ALT_FUNC_0},
};
static const U_MUX_PINS MUX_internal[E_MUX_ID_MAX] = {
    U_MUX_DEF(0U, 0U, 0U, 0U),  /* MUX_KEYB_1 */
    U_MUX_DEF(1U, 0U, 0U, 0U),  /* MUX_KEYB_2 */
    U_MUX_DEF(0U, 1U, 0U, 0U),  /* MUX_KEYB_3 */
    U_MUX_DEF(1U, 1U, 0U, 0U),  /* MUX_KEYB_4 */
    U_MUX_DEF(0U, 0U, 1U, 0U),  /* MUX_KEYB_5 */
    U_MUX_DEF(1U, 0U, 1U, 0U),  /* MUX_KEYB_6 */
    U_MUX_DEF(0U, 1U, 1U, 0U),  /* MUX_KEYB_7 */
    U_MUX_DEF(1U, 1U, 1U, 0U),  /* MUX_KEYB_8 */
};

/** @brief control gpio device names
 * Shall follow same order as ids declared in @E_ctrlgpio_id
 */
const static char *u_ctrlgpio_devnames[E_CTRLGPIO_ID_MAX] =
{
    DT_GPIO_LABEL(DT_PATH(outputs, mux_inh), gpios),
    DT_GPIO_LABEL(DT_PATH(outputs, wd_out), gpios),
    DT_GPIO_LABEL(DT_PATH(outputs, hsd1), gpios),
    DT_GPIO_LABEL(DT_PATH(outputs, en1), gpios),
    DT_GPIO_LABEL(DT_PATH(outputs, hsd2), gpios),
    DT_GPIO_LABEL(DT_PATH(outputs, en2), gpios),
    DT_GPIO_LABEL(DT_PATH(outputs, hsd3), gpios),
    DT_GPIO_LABEL(DT_PATH(outputs, en3), gpios),
    DT_GPIO_LABEL(DT_PATH(outputs, eeprom_wc), gpios),
    DT_GPIO_LABEL(DT_PATH(outputs, csn), gpios),
    DT_GPIO_LABEL(DT_PATH(outputs, idle), gpios),
    DT_GPIO_LABEL(DT_PATH(outputs, can_stby), gpios),
    DT_GPIO_LABEL(DT_PATH(outputs, spi_clk), gpios),    
    DT_GPIO_LABEL(DT_PATH(outputs, led_oe), gpios),
    DT_GPIO_LABEL(DT_PATH(outputs, led_le), gpios),
    DT_GPIO_LABEL(DT_PATH(outputs, mux_a), gpios),
    DT_GPIO_LABEL(DT_PATH(outputs, mux_b), gpios),
    DT_GPIO_LABEL(DT_PATH(outputs, mux_c), gpios),
    DT_GPIO_LABEL(DT_PATH(outputs, mux_d), gpios),
};

/** @brief control gpio pin
 * Shall follow same order as ids declared in @E_ctrlgpio_id
 */
const static gpio_pin_t s_ctrlgpio_pins[E_CTRLGPIO_ID_MAX] =
{
    DT_GPIO_PIN(DT_PATH(outputs, mux_inh), gpios),
    DT_GPIO_PIN(DT_PATH(outputs, wd_out), gpios),
    DT_GPIO_PIN(DT_PATH(outputs, hsd1), gpios),
    DT_GPIO_PIN(DT_PATH(outputs, en1), gpios),
    DT_GPIO_PIN(DT_PATH(outputs, hsd2), gpios),
    DT_GPIO_PIN(DT_PATH(outputs, en2), gpios),
    DT_GPIO_PIN(DT_PATH(outputs, hsd3), gpios),
    DT_GPIO_PIN(DT_PATH(outputs, en3), gpios),
    DT_GPIO_PIN(DT_PATH(outputs, eeprom_wc), gpios),
    DT_GPIO_PIN(DT_PATH(outputs, csn), gpios),
    DT_GPIO_PIN(DT_PATH(outputs, idle), gpios),
    DT_GPIO_PIN(DT_PATH(outputs, can_stby), gpios),
    DT_GPIO_PIN(DT_PATH(outputs, spi_clk), gpios),
    DT_GPIO_PIN(DT_PATH(outputs, led_oe), gpios),
    DT_GPIO_PIN(DT_PATH(outputs, led_le), gpios),
    DT_GPIO_PIN(DT_PATH(outputs, mux_a), gpios),
    DT_GPIO_PIN(DT_PATH(outputs, mux_b), gpios),
    DT_GPIO_PIN(DT_PATH(outputs, mux_c), gpios),
    DT_GPIO_PIN(DT_PATH(outputs, mux_d), gpios),
};

/** @brief control gpio pin
 * Shall follow same order as ids declared in @E_ctrlgpio_id
 */
const static gpio_flags_t s_ctrlgpio_flags[E_CTRLGPIO_ID_MAX] = /* flag bit 30 conf_en */
{
    DT_GPIO_FLAGS(DT_PATH(outputs, mux_inh), gpios)  | GPIO_OUTPUT_INACTIVE,
    DT_GPIO_FLAGS(DT_PATH(outputs, wd_out), gpios)  | GPIO_OUTPUT_INACTIVE,
    DT_GPIO_FLAGS(DT_PATH(outputs, hsd1), gpios)  | GPIO_OUTPUT_INACTIVE,
    DT_GPIO_FLAGS(DT_PATH(outputs, en1), gpios)  | GPIO_OUTPUT_INACTIVE,
    DT_GPIO_FLAGS(DT_PATH(outputs, hsd2), gpios)  | GPIO_OUTPUT_INACTIVE,
    DT_GPIO_FLAGS(DT_PATH(outputs, en2), gpios)  | GPIO_OUTPUT_INACTIVE,
    DT_GPIO_FLAGS(DT_PATH(outputs, hsd3), gpios)  | GPIO_OUTPUT_INACTIVE,
    DT_GPIO_FLAGS(DT_PATH(outputs, en3), gpios)  | GPIO_OUTPUT_INACTIVE,
    DT_GPIO_FLAGS(DT_PATH(outputs, eeprom_wc), gpios)  | GPIO_OUTPUT_INACTIVE,
    DT_GPIO_FLAGS(DT_PATH(outputs, csn), gpios)  | GPIO_OUTPUT_INACTIVE,
    DT_GPIO_FLAGS(DT_PATH(outputs, idle), gpios)  | GPIO_OUTPUT_INACTIVE,
    DT_GPIO_FLAGS(DT_PATH(outputs, can_stby), gpios)  | GPIO_OUTPUT_INACTIVE,
    DT_GPIO_FLAGS(DT_PATH(outputs, spi_clk), gpios) | GPIO_OUTPUT_INACTIVE | GPIO_INIT_DISABLED,
    DT_GPIO_FLAGS(DT_PATH(outputs, led_oe), gpios) | GPIO_OUTPUT_INACTIVE | GPIO_INIT_DISABLED,
    DT_GPIO_FLAGS(DT_PATH(outputs, led_le), gpios) | GPIO_OUTPUT_INACTIVE,
    DT_GPIO_FLAGS(DT_PATH(outputs, mux_a), gpios)  | GPIO_OUTPUT_INACTIVE,
    DT_GPIO_FLAGS(DT_PATH(outputs, mux_b), gpios)  | GPIO_OUTPUT_INACTIVE,
    DT_GPIO_FLAGS(DT_PATH(outputs, mux_c), gpios)  | GPIO_OUTPUT_INACTIVE,
    DT_GPIO_FLAGS(DT_PATH(outputs, mux_d), gpios)  | GPIO_OUTPUT_INACTIVE,
};

/* Global functions
---------------------------------------------------------------------------*/
/** @brief init gpio module
 * Initialize and configure gpio pins 
 */
void gpio_init(void)
{
    MODULE_SINGLE_INIT;

    S_ASSERT(ARRAY_SIZE(u_ctrlgpio_devnames) == ARRAY_SIZE(s_ctrlgpio_pins), ASSERT_RUNTIME, "Error GPIO pins and device names arrays lengths do not match");   
    S_ASSERT(ARRAY_SIZE(s_ctrlgpio_flags) == ARRAY_SIZE(s_ctrlgpio_pins), ASSERT_RUNTIME, "Error GPIO pins and flag arrays lengths do not match"); 

    for(int i=0; i < ARRAY_SIZE(GPIO_ctrlpins); i++)
    {
        const struct device *dev = device_get_binding(u_ctrlgpio_devnames[i]);
        S_ASSERT(dev != NULL, ASSERT_RUNTIME, "Error initializing gpio device");

        GPIO_ctrlpins[i] = dev;

        int ret = 0;

        if (!(s_ctrlgpio_flags[i] & GPIO_INIT_DISABLED))
            ret = gpio_pin_configure(dev, s_ctrlgpio_pins[i], s_ctrlgpio_flags[i]);

        if ( ret != 0 )
            S_ASSERT(dev != NULL, ASSERT_RUNTIME, "Error configuring gpio device");
    }

    gpio_toggle_output(WD_OUT);
    gpio_toggle_external_watchdog_flag = true;
    
    gpio_set(OUT_EN1, 1U);
    gpio_set(OUT_EN2, 1U);
    gpio_set(OUT_EN3, 1U);
}
/** @brief set gpio pin value
 * @param gpio_n - gpio id
 * @param value - valid output value
 * @return Status
 */
RETStatus gpio_set(const E_ctrlgpio_id gpio_n, const int value)
{
    S_ASSERT(gpio_n < E_CTRLGPIO_ID_MAX, ASSERT_BOUNDS, "Invalid gpio number");
    RETStatus status = STATUS_OK;
    const struct device *pindev = GPIO_ctrlpins[gpio_n];

    int rc = gpio_pin_set(pindev, s_ctrlgpio_pins[gpio_n], value);
    status = (rc != 0) ? STATUS_ERROR : STATUS_OK;
    return status;
}
/** @brief get gpio pin value
 * @param gpio_n - gpio id
 * @param value - pointer to data
 * @return Status
 */
RETStatus gpio_get(const E_ctrlgpio_id gpio_n, int *value)
{
    S_ASSERT(gpio_n < E_CTRLGPIO_ID_MAX, ASSERT_BOUNDS, "Invalid gpio number");
    RETStatus status = STATUS_OK;
    const struct device *pindev = GPIO_ctrlpins[gpio_n];

    *value = gpio_pin_get(pindev, s_ctrlgpio_pins[gpio_n]);
    if ( value >= 0 )
        status = STATUS_OK;
    else
        status = STATUS_ERROR;

    return status;
}
/** @brief toggle gpio output pin
 * @param gpio_n - gpio id
 * @return Status
 */
RETStatus gpio_toggle_output(const E_ctrlgpio_id gpio_n)
{
    S_ASSERT(gpio_n < E_CTRLGPIO_ID_MAX, ASSERT_BOUNDS, "Invalid gpio number");
    RETStatus status = STATUS_OK;
    const struct device *pindev = GPIO_ctrlpins[gpio_n];
    int rc = gpio_port_toggle_bits(pindev, (gpio_port_pins_t)BIT(s_ctrlgpio_pins[gpio_n]));
    status = (rc != 0) ? STATUS_ERROR : STATUS_OK;
    return status;
}
/** @brief set multiplexor gpio pin value
 * @param mux_sel - gpio id
 */
void gpio_set_multiplexor(const E_mux_id mux_sel)
{
    S_ASSERT(mux_sel < E_MUX_ID_MAX, ASSERT_BOUNDS, "Invalid mux selection");

    gpio_set(OUT_MUX_A, MUX_internal[mux_sel].individual.FG_OUT_MUX_A ? 1 : 0);
    gpio_set(OUT_MUX_B, MUX_internal[mux_sel].individual.FG_OUT_MUX_B ? 1 : 0);
    gpio_set(OUT_MUX_C, MUX_internal[mux_sel].individual.FG_OUT_MUX_C ? 1 : 0);
    gpio_set(OUT_MUX_D, MUX_internal[mux_sel].individual.FG_OUT_MUX_D ? 1 : 0);
}

/** @brief The only periodic task gpio-related is the external watchdog refresh.
 *  The pin involved is WD_OUT (PC1) and the refresh consists of toggling the output every 1 second.
 */
void gpio_step(void)
{
    gpio_set(OUT_HSD1, get_PumpLeftOutput());
    gpio_set(OUT_HSD3, get_PumpRearOutput());
    gpio_set(OUT_HSD2, get_PumpRightOutput());
}
/** @brief reconfigures the pin PB3, whose use after initialization is spi clk, as a gpio output
 */
void gpio_switch_spi_pb3_to_gpio_output(void)
{
    const struct device *gpiob_dev = GPIO_ctrlpins[OUT_SPI_CLK];

    pinmux_pin_set(gpiob_dev, s_ctrlgpio_pins[OUT_SPI_CLK], GPIO_OUTPUT | GPIO_PULL_UP);
}/** @brief reconfigures the pin PB4, whose use after initialization is pwm output, as a gpio output
 */
void gpio_switch_pwm_pb4_to_gpio_output(void)
{
    const struct device *gpiob_dev = GPIO_ctrlpins[OUT_LED_OE];

    pinmux_pin_set(gpiob_dev, s_ctrlgpio_pins[OUT_LED_OE], GPIO_OUTPUT | GPIO_PULL_UP);
}

/** This workaround is needed since the api call pinmux_pin_set does not works properly.
 *  It's possible to use the lower call stm32_setup_pins for the stm32 compilation and in
 *  this way it works as expected, but the test compilation fails.
 *  In order to get a success compilation in native-posix target the below definition forces
 *  to use the pinmux_pin_set call when a test compilation is launched
 */
/** @brief reconfigures pin PB3 as alternative functionality number 0 (spi clk signal)
 */
void gpio_switch_gpio_output_to_spi_pb3(void)
{
    stm32_setup_pins(pinconf_alt_SCK, ARRAY_SIZE(pinconf_alt_SCK));
}
static const struct pin_config pinconf_alt_PWM[] = {
	{STM32_PIN_PB4, STM32_PINMUX_ALT_FUNC_1},
};
/** @brief reconfigures pin PB4 as alternative functionality number 1 (pwm output signal)
 */
void gpio_switch_gpio_output_to_pwm_pb4(void)
{
    stm32_setup_pins(pinconf_alt_PWM, ARRAY_SIZE(pinconf_alt_PWM));
}
