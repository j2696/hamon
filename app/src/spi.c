/**
 * @file   spi.c
 * @author Rubén Guijarro
 * @date   January 2022
 * @brief  Public APIs and resources for SPI module.
 */

/* Header of project specific types
---------------------------------------------------------------------------*/
#include "assert.h"
#include "util.h"
#include "spi.h"

/* Constant definitions
---------------------------------------------------------------------------*/
#define SPI_MAX_MSG_LEN        16

/* Project specific types
---------------------------------------------------------------------------*/
/** @struct spi_integritydetect
 *  @brief This structure stores parameters for spi integrity error detection
 *  @var spi_integritydetect::integritydetect_en 
 *  Member 'integritydetect_en' true if feature is enabled
 *  @var spi_integritydetect::failure_status
 *  Member 'failure_status' true if a failure has been detected in the last transmission
 */
typedef struct spi_integritydetect_t
{
    bool integritydetect_en;
    bool failure_status;
}spi_integritydetect;

/* Local variables
-----------------------------------------------------------------------------*/
static const struct device *spi1_dev_led = DEVICE_DT_GET(DT_NODELABEL(spi1));
static struct spi_config spi_cfg_led = {
    .operation = SPI_WORD_SET(8) | SPI_OP_MODE_MASTER | SPI_LINES_SINGLE,
    .frequency = MHZ(1),
};
static const struct device *spi2_dev_tle = DEVICE_DT_GET(DT_NODELABEL(spi2));
static struct spi_config spi_cfg_tle = {
    .operation = SPI_WORD_SET(8) | SPI_OP_MODE_MASTER | SPI_LINES_SINGLE | SPI_MODE_CPHA,
    .frequency = MHZ(1),
};
static uint8_t tx1msg[2U][SPI_MAX_MSG_LEN] = {0};
static uint8_t rx1message[SPI_MAX_MSG_LEN] = {0};
static struct spi_buf tx1_even, tx1_odd;
static struct spi_buf_set tx1_bufs = {
    .buffers = &tx1_even,
    .count = 1,
};
static struct spi_buf rx1;
const static struct spi_buf_set rx1_bufs = {
    .buffers = &rx1,
    .count = 1,
};

static uint8_t tx2message[SPI_MAX_MSG_LEN] = {0};
static uint8_t rx2message[SPI_MAX_MSG_LEN] = {0};
static struct spi_buf tx2;
static struct spi_buf_set tx2_bufs = {
    .buffers = &tx2,
    .count = 1,
};
static struct spi_buf rx2;
const static struct spi_buf_set rx2_bufs = {
    .buffers = &rx2,
    .count = 1,
};
/* Internal struct holding parameters for spi integrity error detection */
static spi_integritydetect SPI_integrity = {0};

/* Local functions
---------------------------------------------------------------------------*/
static void spi_enable_integrity_check(bool integrity_en)
{
    SPI_integrity.integritydetect_en = integrity_en;
}

/* Global functions
---------------------------------------------------------------------------*/
/** @brief Initialization function SP1
 */
void spi_led_init(void)
{
    // MODULE_SINGLE_INIT;

    S_ASSERT(device_is_ready(spi1_dev_led), ASSERT_INTERNAL, "SPI1-LED bus is not ready");

    spi_enable_integrity_check(false);
}

/** @brief Main function SPI 1
 * @param txdata - pointer to data transmit
 * @param rxdata - pointer to data receive
 * @param length - valid data length
 */
RETStatus spi_led_step(const uint8_t *txdata, uint8_t *rxdata, const uint16_t length)
{
    S_ASSERT(txdata != NULL, ASSERT_BOUNDS, "Invalid pointer");
    S_ASSERT(!(length > SPI_MAX_MSG_LEN), ASSERT_BOUNDS, "Invalid length");

    RETStatus ret = 0;
    static bool txbff_select = 0U;

    txbff_select = !txbff_select;

    /* Alternative transmission buffer switching */
    /* The memcpy is intended to preserve the actual transmission buffer for the next time spi_step() will be called. */
    /* Like that it's possible to compare the received data with the previous transmission buffer in order to get the */
    /* integrity (failure or success) of the spi transmission operation */

    if (!txbff_select) {
        tx1_even.buf = memcpy(tx1msg[txbff_select ? 1U : 0U], txdata, length);
        tx1_even.len = length;
        tx1_bufs.buffers = &tx1_even;
    } else {
        tx1_odd.buf = memcpy(tx1msg[txbff_select ? 1U : 0U], txdata, length);
        tx1_odd.len = length;
        tx1_bufs.buffers = &tx1_odd;
    }

    rx1.buf = rx1message;
    rx1.len = length;
    int tr_ret = spi_transceive(spi1_dev_led, &spi_cfg_led, &tx1_bufs, &rx1_bufs);

    if (rxdata != NULL)
        memcpy(rxdata, rx1message, length);

    if ((tr_ret == 0) && (SPI_integrity.integritydetect_en == true))
    {
        ret = memcmp(rx1message, tx1msg[txbff_select ? 0U : 1U], length) ? STATUS_INTEGRITY_ERROR : STATUS_OK;
        SPI_integrity.failure_status = (ret != STATUS_OK) ? true : false;
    }
    else
        ret = (tr_ret == 0) ? STATUS_OK : STATUS_ERROR;

    return (ret);
}

/** @brief Initialization function SP2
 */
void spi_tle_init(void)
{
    // MODULE_SINGLE_INIT;

    S_ASSERT(device_is_ready(spi2_dev_tle), ASSERT_INTERNAL, "SPI2-TLE bus is not ready");
}
/** @brief Main function SPI 2
 * @param txdata - pointer to data transmit
 * @param rxdata - pointer to data receive
 * @param length - valid data length
 */
RETStatus spi_tle_step(const uint8_t *txdata, uint8_t *rxdata, const uint16_t length)
{
    S_ASSERT(txdata != NULL, ASSERT_BOUNDS, "Invalid pointer");
    S_ASSERT(!(length > SPI_MAX_MSG_LEN), ASSERT_BOUNDS, "Invalid length");

    RETStatus ret = 0;

    tx2.buf = memcpy(tx2message, txdata, length);
    tx2.len = length;
    tx2_bufs.buffers = &tx2;

    rx2.buf = rx2message;
    rx2.len = length;

    int tr_ret = spi_transceive(spi2_dev_tle, &spi_cfg_tle, &tx2_bufs, &rx2_bufs);

    if (rxdata != NULL)
        memcpy(rxdata, rx2message, length);

    ret = (tr_ret == 0) ? STATUS_OK : STATUS_ERROR;

    return (ret);
}
