#pragma once
/**
 * @file   led.h
 * @author Rubén Guijarro
 * @date   January 2022
 * @brief  Public APIs and resources for LED module.
 */

/* header of project specific types
---------------------------------------------------------------------------*/
#include "config.h"
#include "keyb.h"

/* project specific types
---------------------------------------------------------------------------*/
/** @brief */
typedef enum E_Led_t
{
    LED_1_U, LED_1_D,
    LED_2_U, LED_2_D,
    LED_4_U, LED_4_D,
    LED_6_U, LED_6_D,
    LED_8_U, LED_8_D,
    LED_W_1_2,
    LED_W_3_4,
    LED_W_5_6,
    LED_W_7_8,
    LED_NU_1, LED_NU_2,
    LED_MAX = 16
} E_Led;

typedef enum E_Led_state_t
{
    OFF, LOWER_ON, HIGHER_ON, BOTH_ON,
    STATE_MAX
} E_Led_state;

typedef enum E_LedFailureSt_t
{
    LED_NOT_FAILING = 0,
    LED_BLINK_ERROR,
    LED_COLOR_ERROR,
    LED_FAILING,
    LED_FAIL_ST_UNKNOWN,
    LED_ERROR = 0xE,
    LED_NOT_AVAILABLE,
    LED_FAILURE_MAX,    
} E_LedFailureSt;

/* Global functions declaration
---------------------------------------------------------------------------*/
void led_init(void);
RETStatus led_set_status(const E_Led ledn, const bool state);
RETStatus led_button_set(const WiperCtrlSel_str  button, const E_Led_state state);
void nled_step(void);
void led_step(void);
void led_enable_detect_feature(const uint16_t errdetect_seq_burstperiod);
RETStatus led_refresh_st_error_detect_feature(const uint8_t *trigger_bff);
E_LedFailureSt led_get_error_status_autotest(const E_Led ledn);
E_LedFailureSt led_get_error_status_buttons(const WiperCtrlSel_str  button);
void led_disable_output_on_fail(const E_Led ledn);
void led_enable_output_on_fail(const E_Led ledn);
E_Led_state led_val_ledstate(uint8_t value);
void led_get_time_stamp(const WiperCtrlSel_str  button, uint64_t *time_stamp);
void led_get_last_colour(const WiperCtrlSel_str  button, E_Led_state *Led_colour);
uint16_t led_get_bright_value(void);
void led_set_bright_value(uint16_t bval);

