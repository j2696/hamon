/**
 * @file   tle.c
 * @author Rubén Guijarro
 * @date   February 2022
 * @brief  Public APIs and resources for TLE module.
 */

/* Header of project specific types
---------------------------------------------------------------------------*/
#include "util.h"
#include "gpio.h"
#include "spi.h"
#include "tle.h"
#include "assert.h"
#include "timer.h"
#include "keyb.h"

/* Constant definitions
---------------------------------------------------------------------------*/
#define INVALID_REFRESH_OP_INC_STEP      5U
#define MAXCOUNT_INVALID_REFRESH_OP   2000U
#define INIT                             0U
#define ACTIVE_OUT                       1U
#define ACTIVE_PAUSE                     2U
#define PAUSING_IDLE                     3U
#define SYNCHRONIZE                      4U
#define WIPER_OFF                        0U
#define WIPER_INT                        1U
#define WIPER_CONT                       2U

/* Private macro
---------------------------------------------------------------------------*/
LOG_MODULE_REGISTER(L_tle, CONFIG_LOG_DEFAULT_LEVEL);
#define MSECONDS_TO_TICKS(x) (x) //interrupt enable in cicles of 1 ms
BUILD_ASSERT(MAXCOUNT_INVALID_REFRESH_OP+INVALID_REFRESH_OP_INC_STEP < 0xFFFF, "Invalid tle cfg");
 
/* project specific types
---------------------------------------------------------------------------*/
enum TLE_OUTPUTS{
    WIPER_LEFT_PARKING,
    WIPER_LEFT_HIGH,
    WIPER_LEFT_WASH,
    WIPER_REAR_PARKING,
    WIPER_REAR_HIGH,
    WIPER_REAR_WASH,
    WIPER_RIGHT_PARKING,
    WIPER_RIGHT_HIGH,
    WIPER_RIGHT_WASH
};

typedef union {
	uint16_t u16TLEWholeOut; /*completo;*/
	struct {
		uint8_t Device_A; /*DEVICE_A;*/
		uint8_t Device_B; /*DEVICE_B;*/
	} byte; /*bytes;*/
	struct {
		uint8_t A00 /*WIPER_LEFT_PARK*/     :1;  /* LSB bit DEVICE_A */
		uint8_t A01 /*WIPER_LEFT_HIGH*/     :1;
		uint8_t A02 /*WIPER_LEFT_WASH*/     :1;
		uint8_t A03 /*NOT USED*/            :1;
		uint8_t A04 /*WIPER_RIGHT_PARK*/    :1;
		uint8_t A05 /*WIPER_RIGHT_HIGH*/    :1;
		uint8_t A06 /*WIPER_RIGHT_WASH*/    :1;
		uint8_t A07 /*NOT USED*/            :1;  /* MSB bit DEVICE_A */
        /** ########## ########## **/
        uint8_t B00 /*WIPER_REAR_PARK*/     :1;  /* LSB bit DEVICE_B */
		uint8_t B01 /*WIPER_REAR_HIGH*/     :1;
		uint8_t B02 /*WIPER_REAR_WASH*/     :1;
		uint8_t B03 /*NOT USED*/            :1;
		uint8_t B04 /*NOT USED*/            :1;
		uint8_t B05 /*NOT USED*/            :1;
		uint8_t B06 /*NOT USED*/            :1;
		uint8_t B07 /*NOT USED*/            :1;  /* MSB bit DEVICE_B */
	} bit; /*individual;*/
} U_TLE_st; /*TLE75080-ESD;*/

/* local defined variables
-----------------------------------------------------------------------------*/
static uint8_t tle_driver_buffer[4U] = {0}, tle_driver_bffaux[4U] = {0};
static U_TLE_st tle_Internal_Output = {0};
static U_TLE_st tle_Internal_OutputStatus = {0};
static U_TLE_st tle_Internal_Open_LoadStatus = {0};
static U_TLE_st tle_Internal_Over_LoadStatus = {0};
static U_TLE_st tle_Internal_OpenLoadEn = {0};
static uint8_t rearIntStatus = INIT, leftIntStatus = INIT, rightIntStatus = INIT;
static uint32_t left_wiper_int_cnt = 0;
static uint32_t right_wiper_int_cnt = 0;
static uint32_t rear_wiper_int_cnt = 0;
uint8_t openLoadError_A = 0, openLoadError_B = 0;

/* Local defined functions
-----------------------------------------------------------------------------*/
/** @brief
 */
static RETStatus tle_refresh(const U_TLE_st tle_outputs, U_TLE_st *tle_OpenLoadStatus, U_TLE_st *tle_OverLoadStatus, U_TLE_st *tle_OutputStatus)
{
	RETStatus ret = STATUS_OK;

    //Writing outputs
    gpio_set(OUT_CSN, 1U);
    k_usleep(20);
    tle_driver_buffer[0U] = 0x80;
    tle_driver_buffer[1U] = tle_outputs.byte.Device_B;
    tle_driver_buffer[2U] = 0x80;
    tle_driver_buffer[3U] = tle_outputs.byte.Device_A;
    ret = spi_tle_step(tle_driver_buffer, tle_driver_bffaux, sizeof(tle_driver_buffer));
    gpio_set(OUT_CSN, 0U);
    k_usleep(20);
    LOG_DBG(" MOTOR SPI. Sent info: %02X %02X %02X %02X ; Received info: %02X %02X %02X %02X \n",
    tle_driver_buffer[0], tle_driver_buffer[1], tle_driver_buffer[2], tle_driver_buffer[3],
    tle_driver_bffaux[0], tle_driver_bffaux[1], tle_driver_bffaux[2], tle_driver_bffaux[3] );

    //Reading outputs
    if (ret == STATUS_OK) {
        gpio_set(OUT_CSN, 1U);
        k_usleep(20);
        tle_driver_buffer[0U] = 0x40;
        tle_driver_buffer[1U] = 0x02;
        tle_driver_buffer[2U] = 0x40;
        tle_driver_buffer[3U] = 0x02;
        ret = spi_tle_step(tle_driver_buffer, tle_driver_bffaux, sizeof(tle_driver_buffer));
        gpio_set(OUT_CSN, 0U);
        k_usleep(20);
        //Get standard diagnosis
        tle_OverLoadStatus->byte.Device_A = (tle_driver_bffaux[3U]);
        tle_OverLoadStatus->byte.Device_B = (tle_driver_bffaux[1U]);
        openLoadError_A = tle_driver_bffaux[3U];
        openLoadError_B = tle_driver_bffaux[1U];
        LOG_DBG(" MOTOR SPI. Sent info: %02X %02X %02X %02X ; Received info: %02X %02X %02X %02X \n", tle_driver_buffer[0], tle_driver_buffer[1], tle_driver_buffer[2], tle_driver_buffer[3],
        tle_driver_bffaux[0], tle_driver_bffaux[1], tle_driver_bffaux[2], tle_driver_bffaux[3] );
    }

    //Reading diagnosis output monitor
    if (ret == STATUS_OK) {
        gpio_set(OUT_CSN, 1U);
        k_usleep(20);
        tle_driver_buffer[0U] = 0x49;
        tle_driver_buffer[1U] = 0x02;
        tle_driver_buffer[2U] = 0x49;
        tle_driver_buffer[3U] = 0x02;
        ret = spi_tle_step(tle_driver_buffer, tle_driver_bffaux, sizeof(tle_driver_buffer));
        gpio_set(OUT_CSN, 0U);
        k_usleep(20);
        LOG_DBG(" MOTOR SPI. Sent info: %02X %02X %02X %02X ; Received info: %02X %02X %02X %02X \n", tle_driver_buffer[0], tle_driver_buffer[1], tle_driver_buffer[2], tle_driver_buffer[3],
        tle_driver_bffaux[0], tle_driver_bffaux[1], tle_driver_bffaux[2], tle_driver_bffaux[3] );
        //Get output status
        tle_OutputStatus->byte.Device_A = tle_driver_bffaux[3U];
        tle_OutputStatus->byte.Device_B = tle_driver_bffaux[1U];
    }

    //Writing outputs
    if (ret == STATUS_OK) {
        gpio_set(OUT_CSN, 1U);
        k_usleep(20);
        tle_driver_buffer[0U] = 0x80;
        tle_driver_buffer[1U] = tle_outputs.byte.Device_B;
        tle_driver_buffer[2U] = 0x80;
        tle_driver_buffer[3U] = tle_outputs.byte.Device_A;
        ret = spi_tle_step(tle_driver_buffer, tle_driver_bffaux, sizeof(tle_driver_buffer));
        gpio_set(OUT_CSN, 0U);
        k_usleep(20);
        //Get open load status
        tle_OpenLoadStatus->byte.Device_A = tle_driver_bffaux[3U];
        tle_OpenLoadStatus->byte.Device_B = tle_driver_bffaux[1U];
        LOG_DBG(" MOTOR SPI. Sent info: %02X %02X %02X %02X ; Received info: %02X %02X %02X %02X \n", tle_driver_buffer[0], tle_driver_buffer[1], tle_driver_buffer[2], tle_driver_buffer[3],
        tle_driver_bffaux[0], tle_driver_bffaux[1], tle_driver_bffaux[2], tle_driver_bffaux[3] );
    }

    LOG_DBG(" TLE READINGS. DIAG_OSM dev A = %02X; DIAG_OSM dev B = %02X\n", tle_OpenLoadStatus->byte.Device_A, tle_OpenLoadStatus->byte.Device_B);
    LOG_DBG(" TLE READINGS. OL_OFF dev A = %d, OL_OFF dev B = %d \n", openLoadError_A, openLoadError_B);
    LOG_DBG(" TLE READINGS. ERRn dev A = %02X; ERRn dev B = %02X\n", tle_OverLoadStatus->byte.Device_A, tle_OverLoadStatus->byte.Device_B);
    LOG_DBG(" TLE READINGS. OUT dev A = %02X; OUT dev B = %02X\n\n", tle_OutputStatus->byte.Device_A, tle_OutputStatus->byte.Device_B);

    return ret;
}

/** @brief Configure the Open Load diagnostic current control register with the required values
 *  @param tle_openLoad: data information for the register configuration
 *  @return status of the SPI transceiving operation
 */
static RETStatus tle_OpenLoadDiag_refresh(const U_TLE_st tle_openLoad)
{
    RETStatus ret = STATUS_OK;

    gpio_set(OUT_CSN, 1U);
    k_usleep(20);
    tle_driver_buffer[0U] = 0x88;
    tle_driver_buffer[1U] = tle_openLoad.byte.Device_B;
    tle_driver_buffer[2U] = 0x88;
    tle_driver_buffer[3U] = tle_openLoad.byte.Device_A;
    ret = spi_tle_step(tle_driver_buffer, tle_driver_bffaux, sizeof(tle_driver_buffer) - 1U);
    gpio_set(OUT_CSN, 0U);
    k_usleep(20);

    LOG_DBG(" MOTOR SPI. Sent info: %02X %02X %02X %02X; Received info: %02X %02X %02X %02X\n", tle_driver_buffer[0], tle_driver_buffer[1], tle_driver_buffer[2], tle_driver_buffer[3], 
        tle_driver_bffaux[0], tle_driver_bffaux[1], tle_driver_bffaux[2], tle_driver_bffaux[3]);
    return ret;
}

/** @brief
 */
void tle_FSM (void)
{
    /* TODO: Synchronize motor outputs when they are in intermittence mode */

    //Check which mode of wiper is running in keyboard module
    uint8_t parkPos = get_ParkPosSelected(); //0-Top; 1-Bottom
    uint8_t rightWiperMode = get_ManRightSelected(); //0-Off; 1-Intermittence; 2-Continuous
    uint8_t leftWiperMode = get_ManLeftSelected();
    uint8_t rearWiperMode = get_ManRearSelected();
    uint32_t currentTime = timer_get_totalTicks(E_TIMER_2);

    //Check automatic mode configuration
    if (get_AutoModeSelected() == Intermittence){
        if((get_AutoWipingActive() == Left_wiper) || (get_AutoWipingActive() == Both)) leftWiperMode = WIPER_INT;
        if((get_AutoWipingActive() == Right_wiper) || (get_AutoWipingActive() == Both)) rightWiperMode = WIPER_INT;
    }
    else if(get_AutoModeSelected() == HighSpeed){
        if((get_AutoWipingActive() == Left_wiper) || (get_AutoWipingActive() == Both)) leftWiperMode = WIPER_CONT;
        if((get_AutoWipingActive() == Right_wiper) || (get_AutoWipingActive() == Both)) rightWiperMode = WIPER_CONT;
    }

    /* ## LEFT WIPER ## */
    //Check left wiper intermittence running time
    if (leftWiperMode == WIPER_CONT){
        set_tle_individual_output (1, WIPER_LEFT_HIGH);
        set_tle_individual_output (0, WIPER_LEFT_PARKING);
        leftIntStatus = INIT;
    }
    else if ((leftWiperMode == WIPER_OFF) && (leftIntStatus != ACTIVE_OUT) && (leftIntStatus != ACTIVE_PAUSE)){
        set_tle_individual_output (0, WIPER_LEFT_HIGH);
        set_tle_individual_output (parkPos, WIPER_LEFT_PARKING);
        leftIntStatus = INIT;
    }
    else if ((leftWiperMode == WIPER_INT) || (leftWiperMode == WIPER_OFF)){
        if (leftIntStatus == INIT){
            leftIntStatus = ACTIVE_OUT;
            left_wiper_int_cnt = currentTime + config_get_wiper_pulse_time();
            if(parkPos == Park_top){ set_tle_individual_output (1, WIPER_LEFT_HIGH);}
            else{ set_tle_individual_output (0, WIPER_LEFT_HIGH);}
            set_tle_individual_output (0, WIPER_LEFT_PARKING);
            LOG_DBG("\t LEFT wiper OUT ON in interruption mode, delay to next step = %d \n", config_get_wiper_pulse_time());
        }
        else if ((leftIntStatus == ACTIVE_OUT) && (left_wiper_int_cnt < currentTime) && (abs(left_wiper_int_cnt - currentTime) < config_get_wiper_pulse_time())){
            leftIntStatus = ACTIVE_PAUSE;
            left_wiper_int_cnt = currentTime + MSECONDS_TO_TICKS(config_get_wiper_run_time()) - config_get_wiper_pulse_time();
            set_tle_individual_output (0, WIPER_LEFT_HIGH);
            if(parkPos == Park_bottom){ set_tle_individual_output (parkPos, WIPER_LEFT_PARKING);}
            LOG_DBG("\t LEFT wiper OUT OFF in interruption mode, delay to next step = %d \n", MSECONDS_TO_TICKS(config_get_wiper_run_time()) - config_get_wiper_pulse_time());
        }
        else if ((leftIntStatus == ACTIVE_PAUSE) && (left_wiper_int_cnt < currentTime) && (abs(left_wiper_int_cnt - currentTime) < (MSECONDS_TO_TICKS(config_get_wiper_run_time()) - config_get_wiper_pulse_time()))){
            leftIntStatus = PAUSING_IDLE;
            left_wiper_int_cnt = currentTime + get_IntermittencePausingTime();
            LOG_DBG("\t LEFT wiper STARTING idle pause, delay to next step = %d \n", get_IntermittencePausingTime());
        }
        else if ((leftIntStatus == PAUSING_IDLE) && ((left_wiper_int_cnt < currentTime) && ((abs(left_wiper_int_cnt - currentTime) < get_IntermittencePausingTime()) || (get_IntermittencePausingTime() <= 50)))){
            if ((rightWiperMode == WIPER_INT) || (rearWiperMode == WIPER_INT))
                leftIntStatus = SYNCHRONIZE;
            else 
                leftIntStatus = INIT;
            LOG_DBG("\t LEFT wiper ENDING idle pause \n");
        }
        else if (leftIntStatus == SYNCHRONIZE){
            if((get_IntermittencePausingTime()) > MSECONDS_TO_TICKS(config_get_wiper_run_time())){
                LOG_DBG("\t LEFT wiper SYNCHRONIZED \n");
                leftIntStatus = INIT;
                if((rightIntStatus > ACTIVE_PAUSE) && (rightWiperMode == WIPER_INT)){
                    rightIntStatus = INIT;
                    LOG_DBG("\t RIGHT wiper SYNCHRONIZED \n");
                }
                if((rearIntStatus > ACTIVE_PAUSE) && (rearWiperMode == WIPER_INT)){
                    rearIntStatus = INIT;
                    LOG_DBG("\t REAR wiper SYNCHRONIZED \n");
                }
            } else if (((rightWiperMode == WIPER_INT) && (rearWiperMode == WIPER_INT) && (rightIntStatus == SYNCHRONIZE) && (rearIntStatus == SYNCHRONIZE)) ||
                    ((rightWiperMode == WIPER_INT) && (rearWiperMode != WIPER_INT) && (rightIntStatus == SYNCHRONIZE)) ||
                    ((rightWiperMode != WIPER_INT) && (rearWiperMode == WIPER_INT) && (rearIntStatus == SYNCHRONIZE)) ||
                    ((rightWiperMode != WIPER_INT) && (rearWiperMode != WIPER_INT))) {
                leftIntStatus = INIT;
                LOG_DBG("\t LEFT wiper SYNCHRONIZED \n");
                if((rightIntStatus == SYNCHRONIZE) && (rightWiperMode == WIPER_INT)){
                    rightIntStatus = INIT;
                    LOG_DBG("\t RIGHT wiper SYNCHRONIZED \n");
                }
                if((rearIntStatus == SYNCHRONIZE) && (rearWiperMode == WIPER_INT)){
                    rearIntStatus = INIT;
                    LOG_DBG("\t REAR wiper SYNCHRONIZED \n");
                }
            }
        }
    }

    /* ## RIGHT WIPER ## */
    //Check right wiper intermittence running time
    if (rightWiperMode == WIPER_CONT){
        set_tle_individual_output (1, WIPER_RIGHT_HIGH);
        set_tle_individual_output (0, WIPER_RIGHT_PARKING);
        rightIntStatus = INIT;
    }
    else if ((rightWiperMode == WIPER_OFF) && (rightIntStatus != ACTIVE_OUT) && (rightIntStatus != ACTIVE_PAUSE)){
        set_tle_individual_output (0, WIPER_RIGHT_HIGH);
        set_tle_individual_output (parkPos, WIPER_RIGHT_PARKING);
        rightIntStatus = INIT;
    }
    else if ((rightWiperMode == WIPER_INT) || (rightWiperMode == WIPER_OFF)){
        if (rightIntStatus == INIT){
            rightIntStatus = ACTIVE_OUT;
            right_wiper_int_cnt = currentTime + config_get_wiper_pulse_time();
            if(parkPos == Park_top){ set_tle_individual_output (1, WIPER_RIGHT_HIGH);}
            else{ set_tle_individual_output (0, WIPER_RIGHT_HIGH);}
            set_tle_individual_output (0, WIPER_RIGHT_PARKING);
            LOG_DBG("\t RIGHT wiper OUT ON in interruption mode, delay to next step = %d \n", config_get_wiper_pulse_time());
        }
        else if ((rightIntStatus == ACTIVE_OUT) && (right_wiper_int_cnt < currentTime) && (abs(right_wiper_int_cnt - currentTime) < config_get_wiper_pulse_time())){
            rightIntStatus = ACTIVE_PAUSE;
            right_wiper_int_cnt = currentTime + MSECONDS_TO_TICKS(config_get_wiper_run_time()) - config_get_wiper_pulse_time();
            set_tle_individual_output (0, WIPER_RIGHT_HIGH);
            if(parkPos == Park_bottom){ set_tle_individual_output (parkPos, WIPER_RIGHT_PARKING);}
            LOG_DBG("\t RIGHT wiper OUT OFF in interruption mode, delay to next step = %d \n", MSECONDS_TO_TICKS(config_get_wiper_run_time()) - config_get_wiper_pulse_time());
        }
        else if ((rightIntStatus == ACTIVE_PAUSE) && (right_wiper_int_cnt < currentTime) && (abs(right_wiper_int_cnt - currentTime) < (MSECONDS_TO_TICKS(config_get_wiper_run_time()) - config_get_wiper_pulse_time()))){
            rightIntStatus = PAUSING_IDLE;
            right_wiper_int_cnt = currentTime + get_IntermittencePausingTime();
            LOG_DBG("\t RIGHT wiper STARTING idle pause, delay to next step = %d \n", get_IntermittencePausingTime());
        }
        else if ((rightIntStatus == PAUSING_IDLE) && ((right_wiper_int_cnt < currentTime) && ((abs(right_wiper_int_cnt - currentTime) < get_IntermittencePausingTime()) || (get_IntermittencePausingTime() <= 50)))){
            if ((leftWiperMode == WIPER_INT) || (rearWiperMode == WIPER_INT))
                rightIntStatus = SYNCHRONIZE;
            else
                rightIntStatus = INIT;
            LOG_DBG("\t RIGHT wiper ENDING idle pause \n");
        }
        else if (rightIntStatus == SYNCHRONIZE){
            if(get_IntermittencePausingTime() > MSECONDS_TO_TICKS(config_get_wiper_run_time())){
                rightIntStatus = INIT;
                LOG_DBG("\t RIGHT wiper SYNCHRONIZED \n");
                if((leftIntStatus > ACTIVE_PAUSE) && (leftWiperMode == WIPER_INT)){
                    leftIntStatus = INIT;
                    LOG_DBG("\t LEFT wiper SYNCHRONIZED \n");
                }
                if((rearIntStatus > ACTIVE_PAUSE) && (rearWiperMode == WIPER_INT)){
                    rearIntStatus = INIT;
                    LOG_DBG("\t REAR wiper SYNCHRONIZED \n");
                }
            } else if (((rearWiperMode == WIPER_INT) && (leftWiperMode == WIPER_INT) && (rearIntStatus == SYNCHRONIZE) && (leftIntStatus == SYNCHRONIZE)) ||
                    ((rearWiperMode == WIPER_INT) && (leftWiperMode != WIPER_INT) && (rearIntStatus == SYNCHRONIZE)) ||
                    ((rearWiperMode != WIPER_INT) && (leftWiperMode == WIPER_INT) && (leftIntStatus == SYNCHRONIZE)) ||
                    ((rearWiperMode != WIPER_INT) && (leftWiperMode != WIPER_INT))) {
                rightIntStatus = INIT;
                LOG_DBG("\t RIGHT wiper SYNCHRONIZED \n");
                if((rearIntStatus == SYNCHRONIZE) && (rearWiperMode == WIPER_INT)){
                    rearIntStatus = INIT;
                    LOG_DBG("\t REAR wiper SYNCHRONIZED \n");
                }
                if((leftIntStatus == SYNCHRONIZE) && (leftWiperMode == WIPER_INT)){
                    leftIntStatus = INIT;
                    LOG_DBG("\t LEFT wiper SYNCHRONIZED \n");
                }
            }
        }
    }

    /* ## REAR WIPER ## */
    //Check rear wiper intermittence running mode
    if (rearWiperMode == WIPER_CONT){
        set_tle_individual_output (1, WIPER_REAR_HIGH);
        set_tle_individual_output (0, WIPER_REAR_PARKING);
        rearIntStatus = INIT;
    }
    else if ((rearWiperMode == WIPER_OFF) && (rearIntStatus != ACTIVE_OUT) && (rearIntStatus != ACTIVE_PAUSE)){
        set_tle_individual_output (0, WIPER_REAR_HIGH);
        rearIntStatus = INIT;
    }
    else if ((rearWiperMode == WIPER_INT) || (rearWiperMode == WIPER_OFF)){
        if (rearIntStatus == INIT){
            rearIntStatus = ACTIVE_OUT;
            rear_wiper_int_cnt = currentTime + config_get_wiper_pulse_time();
            set_tle_individual_output (1, WIPER_REAR_HIGH);
            LOG_DBG("\t REAR wiper OUT ON in interruption mode, delay to next step = %d \n", config_get_wiper_pulse_time());
        }
        else if ((rearIntStatus == ACTIVE_OUT) && (rear_wiper_int_cnt < currentTime) && (abs(rear_wiper_int_cnt - currentTime) < config_get_wiper_pulse_time())){
            rearIntStatus = ACTIVE_PAUSE;
            rear_wiper_int_cnt = currentTime + MSECONDS_TO_TICKS(config_get_wiper_run_time()) - config_get_wiper_pulse_time();
            set_tle_individual_output (0, WIPER_REAR_HIGH);
            LOG_DBG("\t REAR wiper OUT OFF in interruption mode, delay to next step = %d \n", MSECONDS_TO_TICKS(config_get_wiper_run_time()) - config_get_wiper_pulse_time());
        }
        else if ((rearIntStatus == ACTIVE_PAUSE) && (rear_wiper_int_cnt < currentTime) && (abs(rear_wiper_int_cnt - currentTime) < (MSECONDS_TO_TICKS(config_get_wiper_run_time()) - config_get_wiper_pulse_time()))){
            rearIntStatus = PAUSING_IDLE;
            rear_wiper_int_cnt = currentTime + get_IntermittencePausingTime();
            LOG_DBG("\t REAR wiper STARTING idle pause, delay to next step = %d \n", get_IntermittencePausingTime());
        }
        else if ((rearIntStatus == PAUSING_IDLE) && ((rear_wiper_int_cnt < currentTime) && ((abs(rear_wiper_int_cnt - currentTime) < get_IntermittencePausingTime()) || (get_IntermittencePausingTime() <= 50)))){
            if ((rightWiperMode == WIPER_INT) || (leftWiperMode == WIPER_INT))
                rearIntStatus = SYNCHRONIZE;
            else 
                rearIntStatus = INIT;
            LOG_DBG("\t REAR wiper ENDING idle pause \n");
        }
        else if (rearIntStatus == SYNCHRONIZE){
            if(get_IntermittencePausingTime() > MSECONDS_TO_TICKS(config_get_wiper_run_time())){
                rearIntStatus = INIT;
                LOG_DBG("\t REAR wiper SYNCHRONIZED \n");
                if((rightIntStatus > ACTIVE_PAUSE) && (rightWiperMode == WIPER_INT)){
                    rightIntStatus = INIT;
                    LOG_DBG("\t RIGHT wiper SYNCHRONIZED \n");
                }
                if((leftIntStatus > ACTIVE_PAUSE) && (leftWiperMode == WIPER_INT)){
                    leftIntStatus = INIT;
                    LOG_DBG("\t LEFT wiper SYNCHRONIZED \n");
                }
            } else if (((rightWiperMode == WIPER_INT) && (leftWiperMode == WIPER_INT) && (rightIntStatus == SYNCHRONIZE) && (leftIntStatus == SYNCHRONIZE)) ||
                    ((rightWiperMode == WIPER_INT) && (leftWiperMode != WIPER_INT) && (rightIntStatus == SYNCHRONIZE)) ||
                    ((rightWiperMode != WIPER_INT) && (leftWiperMode == WIPER_INT) && (leftIntStatus == SYNCHRONIZE)) ||
                    ((rightWiperMode != WIPER_INT) && (leftWiperMode != WIPER_INT))) {
                rearIntStatus = INIT;
                LOG_DBG("\t REAR wiper SYNCHRONIZED \n");
                if((rightIntStatus == SYNCHRONIZE) && (rightWiperMode == WIPER_INT)){
                    rightIntStatus = INIT;
                    LOG_DBG("\t RIGHT wiper SYNCHRONIZED \n");
                }
                if((leftIntStatus == SYNCHRONIZE) && (leftWiperMode == WIPER_INT)){
                    leftIntStatus = INIT;
                    LOG_DBG("\t LEFT wiper SYNCHRONIZED \n");
                }
            }
        }
    }
}

/** @brief
 */
void updateMotorWash(void) 
{
    if(config_get_c_output_enable()) {
        set_tle_individual_output (get_PumpLeftOutput(), WIPER_LEFT_WASH);
        set_tle_individual_output (get_PumpRearOutput(), WIPER_REAR_WASH);
        set_tle_individual_output (get_PumpRightOutput(), WIPER_RIGHT_WASH);
    }
    // else {
    //     set_tle_individual_output (0, WIPER_LEFT_WASH);
    //     set_tle_individual_output (0, WIPER_REAR_WASH);
    //     set_tle_individual_output (0, WIPER_RIGHT_WASH);
    // }
}

/* Global functions
---------------------------------------------------------------------------*/
/** @brief Initialization of the TLE layer. Setting chip select to 0V
 */
void tle_init(void)
{
    MODULE_SINGLE_INIT;

    tle_Internal_Output.u16TLEWholeOut = 0x0000;
    tle_Internal_OutputStatus.u16TLEWholeOut = 0x0000;
    tle_Internal_OpenLoadEn.u16TLEWholeOut = 0x0000;
    tle_Internal_Open_LoadStatus.u16TLEWholeOut = 0x0000;
    tle_Internal_Over_LoadStatus.u16TLEWholeOut = 0x0000;

    tle_Internal_OpenLoadEn.bit.A00 = 1; //Left wiper park
    tle_Internal_OpenLoadEn.bit.A01 = 1; //Left wiper high
    tle_Internal_OpenLoadEn.bit.A04 = 1; //Right wiper park
    tle_Internal_OpenLoadEn.bit.A05 = 1; //Right wiper high
    // tle_Internal_OpenLoadEn.bit.B00 = 1; //Rear wiper park
    tle_Internal_OpenLoadEn.bit.B01 = 1; //Rear wiper high

    if(config_get_c_output_enable()) {
        tle_Internal_OpenLoadEn.bit.A02 = 1; //Left wiper wash
        tle_Internal_OpenLoadEn.bit.A06 = 1; //right wiper wash
        tle_Internal_OpenLoadEn.bit.B02 = 1; //rear wiper wash
    }

    spi_tle_init();
    gpio_set(OUT_IDLE, 1U);

    tle_OpenLoadDiag_refresh (tle_Internal_OpenLoadEn);
}

/** @brief Update the values of the outputs connected to the TLE ICs, checking the refreshing failures
 */
void tle_step(void)
{
    RETStatus retval = STATUS_OK;
    static uint16_t invalid_refresh_op_counter = 0U;

    tle_FSM();
    updateMotorWash();
    retval = tle_refresh(tle_Internal_Output, &tle_Internal_Open_LoadStatus, &tle_Internal_Over_LoadStatus, &tle_Internal_OutputStatus);

    /* Policy for repetitive tle-refreshing failures */
    if (invalid_refresh_op_counter < MAXCOUNT_INVALID_REFRESH_OP)
        invalid_refresh_op_counter += (retval != STATUS_OK) ? INVALID_REFRESH_OP_INC_STEP : 0U;

    if (invalid_refresh_op_counter && (retval == STATUS_OK))
        invalid_refresh_op_counter--;

    if (invalid_refresh_op_counter > MAXCOUNT_INVALID_REFRESH_OP/2)
        LOG_DBG("\n>>> Several invalid tle-refresh op detected! [%d]", invalid_refresh_op_counter);
}

/** @brief Getter for the TLE output internal variable value
 */
uint16_t get_tle_outputs (void) 
{
    return tle_Internal_Output.u16TLEWholeOut;
}

/** @brief Setter for the TLE output internal variable value
 *  @param values: The value of all outputs configured in 2 bytes
 */
void set_tle_outputs (uint16_t values)
{
    tle_Internal_Output.u16TLEWholeOut = values;
}

/** @brief Setter for the TLE output internal variable value
 *  @param value: The value of one output, it can be 1 (activate) or 0 (deactivate)
 *  @param id: Identification code for the output to be configured
 *  @return 0: success setting
 *          1: Not valid value
 *          2: Not valid id
 */
uint8_t set_tle_individual_output (uint8_t value, uint8_t id)
{
    switch (id)
    {
        case(WIPER_LEFT_PARKING):
            if(value!=0 && value!=1) { return 1;}
            tle_Internal_Output.bit.A00= value;
            if(value == 1U)
                set_WiperLeftOutput(get_WiperLeftOutput() | WiperXOutput_Park);
            else
                set_WiperLeftOutput(get_WiperLeftOutput() & (~WiperXOutput_Park));
            break;
        case(WIPER_LEFT_HIGH):
            if(value!=0 && value!=1) { return 1;}
            tle_Internal_Output.bit.A01 = value;
            if(value == 1U)
                set_WiperLeftOutput(get_WiperLeftOutput() | WiperXOutput_High);
            else 
                set_WiperLeftOutput(get_WiperLeftOutput() & (~WiperXOutput_High));
            break;
        case(WIPER_LEFT_WASH):
            if(value!=0 && value!=1) { return 1;}
            tle_Internal_Output.bit.A02 = value;
            if(config_get_c_output_enable()){
                if(value == 1U)
                    set_WiperLeftOutput(get_WiperLeftOutput() | WiperXOutput_Wash);
                else
                    set_WiperLeftOutput(get_WiperLeftOutput() & (~WiperXOutput_Wash));
            }
            break;
        case(WIPER_REAR_PARKING):
            if(value!=0 && value!=1) { return 1;}
            tle_Internal_Output.bit.B00 = value;
            if(value == 1U)
                set_WiperRearOutput(get_WiperRearOutput() | WiperXOutput_Park);
            else
                set_WiperRearOutput(get_WiperRearOutput() & (~WiperXOutput_Park));
            break;
        case(WIPER_REAR_HIGH):
            if(value!=0 && value!=1) { return 1;}
            tle_Internal_Output.bit.B01 = value;
            if(value == 1U)
                set_WiperRearOutput(get_WiperRearOutput() | WiperXOutput_High);
            else
                set_WiperRearOutput(get_WiperRearOutput() & (~WiperXOutput_High));
            break;
        case(WIPER_REAR_WASH):
            if(value!=0 && value!=1) { return 1;}
            tle_Internal_Output.bit.B02 = value;
            if(config_get_c_output_enable()){
                if(value == 1U)
                    set_WiperRearOutput(get_WiperRearOutput() | WiperXOutput_Wash);
                else
                    set_WiperRearOutput(get_WiperRearOutput() & (~WiperXOutput_Wash));
            }
            break;
        case(WIPER_RIGHT_PARKING):
            if(value!=0 && value!=1) { return 1;}
            tle_Internal_Output.bit.A04 = value;
            if(value == 1U)
                set_WiperRightOutput(get_WiperRightOutput() | WiperXOutput_Park);
            else
                set_WiperRightOutput(get_WiperRightOutput() & (~WiperXOutput_Park));
            break;
        case(WIPER_RIGHT_HIGH):
            if(value!=0 && value!=1) { return 1;}
            tle_Internal_Output.bit.A05 = value;
            if(value == 1U)
                set_WiperRightOutput(get_WiperRightOutput() | WiperXOutput_High);
            else
                set_WiperRightOutput(get_WiperRightOutput() & (~WiperXOutput_High));
            break;
        case(WIPER_RIGHT_WASH):
            if(value!=0 && value!=1) { return 1;}
            tle_Internal_Output.bit.A06 = value;
            if(config_get_c_output_enable()){
                if(value == 1U)
                    set_WiperRightOutput(get_WiperRightOutput() | WiperXOutput_Wash);
                else
                    set_WiperRightOutput(get_WiperRightOutput() & (~WiperXOutput_Wash));
            }
            break;
        default:
            return 2;
    }

    return 0;
}

/** @brief Getter for the TLE status internal variable value
 */
uint16_t get_tle_status (void)
{
    return tle_Internal_OutputStatus.u16TLEWholeOut;
}

/** @brief Getter for the TLE open load status value from the OSM register
 *  Remove On outputs from the response, due to they are on as well in OSM
 */

uint16_t get_tle_open_load_status (void)
{
    return tle_Internal_Open_LoadStatus.u16TLEWholeOut;
}


/** @brief Getter for the TLE open load status value from the standard diagnosis response
 */
uint16_t get_tle_OL_OFF_status (void)
{
    return ((openLoadError_B << 8) + openLoadError_A);
}

/** @brief Getter for the TLE over load status value
 */
uint16_t get_tle_over_load_status (void)
{
    return tle_Internal_Over_LoadStatus.u16TLEWholeOut;
}
