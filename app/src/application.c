/**
 * @file   application.c
 * @author Abel Tarragó
 * @date   March 2022
 * @brief  Public APIs and resources for APP module.
 */

/* Header of project specific types
---------------------------------------------------------------------------*/
#include "watchdog.h"
#include "adc.h"
#include "keyb.h"
#include "timer.h"
#include "gpio.h"
#include "led.h"
#include "i2c.h"
#include "can_claas.h"
#include "can_uds.h"
#include "config.h"
#include "uds.h"
#include "application.h"
#include "pwm.h"
#include "fault_manager.h"
#include "eeprom.h"
#include "dgn.h"
#include "tle.h"
#include "assert.h"
#include "stdlib.h"

/* Constant definitions
---------------------------------------------------------------------------*/
#define PRERUNNING_LPMS_TH_SLEEP     15U
#define PRERUNNING_LPMS_TH_RUNNING   10U

/* Private macro
---------------------------------------------------------------------------*/
LOG_MODULE_REGISTER(application, CONFIG_LOG_DEFAULT_LEVEL);

/* Local variables
-----------------------------------------------------------------------------*/

/* External variables
-----------------------------------------------------------------------------*/
extern uint64_t T2u64totalizer;
extern atomic_t eeprombusy_atom_var;
extern atomic_t atom_T2_var;
extern uint16_t value_received;

/* Local functions
---------------------------------------------------------------------------*/

/* Global functions
---------------------------------------------------------------------------*/
/** @brief Initialization function for APP module
 */
void APP_Initialize(void)
{
    //LOG_INF("SW VERSION: %d.%d.%d build: %s - %s BOARD: %s",MSB_SW_VER,MID_SW_VER,LSB_SW_VER, STRINGIFY(BUILD_VERSION), __TIMESTAMP__, STRINGIFY(BOARD));
    LOG_DBG("\t Starting Initialization");

    wdt_init();
    gpio_init();
    eeprom_init();
    config_init();
    adc_init();
    keyb_init();
    led_init();
    tle_init();
    i2c_init();
    can_claas_init();
    can_uds_init();
    uds_init();
    pwm_init();
    dgn_init();
    timer_init();

    LOG_DBG("\t Ending Initialization");
}

/** @brief Main function
 */
void APP_Update(void)
{
    static uint32_t Counter_5ms = 0, Counter_10ms = 0, Counter_40ms = 0, Counter_20ms = 0, Counter_100ms, Counter_500ms = 0, Counter_1000ms = 0U;

    if (abs(T2u64totalizer - Counter_5ms) >= 5U)        /* ~ 5ms */
    {
        Counter_5ms = T2u64totalizer;
        uds_update();
    }

    if (abs(T2u64totalizer - Counter_10ms) >= 10U)      /* ~ 10ms */
    {
        Counter_10ms = T2u64totalizer;
        tle_step();
        adc_step();
        keyb_step();
        eeprom_step();
        dgn_step();
        gpio_step();
    }

    if (abs(T2u64totalizer - Counter_20ms) >= 20U)    /* ~ 20ms */
    {
        Counter_20ms = T2u64totalizer;
    }

    if (abs(T2u64totalizer - Counter_40ms) >= 40U)      /* ~ 40ms */
    {
        Counter_40ms = T2u64totalizer;
        nled_step();
    }

    if (abs(T2u64totalizer - Counter_100ms) >= 100U)      /* ~ 100ms */
    {
        Counter_100ms = T2u64totalizer;
        fault_manager_step();
    }

    if (abs(T2u64totalizer - Counter_500ms) >= 500U)    /* ~ 500ms */
    {
        Counter_500ms = T2u64totalizer;
        can_claas_step(CAN_CYCLIC_500_MS);
    }

    if (abs(T2u64totalizer - Counter_1000ms) >= 1000U)  /* ~ 1000ms */
    {
        Counter_1000ms = T2u64totalizer;
        can_claas_step(CAN_CYCLIC_1000_MS);
        wdext_step();
        wdt_step();
    }
    k_usleep(5000);
}
