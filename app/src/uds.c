/**
 * @file   uds.c
 * @author Abel Tarragó
 * @date   January 2022
 * @brief  Public APIs and resources for UDS module.
 */

/* header of project specific types
---------------------------------------------------------------------------*/
#include "uds.h"
#include "can_claas.h"
#include "can_uds.h"
#include "adc.h"
#include "gpio.h"
#include "watchdog.h"
#include "eeprom.h"
#include "assert.h"
#include "dgn.h"
#include "keyb.h"

/* Constant definitions
---------------------------------------------------------------------------*/
#define UDS_SHAREDM_OFFSET_ECU_HW_PN        0U
#define UDS_SHAREDM_LEN_ECU_HW_PN           4U
#define UDS_SHAREDM_OFFSET_ECU_SN           4U
#define UDS_SHAREDM_LEN_ECU_SN              4U
#define UDS_SHAREDM_OFFSET_ECU_HW_VERSION   8U
#define UDS_SHAREDM_LEN_ECU_HW_VERSION      2U
#define UDS_SHAREDM_OFFSET_ECU_MainPartNb  10U
#define UDS_SHAREDM_LEN_ECU_MainPartNb      4U
#define UDS_SHAREDM_OFFSET_ECU_CHECKSUM    14U
#define UDS_SHAREDM_LEN_ECU_CHECKSUM        4U

/* MSB Supplier code MAXIMA = 34889d = 0x008849 */
#define MSB_MAXIMA_CODE         0x00   
#define MID_MAXIMA_CODE         0x88
#define LSB_MAXIMA_CODE         0x49 
/* ECU Part Number CLAAS 
 * decimal is the last byte */
#define MSB_PART_NUMBER         0x00
#define MID2_PART_NUMBER        0x00
#define MID1_PART_NUMBER        0x00
#define LSB_PART_NUMBER         0x00
/* DIAGNOSTIC SESSIONS */
#define DEFAULT_SESSION_LID     0x01
#define PROGRAMMING_SESSION_LID 0x02
#define EXTENDED_SESSION_LID    0x03
#define MAX_TIME_DIAG_SESSION   1000 /* multiplied by 5ms = 5000 ms = 5s */
/* SECURITY ACCES*/
#define SECURITY_ACCESS_SID     0x27
#define REQ_SEED0_ID		    0x01
#define SEND_KEY0_ID 		    0x02
#define REQ_SEED1_ID			0x03
#define SEND_KEY1_ID    		0x04
#define REQ_SEED2_ID			0x05
#define SEND_KEY2_ID    		0x06
#define REQ_SEED3_ID			0x07
#define SEND_KEY3_ID    		0x08
#define REQ_SEED4_ID			0x09
#define SEND_KEY4_ID    		0x0A
#define REQ_SEED5_ID			0x0B
#define SEND_KEY5_ID    		0x0C
#define SECURITY_ACCES_LOCKED   0x00
#define SECURITY_ACCES_DIAGNOSTIC   0x11
#define SECURITY_ACCES_DOWNLOAD    0x12
#define SECURITY_ACCES_LOW_LEVEL   0x13
#define SECURITY_ACCES_THIRD_PARTY    0x14
#define SECURITY_ACCES_DOWNLOAD_THIRD_PARTY   0x15
#define SECURITY_ACCES_MAXIMATECC  0x16
#define PASSWORD_DIAG           0xDEBBD597  //diagnostic
#define PASSWORD_DOWN           0x3157BE76  //download
#define PASSWORD_LOW            0xB48D5E32  //low level
#define PASSWORD_THIRD          0x7C66A60  //third party
#define PASSWORD_DOWN_THIRD     0x88F192BC  //download third party
#define PASSWORD_MAXI           0xE189ABE1  //Maximatecc
/* NEGATIVE RESPONSE CODES */
#define GENERIC_ERR             0x10
#define UNSUPPORTED_SID         0x11
#define UNSUPPORTED_LID         0x12
#define INVALID_CONFIGURATION   0x13
#define CONDITIONS_NOT_CORRECT  0x22
#define REQUEST_OUT_OF_RANGE    0x31
#define WRONG_KEY               0x35
#define SECURITY_ACCES_DENIED   0x36
#define TRANSFER_DATA_NEGRESP   0x37
#define DATA_CHECKSUM_ERROR     0x77
#define RESPONSE_PENDING        0x78
#define NEGATIVE_RESPONSE       0x7F

/* Private macro
---------------------------------------------------------------------------*/
LOG_MODULE_REGISTER(uds);

/* project specific types
---------------------------------------------------------------------------*/
enum {
    INIT_SESSION,
    DEFAULT_SESSION,
    PROGRAMMING_SESSION,
    EXTENDED_SESSION,
};

/* Local variables
-----------------------------------------------------------------------------*/
volatile uint8_t *bl_sharedm = (uint8_t *) 0x20023EE0;
static uint8_t data_send[300U];
static uint8_t diag_status;
static uint8_t security_level;
static uint8_t security_level_internal;
static uint32_t diagTimeOutCounter;
static uint8_t accessSecurityAttempts;
static uint32_t mySeed;
static uint32_t myKey;
static uint16_t SID_received;
static int adc_value;
static uint8_t data_transfer[300U];
static uint8_t dlc;
static uint8_t mask;
static RETStatus retVal = STATUS_OK;
volatile uint8_t *checksum = (uint8_t *) 0x0800F814;
static uint32_t old_tester_address = 0;
static uint8_t data_eeprom[4];

/* External variables
-----------------------------------------------------------------------------*/
extern uds_can_frame_info recvFrameDiag;
extern dtcstr dtc_filter[E_MAX_DTC];
extern uint32_t last_test_address;
/* Local functions
-----------------------------------------------------------------------------*/
void uds_update_shared_memory(void)
{
    /* /// SHARED ECU_CHECKSUM /// */
    bl_sharedm[UDS_SHAREDM_OFFSET_ECU_HW_PN + 0U] = config_get_ecu_hw_pn(0);
    bl_sharedm[UDS_SHAREDM_OFFSET_ECU_HW_PN + 1U] = config_get_ecu_hw_pn(1);
    bl_sharedm[UDS_SHAREDM_OFFSET_ECU_HW_PN + 2U] = config_get_ecu_hw_pn(2);
    bl_sharedm[UDS_SHAREDM_OFFSET_ECU_HW_PN + 3U] = config_get_ecu_hw_pn(3);
    bl_sharedm[UDS_SHAREDM_OFFSET_ECU_HW_VERSION + 0U] = config_get_ecu_hw_version(0);
    bl_sharedm[UDS_SHAREDM_OFFSET_ECU_HW_VERSION + 1U] = config_get_ecu_hw_version(0);
    bl_sharedm[UDS_SHAREDM_OFFSET_ECU_CHECKSUM + 0U] = checksum[3U];
    bl_sharedm[UDS_SHAREDM_OFFSET_ECU_CHECKSUM + 1U] = checksum[2U];
    bl_sharedm[UDS_SHAREDM_OFFSET_ECU_CHECKSUM + 2U] = checksum[1U];
    bl_sharedm[UDS_SHAREDM_OFFSET_ECU_CHECKSUM + 3U] = checksum[0U];
}


/* Global functions
---------------------------------------------------------------------------*/
void uds_init(void) {
    diag_status = DEFAULT_SESSION;
    diagTimeOutCounter = 0;
    accessSecurityAttempts = 0;
    security_level = SECURITY_ACCES_LOCKED;
    security_level_internal = SECURITY_ACCES_LOCKED;
}

void uds_update(void) {

    RETStatus retsend = STATUS_ERROR;

    if(recvFrameDiag.length != 0)
    {
        if(((old_tester_address == last_test_address) || (!old_tester_address)) &&
           ((last_test_address == CANID_Id_Diagnostics_Req_F9) ||
            /*(last_test_address == CANID_Id_Diagnostics_Req_F6) ||*/
            /*(last_test_address == CANID_Id_Diagnostics_Req_F7) ||*/
            /*(last_test_address == CANID_Id_Diagnostics_Req_F8) ||*/
            (last_test_address == CANID_Id_Diagnostics_Req_FA) ||
            /*(last_test_address == CANID_Id_Diagnostics_Req_FB) ||*/
            (last_test_address == CANID_Id_Diagnostics_Req_FC) //||
            /*(last_test_address == CANID_Id_Diagnostics_Req_FD)*/ )){

            diagTimeOutCounter = 0;
            old_tester_address =  last_test_address;

            switch (recvFrameDiag.data[0])
            {
                case 0x10: /* DIAG_SESSION_CTRL_SID */
                    if (((diag_status == INIT_SESSION) && (recvFrameDiag.data[1] == DEFAULT_SESSION_LID)) ||
                        ((diag_status == DEFAULT_SESSION) && (recvFrameDiag.data[1] == DEFAULT_SESSION_LID)) ||
                        ((diag_status == DEFAULT_SESSION) && (recvFrameDiag.data[1] == EXTENDED_SESSION_LID)) ||
                        ((diag_status == DEFAULT_SESSION) && (recvFrameDiag.data[1] == PROGRAMMING_SESSION_LID)) ||
                        ((diag_status == PROGRAMMING_SESSION) && (recvFrameDiag.data[1] == PROGRAMMING_SESSION_LID)) ||
                        ((diag_status == PROGRAMMING_SESSION) && (recvFrameDiag.data[1] == DEFAULT_SESSION_LID)) ||
                        (diag_status == EXTENDED_SESSION))
                    {
                        if (recvFrameDiag.data[1] == DEFAULT_SESSION_LID) diag_status = DEFAULT_SESSION;
                        if (recvFrameDiag.data[1] == EXTENDED_SESSION_LID) diag_status = EXTENDED_SESSION;

                        diagTimeOutCounter = 0;

                        data_send[0] = recvFrameDiag.data[0] + 0x40;
                        data_send[1] = recvFrameDiag.data[1];
                        data_send[2] = 0x07;
                        data_send[3] = 0xD0;
                        data_send[4] = 0x03;
                        data_send[5] = 0xE8;
                        retsend = can_uds_send_frame(data_send, 6);

                        if (recvFrameDiag.data[1] == PROGRAMMING_SESSION_LID)
                        {
                            k_msleep(1);
                            wdt_loop_swreset(1U);
                        }
                    }
                    else retsend = send_negative_response_code (recvFrameDiag.data[0], CONDITIONS_NOT_CORRECT);
                    break;

                case 0x11: /* ECU_RESET_SID */
                    if ((diag_status == PROGRAMMING_SESSION) || ((diag_status == EXTENDED_SESSION) && (security_level =! SECURITY_ACCES_LOCKED)))
                    {
                        if (recvFrameDiag.data[1] == 0x01) /* ECU_RESET_LID */
                        {
                            retsend = send_positive_response_code();
                            k_msleep(5);
                            wdt_loop_swreset(0U);
                        }
                        else retsend = send_negative_response_code (recvFrameDiag.data[0], CONDITIONS_NOT_CORRECT);
                    } else retsend = send_negative_response_code (recvFrameDiag.data[0], CONDITIONS_NOT_CORRECT);
                    break;

                case 0x14: // CLEAR_DIAG_INFO_SID
                    if ((diag_status == EXTENDED_SESSION) && (security_level =! SECURITY_ACCES_LOCKED))
                    {
                        bool dtc_id = false;
                        uint32_t dtc = (recvFrameDiag.data[1] << 16) + (recvFrameDiag.data[2] << 8) + recvFrameDiag.data[3];
                        for(E_DTC_ID id = 0; id < E_MAX_DTC; id ++){
                            if(dtc_filter[id].ID == dtc) {
                                dtc_id = true;
                                break;
                            }
                        }
                        if((dtc_id) || (dtc == ALL_DTC)){
                            retsend = send_positive_response_code();
                            dgn_clear_dtc(dtc);
                            break;
                        }
                        else retsend = send_negative_response_code (recvFrameDiag.data[0], REQUEST_OUT_OF_RANGE);
                    } else retsend = send_negative_response_code (recvFrameDiag.data[0], CONDITIONS_NOT_CORRECT);
                    break;

                case 0x19: // READ_DTC_INFO_SID
                    if (((diag_status == EXTENDED_SESSION) && (security_level =! SECURITY_ACCES_LOCKED)) ||  (diag_status == DEFAULT_SESSION))
                    {
                        mask = recvFrameDiag.data[2];
                        if (recvFrameDiag.data[1] == 0x01){ // NUMBER_OF_DTC_LID
                            data_send[0] = recvFrameDiag.data[0] + 0x40;
                            data_send[1] = recvFrameDiag.data[1];
                            data_send[2] = recvFrameDiag.data[2];
                            data_send[3] = 0x01; // DTC Format Identifier = ISO14229-1
                            dgn_get_number_of_dtc(mask, data_transfer, &dlc);
                            data_send[4] = data_transfer[0];
                            data_send[5] = data_transfer[1];
                            retsend = can_uds_send_frame(data_send, 6);

                        } else if (recvFrameDiag.data[1] == 0x02){ // STATUS_OF_DTC_LID
                            data_send[0] = recvFrameDiag.data[0] + 0x40;
                            data_send[1] = recvFrameDiag.data[1];
                            data_send[2] = recvFrameDiag.data[2];
                            dgn_get_status_of_dtc(mask, data_transfer, &dlc);
                            memcpy(&data_send[3], data_transfer, dlc);
                            retsend = can_uds_send_frame(data_send, dlc + 3);

                        } else if (recvFrameDiag.data[1] == 0x04){ // SNAPSHOT_OF_DTC_LID
                            data_send[0] = recvFrameDiag.data[0] + 0x40;
                            data_send[1] = recvFrameDiag.data[1];
                            data_send[2] = recvFrameDiag.data[2];
                            data_send[3] = recvFrameDiag.data[3];
                            data_send[4] = recvFrameDiag.data[4];
                            uint32_t dtc = (recvFrameDiag.data[2] << 16) + (recvFrameDiag.data[3] << 8) + recvFrameDiag.data[4];
                            dgn_get_snapshoot_data_of_dtc(data_transfer, &dlc, dtc);
                            memcpy(&data_send[5], data_transfer, dlc);
                            retsend = can_uds_send_frame(data_send, dlc + 5);
                        } else if (recvFrameDiag.data[1] == 0x06){ // EXTENDED_DATA_DTC_LID
                            data_send[0] = recvFrameDiag.data[0] + 0x40;
                            data_send[1] = recvFrameDiag.data[1];
                            data_send[2] = recvFrameDiag.data[2];
                            data_send[3] = recvFrameDiag.data[3];
                            data_send[4] = recvFrameDiag.data[4];
                            uint32_t dtc = (recvFrameDiag.data[2] << 16) + (recvFrameDiag.data[3] << 8) + recvFrameDiag.data[4];
                            dgn_get_extended_data_of_dtc(data_transfer, &dlc, dtc);
                            memcpy(&data_send[5], data_transfer, dlc);
                            retsend = can_uds_send_frame(data_send, dlc + 5);

                        } else if (recvFrameDiag.data[1] == 0x0A) { // SUPPORTED_DTC_LID
                            uint16_t dlc_supported = 0;
                            data_send[0] = recvFrameDiag.data[0] + 0x40;
                            data_send[1] = recvFrameDiag.data[1];
                            dgn_get_supported_dtc(data_transfer, &dlc_supported);
                            memcpy(&data_send[2], data_transfer, dlc_supported);
                            retsend = can_uds_send_frame(data_send, dlc_supported + 2);
        
                        } else retsend = send_negative_response_code (recvFrameDiag.data[0], UNSUPPORTED_LID);
                    } else retsend = send_negative_response_code (recvFrameDiag.data[0], CONDITIONS_NOT_CORRECT);
                    break;
                    
                case 0x3E: /* TESTER_PRESENT_SID */
                    if (recvFrameDiag.data[1] == 0x00) /* TESTER_PRESENT_LID */
                    {
                        diagTimeOutCounter = 0;
                        retsend = send_positive_response_code();
                    } else retsend = send_negative_response_code (recvFrameDiag.data[0], CONDITIONS_NOT_CORRECT);
                    break;

                case 0x22: /* READ_DATA_BY_IDENTIFIER_SID */
                    if(recvFrameDiag.length <= 7){
                        if (((diag_status == EXTENDED_SESSION) && (security_level =! SECURITY_ACCES_LOCKED)) || (diag_status == DEFAULT_SESSION))
                        {
                            SID_received = recvFrameDiag.data[1] << 8 | recvFrameDiag.data[2];
                            switch (SID_received)
                            {
                                case 0x1011: /* ECU_WORKING_HOUR */
                                        data_send[0] = recvFrameDiag.data[0] + 0x40;
                                        data_send[1] = recvFrameDiag.data[1];
                                        data_send[2] = recvFrameDiag.data[2];
                                        data_send[3] = (uint8_t) ((config_get_working_hour() & 0xFF000000) >> 24); /* MSB (secs) */
                                        data_send[4] = (uint8_t) ((config_get_working_hour() & 0x00FF0000) >> 16);
                                        data_send[5] = (uint8_t) ((config_get_working_hour() & 0x0000FF00) >> 8);
                                        data_send[6] = (uint8_t) config_get_working_hour(); /* LSB (secs) */
                                        retsend = can_uds_send_frame(data_send, 7);
                                        break;
                                case 0xF100: // ECU_PIN
                                        data_send[0] = recvFrameDiag.data[0] + 0x40;
                                        data_send[1] = recvFrameDiag.data[1];
                                        data_send[2] = recvFrameDiag.data[2];
                                        data_send[3] = 0x02; /* UDS protocol */
                                        data_send[4] = 0x20; /* tractor (ET88) from CLAAS */
                                        data_send[5] = 0x01; /* ECU TYPE ? */
                                        data_send[6] = 0x56; /* Source Address ? */
                                        retsend = can_uds_send_frame(data_send, 7);
                                        break;
                                case 0xF101: // ECU_Checksum
                                        data_send[0] = recvFrameDiag.data[0] + 0x40;
                                        data_send[1] = recvFrameDiag.data[1];
                                        data_send[2] = recvFrameDiag.data[2];
                                        data_send[3] = checksum[3U];
                                        data_send[4] = checksum[2U];
                                        data_send[5] = checksum[1U];
                                        data_send[6] = checksum[0U];
                                        retsend = can_uds_send_frame(data_send, 7);
                                        break;
                                case 0xF103: // ECU_FBL_QUESTION
                                        data_send[0] = recvFrameDiag.data[0] + 0x40;
                                        data_send[1] = recvFrameDiag.data[1];
                                        data_send[2] = recvFrameDiag.data[2];
                                        data_send[3] = 0x02; //ApplicationAcces*/
                                        data_send[4] = 0x00; // reserved
                                        retsend = can_uds_send_frame(data_send, 5);
                                        break;
                                case 0xF105: // ECU_SUPPLIER_CODE
                                        data_send[0] = recvFrameDiag.data[0] + 0x40;
                                        data_send[1] = recvFrameDiag.data[1];
                                        data_send[2] = recvFrameDiag.data[2];
                                        data_send[3] = MSB_MAXIMA_CODE;
                                        data_send[4] = MID_MAXIMA_CODE;
                                        data_send[5] = LSB_MAXIMA_CODE;
                                        retsend = can_uds_send_frame(data_send, 6);
                                        break;
                                case 0xF180: /* FBL_SW_VERSION */
                                        data_send[0] = recvFrameDiag.data[0] + 0x40;
                                        data_send[1] = recvFrameDiag.data[1];
                                        data_send[2] = recvFrameDiag.data[2];
                                        data_send[3] = 0x05; /* Version High Byte */
                                        data_send[4] = 0x00; /* Version Low Byte */
                                        retsend = can_uds_send_frame(data_send, 5);
                                        break;
                                case 0xF181: /* ECU_SOFTWARE */
                                        data_send[0] = recvFrameDiag.data[0] + 0x40;
                                        data_send[1] = recvFrameDiag.data[1];
                                        data_send[2] = recvFrameDiag.data[2];
                                        data_send[3] = 0x00; // Software builder: Extern
                                        data_send[4] = MSB_SW_VER;
                                        data_send[5] = MID_SW_VER;
                                        data_send[6] = LSB_SW_VER;
                                        retsend = can_uds_send_frame(data_send, 7);
                                        break;
                                case 0xF187: // ECU_MAIN_PN
                                        eeprom_read(ECU_MainPartNumber, data_eeprom);
                                        data_send[0] = recvFrameDiag.data[0] + 0x40;
                                        data_send[1] = recvFrameDiag.data[1];
                                        data_send[2] = recvFrameDiag.data[2];
                                        data_send[3] = data_eeprom[0];
                                        data_send[4] = data_eeprom[1];
                                        data_send[5] = data_eeprom[2];
                                        data_send[6] = data_eeprom[3];
                                        retsend = can_uds_send_frame(data_send, 7);
                                        break;
                                case 0xF188: // ECU_SW_PN
                                        data_send[0] = recvFrameDiag.data[0] + 0x40;
                                        data_send[1] = recvFrameDiag.data[1];
                                        data_send[2] = recvFrameDiag.data[2];
                                        data_send[3] = MSB_PART_NUMBER;
                                        data_send[4] = MID2_PART_NUMBER;
                                        data_send[5] = MID1_PART_NUMBER;
                                        data_send[6] = LSB_PART_NUMBER;
                                        retsend = can_uds_send_frame(data_send, 7);
                                        break;
                                case 0xF18C: // ECU_SERIAL_NUMBER
                                        data_send[0] = recvFrameDiag.data[0] + 0x40;
                                        data_send[1] = recvFrameDiag.data[1];
                                        data_send[2] = recvFrameDiag.data[2];
                                        data_send[3] = config_get_ecu_sn(0); // from maxima
                                        data_send[4] = config_get_ecu_sn(1);
                                        data_send[5] = config_get_ecu_sn(2);
                                        data_send[6] = config_get_ecu_sn(3);
                                        retsend = can_uds_send_frame(data_send, 7);
                                        break;
                                case 0xF191: // ECU_HW_PN
                                        data_send[0] = recvFrameDiag.data[0] + 0x40;
                                        data_send[1] = recvFrameDiag.data[1];
                                        data_send[2] = recvFrameDiag.data[2];
                                        data_send[3] = 0;
                                        data_send[4] = 0;
                                        data_send[5] = 0;
                                        data_send[6] = 0;
                                        data_send[7] = config_get_ecu_hw_pn(0);
                                        data_send[8] = config_get_ecu_hw_pn(1);
                                        data_send[9] = config_get_ecu_hw_pn(2);
                                        data_send[10] = config_get_ecu_hw_pn(3);
                                        retsend = can_uds_send_frame(data_send, 11U);
                                        break;
                                case 0xF193: // ECU_HW_VERSION
                                        data_send[0] = recvFrameDiag.data[0] + 0x40;
                                        data_send[1] = recvFrameDiag.data[1];
                                        data_send[2] = recvFrameDiag.data[2];
                                        data_send[3] = config_get_ecu_hw_version(0);
                                        data_send[4] = config_get_ecu_hw_version(1);
                                        retsend = can_uds_send_frame(data_send, 5);
                                        break;

                                case 0x3000:
                                        adc_get(MUXED_ADC_CHKEY1, &adc_value);
                                        retsend = send_positive_response_code_for_configuration(adc_value);
                                        break;
                                case 0x3001:
                                        adc_get(MUXED_ADC_CHKEY2, &adc_value);
                                        retsend = send_positive_response_code_for_configuration(adc_value);
                                        break;
                                case 0x3002:
                                        adc_get(MUXED_ADC_CHKEY3, &adc_value);
                                        retsend = send_positive_response_code_for_configuration(adc_value);
                                        break;
                                case 0x3003:
                                        adc_get(MUXED_ADC_CHKEY4, &adc_value);
                                        retsend = send_positive_response_code_for_configuration(adc_value);
                                        break;
                                case 0x3004:
                                        adc_get(MUXED_ADC_CHKEY5, &adc_value);
                                        retsend = send_positive_response_code_for_configuration(adc_value);
                                        break;
                                case 0x3005:
                                        adc_get(MUXED_ADC_CHKEY6, &adc_value);
                                        retsend = send_positive_response_code_for_configuration(adc_value);
                                        break;
                                case 0x3006:
                                        adc_get(MUXED_ADC_CHKEY7, &adc_value);
                                        retsend = send_positive_response_code_for_configuration(adc_value);
                                        break;
                                case 0x3007:
                                        adc_get(MUXED_ADC_CHKEY8, &adc_value);
                                        retsend = send_positive_response_code_for_configuration(adc_value);
                                        break;
                                case 0x3008:
                                        adc_get(ADC_VBATT_AI, &adc_value);
                                        retsend = send_positive_response_code_for_configuration(adc_value);
                                        break;
                                case 0x3009:
                                        adc_get(ADC_DIAG_WASH_LEFT, &adc_value);
                                        retsend = send_positive_response_code_for_configuration(adc_value);
                                        break;
                                case 0x300A:
                                        adc_get(ADC_DIAG_WASH_REAR, &adc_value);
                                        retsend = send_positive_response_code_for_configuration(adc_value);
                                        break;
                                case 0x300B:
                                        adc_get(ADC_DIAG_WASH_RIGHT, &adc_value);
                                        retsend = send_positive_response_code_for_configuration(adc_value);
                                        break;
                                case 0x300C:
                                        retsend = send_positive_response_code_for_configuration(config_get_reset_time());
                                        break;
                                case 0x300D:
                                        retsend = send_positive_response_code_for_configuration(config_get_max_pump_time());
                                        break;
                                case 0x300E:
                                        retsend = send_positive_response_code_for_configuration(config_get_pause_pump_ratio());
                                        break;
                                case 0x300F:
                                        retsend = send_positive_response_code_for_configuration(config_get_present_interval_time());
                                        break;
                                case 0x3010:
                                        retsend = send_positive_response_code_for_configuration(config_get_min_interval_time());
                                        break;
                                case 0x3011:
                                        retsend = send_positive_response_code_for_configuration(config_get_max_interval_time());
                                        break;
                                case 0x3012:
                                        retsend = send_positive_response_code_for_buttons_can_id(config_get_wiper_run_time());
                                        break;
                                case 0x3013:
                                        retsend = send_positive_response_code_for_buttons_can_id(config_get_wiper_pulse_time());
                                        break;
                                case 0x3014:
                                        retsend = send_positive_response_code_for_buttons_info(config_get_c_output_enable());
                                        break;
                                case 0x3015:
                                        retsend = send_positive_response_code_for_configuration(config_get_washer_ol());
                                        break;
                                case 0x3016:
                                        retsend = send_positive_response_code_for_configuration(config_get_washer_scgnd());
                                        break;
                                case 0x3017:
                                        retsend = send_positive_response_code_for_buttons_info(config_get_pwm_max_limit());
                                        break;
                                case 0x3018:
                                        retsend = send_positive_response_code_for_buttons_info(config_get_pwm_min_limit());
                                        break;
                                default:
                                        retsend = send_negative_response_code (recvFrameDiag.data[0], UNSUPPORTED_LID);
                                        break;
                            }
                        }else retsend = send_negative_response_code (recvFrameDiag.data[0], CONDITIONS_NOT_CORRECT);
                    }else retsend = send_negative_response_code (recvFrameDiag.data[0], CONDITIONS_NOT_CORRECT);
                    break;

                case 0x2E: // WRITE_DATA_BY_IDENTIFIER_SID
                        if ((diag_status == EXTENDED_SESSION) || (diag_status == DEFAULT_SESSION))
                        {
                            SID_received = recvFrameDiag.data[1] << 8 | recvFrameDiag.data[2];
                            if ((security_level == SECURITY_ACCES_DIAGNOSTIC) || (security_level == SECURITY_ACCES_DOWNLOAD) || (security_level == SECURITY_ACCES_LOW_LEVEL) || (security_level == SECURITY_ACCES_THIRD_PARTY) || (security_level == SECURITY_ACCES_DOWNLOAD_THIRD_PARTY))
                            {
                                switch (SID_received)
                                {
                                    case 0xF187: // ECU_MAIN_PN
                                        retVal = eeprom_write_nowait(ECU_MainPartNumber, &recvFrameDiag.data[3]);
                                        config_set_main_pn(&recvFrameDiag.data[3]);
                                        if(retVal == STATUS_OK)
                                            retsend = send_positive_response_code();
                                        else
                                            send_negative_response_code (recvFrameDiag.data[0], GENERIC_ERR);
                                        break;
                                    default:
                                        retsend = send_negative_response_code (recvFrameDiag.data[0], UNSUPPORTED_LID);
                                        break;
                                }
                            }else if (security_level == SECURITY_ACCES_MAXIMATECC)
                            {
                                switch (SID_received)
                                {
                                    case 0xF187: // ECU_MAIN_PN
                                        retVal = eeprom_write_nowait(ECU_MainPartNumber, &recvFrameDiag.data[3]);
                                        config_set_main_pn(&recvFrameDiag.data[3]);
                                        if(retVal == STATUS_OK)
                                            retsend = send_positive_response_code();
                                        else
                                            send_negative_response_code (recvFrameDiag.data[0], GENERIC_ERR);
                                        break;
                                    case 0xF191: // ECU_HW_PN
                                        retVal = eeprom_write_nowait(ECU_HW_PN, &recvFrameDiag.data[7]);
                                        if(retVal == STATUS_OK)
                                            retsend = send_positive_response_code();
                                        else
                                            send_negative_response_code (recvFrameDiag.data[0], GENERIC_ERR);
                                        break;
                                    case 0xF18C: // ECU_SERIAL_NUMBER
                                        retVal = eeprom_write_nowait(ECU_SN, &recvFrameDiag.data[3]);
                                        if(retVal == STATUS_OK)
                                            retsend = send_positive_response_code();
                                        else
                                            send_negative_response_code (recvFrameDiag.data[0], GENERIC_ERR);
                                        break;
                                    case 0xF193: // ECU_HW_VERSION
                                        retVal = eeprom_write_nowait(ECU_HW_VERSION, &recvFrameDiag.data[3]);
                                        if(retVal == STATUS_OK)
                                            retsend = send_positive_response_code();
                                        else
                                            send_negative_response_code (recvFrameDiag.data[0], GENERIC_ERR);
                                        break;

                                    case 0x300C:
                                        retVal = eeprom_write_nowait(RESET_TIME, &recvFrameDiag.data[3]);
                                        if(retVal == STATUS_OK)
                                            retsend = send_positive_response_code();
                                        else
                                            send_negative_response_code (recvFrameDiag.data[0], GENERIC_ERR);
                                        break;
                                    case 0x300D:
                                        retVal = eeprom_write_nowait(MAX_PUMP_TIME, &recvFrameDiag.data[3]);
                                        if(retVal == STATUS_OK)
                                            retsend = send_positive_response_code();
                                        else
                                            send_negative_response_code (recvFrameDiag.data[0], GENERIC_ERR);
                                        break;
                                    case 0x300E:
                                        retVal = eeprom_write_nowait(PAUSE_PUMP_RATIO, &recvFrameDiag.data[3]);
                                        if(retVal == STATUS_OK)
                                            retsend = send_positive_response_code();
                                        else
                                            send_negative_response_code (recvFrameDiag.data[0], GENERIC_ERR);
                                        break;
                                    case 0x300F:
                                        retVal = eeprom_write_nowait(PRESENT_INTERVAL_TIME, &recvFrameDiag.data[3]);
                                        if(retVal == STATUS_OK)
                                            retsend = send_positive_response_code();
                                        else
                                            send_negative_response_code (recvFrameDiag.data[0], GENERIC_ERR);
                                        break;
                                    case 0x3010:
                                        retVal = eeprom_write_nowait(MIN_INTERVAL_TIME, &recvFrameDiag.data[3]);
                                        if(retVal == STATUS_OK)
                                            retsend = send_positive_response_code();
                                        else
                                            send_negative_response_code (recvFrameDiag.data[0], GENERIC_ERR);
                                        break;
                                    case 0x3011:
                                        retVal = eeprom_write_nowait(MAX_INTERVAL_TIME, &recvFrameDiag.data[3]);
                                        if(retVal == STATUS_OK)
                                            retsend = send_positive_response_code();
                                        else
                                            send_negative_response_code (recvFrameDiag.data[0], GENERIC_ERR);
                                        break;
                                    case 0x3012:
                                        retVal = eeprom_write_nowait(WIPER_RUN_TIME, &recvFrameDiag.data[3]);
                                        if(retVal == STATUS_OK)
                                            retsend = send_positive_response_code();
                                        else
                                            send_negative_response_code (recvFrameDiag.data[0], GENERIC_ERR);
                                        break;
                                    case 0x3013:
                                        retVal = eeprom_write_nowait(WIPER_PULSE_TIME , &recvFrameDiag.data[3]);
                                        if(retVal == STATUS_OK)
                                            retsend = send_positive_response_code();
                                        else
                                            send_negative_response_code (recvFrameDiag.data[0], GENERIC_ERR);
                                        break;
                                    case 0x3014:
                                        retVal = eeprom_write_nowait(C_OUTPUT_ENABLE , &recvFrameDiag.data[3]);
                                        if(retVal == STATUS_OK)
                                            retsend = send_positive_response_code();
                                        else
                                            send_negative_response_code (recvFrameDiag.data[0], GENERIC_ERR);
                                        break;
                                    case 0x3015:
                                        retVal = eeprom_write_nowait(WASHER_OL , &recvFrameDiag.data[3]);
                                        if(retVal == STATUS_OK)
                                            retsend = send_positive_response_code();
                                        else
                                            send_negative_response_code (recvFrameDiag.data[0], GENERIC_ERR);
                                        break;
                                    case 0x3016:
                                        retVal = eeprom_write_nowait(WASHER_SCGND , &recvFrameDiag.data[3]);
                                        if(retVal == STATUS_OK)
                                            retsend = send_positive_response_code();
                                        else
                                            send_negative_response_code (recvFrameDiag.data[0], GENERIC_ERR);
                                        break;
                                    case 0x3017:
                                        retVal = eeprom_write_nowait(PWM_MAX_PERCENTAGE, &recvFrameDiag.data[3]);
                                        if(retVal == STATUS_OK)
                                            retsend = send_positive_response_code();
                                        else
                                            send_negative_response_code (recvFrameDiag.data[0], GENERIC_ERR);
                                        break;
                                    case 0x3018:
                                        retVal = eeprom_write_nowait(PWM_MIN_PERCENTAGE, &recvFrameDiag.data[3]);
                                        if(retVal == STATUS_OK)
                                            retsend = send_positive_response_code();
                                        else
                                            send_negative_response_code (recvFrameDiag.data[0], GENERIC_ERR);
                                        break;
                                    default:
                                        retsend = send_negative_response_code (recvFrameDiag.data[0], UNSUPPORTED_LID);
                                        break;
                                }
                            }else retsend = send_negative_response_code (recvFrameDiag.data[0], CONDITIONS_NOT_CORRECT);
                        }else retsend = send_negative_response_code (recvFrameDiag.data[0], SECURITY_ACCES_DENIED);
                    break;

                case 0x27: // SECURITY_ACCESS_SID

                    if ((diag_status == EXTENDED_SESSION) || (diag_status == DEFAULT_SESSION))
                    {
                        if ((recvFrameDiag.data[1] == REQ_SEED0_ID) || (recvFrameDiag.data[1] == REQ_SEED1_ID) || (recvFrameDiag.data[1] == REQ_SEED2_ID) || (recvFrameDiag.data[1] == REQ_SEED3_ID) || (recvFrameDiag.data[1] == REQ_SEED4_ID) || (recvFrameDiag.data[1] == REQ_SEED5_ID))
                        {
                            if ((security_level != SECURITY_ACCES_LOCKED) && (security_level != 1)) mySeed = 0;
                            else mySeed = config_get_working_hour();

                            security_level_internal = recvFrameDiag.data[1];

                            switch (security_level_internal) {
                                    case  REQ_SEED0_ID:     
                                        myKey = ((((mySeed ^ PASSWORD_DIAG) << 16) & 0xFFFFFFFF) + (mySeed ^ PASSWORD_DIAG) + PASSWORD_DIAG) & 0xFFFFFFFF;
                                        break;
                                    case  REQ_SEED1_ID:
                                        myKey = ((((mySeed ^ PASSWORD_DOWN) << 12) & 0xFFFFFFFF) + (mySeed ^ PASSWORD_DOWN) + PASSWORD_DOWN) & 0xFFFFFFFF;
                                        break;
                                    case  REQ_SEED2_ID:
                                        myKey = ((((mySeed ^ PASSWORD_LOW) << 10) & 0xFFFFFFFF) + (mySeed ^ PASSWORD_LOW) + PASSWORD_LOW) & 0xFFFFFFFF;
                                        break;
                                    case  REQ_SEED3_ID:
                                        myKey = ((((mySeed ^ PASSWORD_THIRD) << 8) & 0xFFFFFFFF) + (mySeed ^ PASSWORD_THIRD) + PASSWORD_THIRD) & 0xFFFFFFFF;
                                        break;
                                    case  REQ_SEED4_ID:
                                        myKey = ((((mySeed ^ PASSWORD_DOWN_THIRD) << 4) & 0xFFFFFFFF) + (mySeed ^ PASSWORD_DOWN_THIRD) + PASSWORD_DOWN_THIRD) & 0xFFFFFFFF;
                                        break;
                                    case  REQ_SEED5_ID:
                                        myKey = ((((mySeed ^ PASSWORD_MAXI) << 16) & 0xFFFFFFFF) + (mySeed ^ PASSWORD_MAXI) + PASSWORD_MAXI) & 0xFFFFFFFF;
                                        break;
                            }

                            data_send[0] = recvFrameDiag.data[0] + 0x40;
                            data_send[1] = recvFrameDiag.data[1];
                            data_send[2] = (uint8_t) (mySeed >> 24);
                            data_send[3] = (uint8_t) (mySeed >> 16);
                            data_send[4] = (uint8_t) (mySeed >> 8);
                            data_send[5] = (uint8_t) mySeed;
                            retsend = can_uds_send_frame(data_send, 6);

                        } else if ((recvFrameDiag.data[1] == SEND_KEY0_ID) || (recvFrameDiag.data[1] == SEND_KEY1_ID) || (recvFrameDiag.data[1] == SEND_KEY2_ID) || (recvFrameDiag.data[1] == SEND_KEY3_ID) || (recvFrameDiag.data[1] == SEND_KEY4_ID) || (recvFrameDiag.data[1] == SEND_KEY5_ID)){
                            
                            if (accessSecurityAttempts >= 3) {
                                retsend = send_negative_response_code (recvFrameDiag.data[0], SECURITY_ACCES_DENIED);
                                diag_status = DEFAULT_SESSION;
                                security_level = SECURITY_ACCES_LOCKED;
                            }
                            else{
                                if ((myKey == (((recvFrameDiag.data[2] << 24) + (recvFrameDiag.data[3] << 16) + (recvFrameDiag.data[4] << 8) + recvFrameDiag.data[5]))) && ((security_level_internal + 1) == (recvFrameDiag.data[1]))) {

                                    switch (recvFrameDiag.data[1]) {
                                        case  SEND_KEY0_ID:
                                            security_level = SECURITY_ACCES_DIAGNOSTIC;
                                            break;
                                        case  SEND_KEY1_ID:
                                            security_level = SECURITY_ACCES_DOWNLOAD;
                                            break;
                                        case  SEND_KEY2_ID:
                                            security_level = SECURITY_ACCES_LOW_LEVEL;
                                            break;
                                        case  SEND_KEY3_ID:
                                            security_level = SECURITY_ACCES_THIRD_PARTY;
                                            break;
                                        case  SEND_KEY4_ID:
                                            security_level = SECURITY_ACCES_DOWNLOAD_THIRD_PARTY;
                                            break;
                                        case  SEND_KEY5_ID:
                                            security_level = SECURITY_ACCES_MAXIMATECC;
                                            break;
                                    }
                                    retsend = send_positive_response_code();
                                }
                                else
                                {
                                    security_level = SECURITY_ACCES_LOCKED;
                                    accessSecurityAttempts++;
                                    retsend = send_negative_response_code (recvFrameDiag.data[0], WRONG_KEY);
                                }
                            }
                        }else
                        {
                            send_negative_response_code (recvFrameDiag.data[0], UNSUPPORTED_LID);
                        }
                    }
                    else
                    {
                        retsend = send_negative_response_code (recvFrameDiag.data[0], CONDITIONS_NOT_CORRECT);
                    }
                    break;
                case 0xAB: // ERASE EEPROM
                    set_erase_eeprom(true);
                    send_positive_response_code();
                    break;
                default:
                    retsend = STATUS_OK;
                    send_negative_response_code (recvFrameDiag.data[0], UNSUPPORTED_SID);
                    break;
            }
        }else retsend = send_negative_response_code (recvFrameDiag.data[0], CONDITIONS_NOT_CORRECT);
    }

    if (retsend != STATUS_ERROR)
    {
        recvFrameDiag.length = 0;
        recvFrameDiag.data[0] = 0x00;
    }

    diagTimeOutCounter++;
    if (diagTimeOutCounter == MAX_TIME_DIAG_SESSION)
    {
        old_tester_address = 0;
        diag_status = DEFAULT_SESSION;
        security_level = SECURITY_ACCES_LOCKED;
        diagTimeOutCounter = 0;
    }
}

RETStatus send_negative_response_code (uint8_t serviceId, uint8_t errorCode) 
{
    data_send[0] = NEGATIVE_RESPONSE;
    data_send[1] = serviceId;
    data_send[2] = errorCode;
    return (can_uds_send_frame(data_send, 3));
}
RETStatus send_positive_response_code(void)
{
    data_send[0] = recvFrameDiag.data[0] + 0x40;
    data_send[1] = recvFrameDiag.data[1];
    data_send[2] = recvFrameDiag.data[2];
    data_send[3] = recvFrameDiag.data[3];

    if( (recvFrameDiag.data[0] == 0x3E) || 
        (recvFrameDiag.data[0] == 0x27) || 
        (recvFrameDiag.data[0] == 0xAB) || 
        (recvFrameDiag.data[0] == 0x11) )   return (can_uds_send_frame(data_send, 2));
    else if (recvFrameDiag.data[0] == 0x14) return (can_uds_send_frame(data_send, 4));
    else return (can_uds_send_frame(data_send, 3));
}
RETStatus send_positive_response_code_for_buttons_can_id(uint32_t can_id)
{
    data_send[0] = recvFrameDiag.data[0] + 0x40;
    data_send[1] = recvFrameDiag.data[1];
    data_send[2] = recvFrameDiag.data[2];
    data_send[3] = (uint8_t) (can_id >> 24);
    data_send[4] = (uint8_t) ((can_id & 0x00FF0000) >> 16);
    data_send[5] = (uint8_t) ((can_id & 0x0000FF00) >> 8);
    data_send[6] = (uint8_t) can_id;
    return (can_uds_send_frame(data_send, 7));
}
RETStatus send_positive_response_code_for_buttons_info(uint8_t info)
{
    data_send[0] = recvFrameDiag.data[0] + 0x40;
    data_send[1] = recvFrameDiag.data[1];
    data_send[2] = recvFrameDiag.data[2];
    data_send[3] = info;
    return (can_uds_send_frame(data_send, 4));
}
RETStatus send_positive_response_code_for_configuration(uint16_t info)
{
    data_send[0] = recvFrameDiag.data[0] + 0x40;
    data_send[1] = recvFrameDiag.data[1];
    data_send[2] = recvFrameDiag.data[2];
    data_send[3] = (uint8_t) ((info & 0x0000FF00) >> 8);
    data_send[4] = (uint8_t) info;
    return (can_uds_send_frame(data_send, 5));
}
