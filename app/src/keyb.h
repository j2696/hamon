#pragma once
/**
 * @file   keyb.h
 * @author Abel Tarragó
 * @date   January 2022
 * @brief  Public APIs and resources for KEYB module.
 */

/* header of project specific types
---------------------------------------------------------------------------*/
#include "config.h"

/* project specific defines
---------------------------------------------------------------------------*/
#define BUTTON_ID_OFFSET     1U

/* project specific types
---------------------------------------------------------------------------*/
/**
 * @brief
 */
typedef enum num_of_buttons_t
{
    KEYB_1 = 0,
    KEYB_2,
    KEYB_3,
    KEYB_4,
    KEYB_5,
    KEYB_6,
    KEYB_7,
    KEYB_8,
    KEYB_MAX
} num_of_buttons;

typedef enum WiperCtrlSel_str_t
{
    LeftWiperButton,
    RearWiperButton,
    RightWiperButton,
    AutoButton,
    ParkPosButton,
    LeftPumpButton,
    RearPumpButton,
    RightPumpButton,
    WiperCtrlSelMax,
} WiperCtrlSel_str;

typedef enum button_status_t
{
    BUTTON_RELEASED = 0,
    BUTTON_PRESSED,
    BUTTON_LONG_PRESSED,
    BUTTON_UNDETERMINED,
    BUTTON_FAILURE,
    BUTTON_STATUS_MAX = 5
} button_status;

typedef enum AutoWipingActive_str_t
{
    No = 0b00,
    Left_wiper = 0b01,
    Right_wiper = 0b10,
    Both = 0b11,
} AutoWipingActive_str;

typedef enum WiperCV_X_str_t
{
    Off = 0b00, // off pumps, default park
    Intermittence = 0b01, // on pumps, alternative park
    HighSpeed = 0b10,
    Error = 0xFE,
    NotAvailable = 0xFF,
    WiperCV_XMax,
} WiperCV_X_str;

typedef enum ParkPosSelected_str_t
{
	Park_top = 0b00,
	Park_bottom = 0b01,
} ParkPosSelected_str;

typedef enum WiperXOutput_str_t
{
    WiperXOutput_Wash = 0b100,
    WiperXOutput_Park = 0b010,
    WiperXOutput_High = 0b001,
} WiperXOutput_str;


/* Global functions declaration
---------------------------------------------------------------------------*/
void keyb_init(void);
void keyb_step(void);
RETStatus keyb_get(const num_of_buttons keyb_channel, uint8_t *status);
void keyb_set_data(uint8_t *data);
void keyb_get_voltage(const num_of_buttons keyb_channel, int *voltage);
void set_WiperCtrlSel(const num_of_buttons keyb_channel);
WiperCV_X_str keyb_get_wipercv_x(WiperCtrlSel_str button);
void keyb_hotKey(uint8_t val1_left, uint8_t val2_right);
