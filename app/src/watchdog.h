#pragma once
/**
 * @file   watchdog.h
 * @author Rubén Guijarro
 * @date   April 2022
 * @brief  Public APIs and resources for Watchdog module.
 */

/* header of project specific types
---------------------------------------------------------------------------*/
#include "config.h"

/* Global functions declaration
---------------------------------------------------------------------------*/
void wdt_init(void);
void wdt_step(void);
void wdext_step(void);
void wdt_loop_swreset(bool bEnCommandTroughRamMem);
void wdt_loop_lpms(void);
void wdt_loop_lpms_sleep_message(void);
