#pragma once
/**
 * @file   gpio.h
 * @author Rubén Guijarro
 * @date   January 2022
 * @brief  Public APIs and resources for GPIO module.
 */

/* header of project specific types
---------------------------------------------------------------------------*/
#include "config.h"

/* project specific types
---------------------------------------------------------------------------*/
/**
 * @brief list of control gpio ids
 * These should also be declared on the dts overlay the device name list
 * should follow the same order in the @ref u_ctrlgpio_devnames array
 * 
 * IDs in this enums are arbitrary but names in @ref u_ctrlgpio_devnames 
 * should exactly match those in the dts overlay.
 */
typedef enum E_ctrlgpio_id_t
{
    OUT_MUX_INH,
    WD_OUT,
    OUT_HSD1,
    OUT_EN1,
    OUT_HSD2,
    OUT_EN2,
    OUT_HSD3,
    OUT_EN3,
    OUT_EEPROM_WC,
    OUT_CSN,
    OUT_IDLE,
    OUT_CAN_STBY,
    OUT_SPI_CLK,
    OUT_LED_OE,
    OUT_LED_LE,
    OUT_MUX_A,
    OUT_MUX_B,
    OUT_MUX_C,
    OUT_MUX_D,
    E_CTRLGPIO_ID_MAX,
} E_ctrlgpio_id;

/**
 * @brief list of multiplexor control ids
 * should follow the same order in schematic file
 */
typedef enum E_mux_id_t
{
    MUX_KEYB_1 = 0,
    MUX_KEYB_2,
    MUX_KEYB_3,
    MUX_KEYB_4,
    MUX_KEYB_5,
    MUX_KEYB_6,
    MUX_KEYB_7,
    MUX_KEYB_8,
    E_MUX_ID_MAX,
} E_mux_id;

/* Global functions declaration
---------------------------------------------------------------------------*/
void gpio_init(void);
void gpio_step(void);
RETStatus gpio_set(const E_ctrlgpio_id gpio_n, const int value);
RETStatus gpio_get(const E_ctrlgpio_id gpio_n, int *value);
RETStatus gpio_toggle_output(const E_ctrlgpio_id gpio_n);
void gpio_set_multiplexor(const E_mux_id mux_sel);
void gpio_switch_spi_pb3_to_gpio_output(void);
void gpio_switch_pwm_pb4_to_gpio_output(void);
void gpio_switch_gpio_output_to_spi_pb3(void);
void gpio_switch_gpio_output_to_pwm_pb4(void);

