#pragma once
/**
 * @file   can_uds.h
 * @author Rubén Guijarro
 * @date   April 2022
 * @brief  Public APIs and resources for CAN UDS module.
 */

/* header of project specific types
---------------------------------------------------------------------------*/
#include "config.h"

/* constant definitions
---------------------------------------------------------------------------*/
#define CANID_Id_Diagnostics_Resp          (0x0CDAF956)

#define CANID_Id_Diagnostics_Req_F6        (0x0CDA56F6)
#define CANID_Id_Diagnostics_Req_F7        (0x0CDA56F7)
#define CANID_Id_Diagnostics_Req_F8        (0x0CDA56F8)
#define CANID_Id_Diagnostics_Req_F9        (0x0CDA56F9)
#define CANID_Id_Diagnostics_Req_FA        (0x0CDA56FA)
#define CANID_Id_Diagnostics_Req_FB        (0x0CDA56FB)
#define CANID_Id_Diagnostics_Req_FC        (0x0CDA56FC)
#define CANID_Id_Diagnostics_Req_FD        (0x0CDA56FD)
/* project specific types
---------------------------------------------------------------------------*/
/** @brief UDS CAN frame parameters */
typedef struct uds_can_frame_info_t
{
	uint8_t  data[UDS_RX_MAX_ABSOLUTE_LENGTH];
	uint8_t  length;
}uds_can_frame_info;
/** @brief CAN frame structure. This is mainly
 * used by Socket CAN code.
 * @details Used to pass CAN messages from userspace to the socket CAN and vice
 * versa */
typedef struct buffer_rx_uds_canframes_t
{
	uint8_t depth;
	uint8_t index;
    bool messageProcessed;
	uds_can_frame_info buffer[UDS_RX_MAX_ABSOLUTE_LENGTH];
}buffer_rx_uds_canframes;

/* Global functions declaration
---------------------------------------------------------------------------*/
void can_uds_init(void);
RETStatus can_uds_send_frame(const uint8_t *data, const uint16_t dlc);
