/**
 * @file   adc.c
 * @author Abel Tarragó
 * @date   January 2022
 * @brief  ADC module body
 */

/* header of project specific types
---------------------------------------------------------------------------*/
#include "util.h"
#include "gpio.h"
#include "adc.h"
#include "debounce.h"
#include "assert.h"

/* header of standard C - libraries
---------------------------------------------------------------------------*/
#include <drivers/adc.h>

/* constant definitions
---------------------------------------------------------------------------*/
#define ADC_MUX__OVERLAY_IDX         0
#define ADC_VBATT_AI__OVERLAY_IDX      1
#define ADC_CSN_1__OVERLAY_IDX       2
#define ADC_CSN_2__OVERLAY_IDX       3
#define ADC_CSN_3__OVERLAY_IDX       4
/* Common settings supported by most ADCs */
#define ADC_RESOLUTION		         12
#define ADC_GAIN		             ADC_GAIN_1
#define ADC_REFERENCE		         ADC_REF_INTERNAL
#define ADC_ACQUISITION_TIME	     ADC_ACQ_TIME_DEFAULT
#define FSM_ADCSTEP_STAT_MAX         FSM_ADCSTEP_MMUXED_KB08_ST_07 + 1U
#define ADC_NUM_MAX_LOGIC_CHANNELS   5

/* Private macro
---------------------------------------------------------------------------*/
LOG_MODULE_REGISTER(adc);

#if !DT_NODE_EXISTS(DT_PATH(zephyr_user)) || \
	!DT_NODE_HAS_PROP(DT_PATH(zephyr_user), io_channel_names)
#error "No suitable devicetree overlay specified"
#endif
#define ADC_NODE		DT_PHANDLE(DT_PATH(zephyr_user), io_channels)
#define ADC_CHANNEL_OVERLAY_ID(i_)   DT_PROP_BY_IDX(DT_PATH(zephyr_user), io_channel_ids, i_)
#define ADC_NUM_CHANNELS	DT_PROP_LEN(DT_PATH(zephyr_user), io_channel_names)
BUILD_ASSERT(ADC_NUM_CHANNELS == ADC_NUM_MAX_LOGIC_CHANNELS, "Invalid ADC channels configuration");
BUILD_ASSERT(DT_PROP_LEN(DT_PATH(zephyr_user), io_channel_ids) == ADC_NUM_CHANNELS, "Invalid ADC overlay");

/* Project specific types
---------------------------------------------------------------------------*/
typedef enum E_fsm_adcstep_t
{
    FSM_ADCSTEP_MMUXED_KB01_ST_00,       /* MUX_KEYB_1 */
    FSM_ADCSTEP_MMUXED_KB02_ST_01,       /* MUX_KEYB_2 */
    FSM_ADCSTEP_MMUXED_KB03_ST_02,       /* MUX_KEYB_3 */
    FSM_ADCSTEP_MMUXED_KB04_ST_03,       /* MUX_KEYB_4 */
    FSM_ADCSTEP_MMUXED_KB05_ST_04,       /* MUX_KEYB_5 */
    FSM_ADCSTEP_MMUXED_KB06_ST_05,       /* MUX_KEYB_6 */
    FSM_ADCSTEP_MMUXED_KB07_ST_06,       /* MUX_KEYB_7 */
    FSM_ADCSTEP_MMUXED_KB08_ST_07,       /* MUX_KEYB_8 */
} E_fsm_adcstep;

/* Local defined variables
-----------------------------------------------------------------------------*/
static int16_t sample_buffer;
static int16_t adc_sample_buffer[NUM_CHANNELS_MAX_ADC];
static int32_t adc_vref = 0;
const struct device *dev_adc;
/** @brief ordered ADC channel ids according to @ref E_adc_channel enum
 */
static uint8_t channel_ids[ADC_NUM_CHANNELS] = {
    ADC_CHANNEL_OVERLAY_ID(ADC_MUX__OVERLAY_IDX),
    ADC_CHANNEL_OVERLAY_ID(ADC_VBATT_AI__OVERLAY_IDX),
	ADC_CHANNEL_OVERLAY_ID(ADC_CSN_1__OVERLAY_IDX),
	ADC_CHANNEL_OVERLAY_ID(ADC_CSN_2__OVERLAY_IDX),
	ADC_CHANNEL_OVERLAY_ID(ADC_CSN_3__OVERLAY_IDX),
};
static const E_debounce_channel adc2debounce[] = {
    DEB_KEYB_1, DEB_KEYB_2, DEB_KEYB_3, DEB_KEYB_4, 
    DEB_KEYB_5, DEB_KEYB_6, DEB_KEYB_7, DEB_KEYB_8,
    DEB_IGN_ANALOG, DIAG_WASH_1, DIAG_WASH_2, DIAG_WASH_3
};
static const bool adc_sample_triggers_debounce[] = {
    true, true, true, true, true, true, true, true, true, true, true, true,
};

struct adc_channel_cfg channel_cfg = {
	.gain = ADC_GAIN,
	.reference = ADC_REFERENCE,
	.acquisition_time = ADC_ACQUISITION_TIME,
	/* channel ID will be overwritten below */
	.channel_id = 0,
	.differential = 0
};

struct adc_sequence sequence = {
	/* individual channels will be added below */
	.channels    = 0,
	.buffer      = &sample_buffer,
	/* buffer size in bytes, not number of samples */
	.buffer_size = sizeof(sample_buffer),
	.resolution  = ADC_RESOLUTION,
};

/* Local functions
---------------------------------------------------------------------------*/
/** @brief public function to get adc string for single channel */
char* adc_get_channel_string(const E_adc_channel adc_channel){
    switch(adc_channel){
        case MUXED_ADC_CHKEY1:
            return "MUXED_ADC_CHKEY1";
        case MUXED_ADC_CHKEY2:
            return "MUXED_ADC_CHKEY2";
        case MUXED_ADC_CHKEY3:
            return "MUXED_ADC_CHKEY3";
        case MUXED_ADC_CHKEY4:
            return "MUXED_ADC_CHKEY4";
        case MUXED_ADC_CHKEY5:
            return "MUXED_ADC_CHKEY5";
        case MUXED_ADC_CHKEY6:
            return "MUXED_ADC_CHKEY6";
        case MUXED_ADC_CHKEY7:
            return "MUXED_ADC_CHKEY7";
        case MUXED_ADC_CHKEY8:
            return "MUXED_ADC_CHKEY8";
        case ADC_VBATT_AI:
            return "ADC_VBATT_AI";
        case ADC_DIAG_WASH_LEFT:
            return "ADC_DIAG_WASH_LEFT";
        case ADC_DIAG_WASH_REAR:
            return "ADC_DIAG_WASH_REAR";
        case ADC_DIAG_WASH_RIGHT:
            return "ADC_DIAG_WASH_RIGHT";
        default:
            return "";
    }
}

/* Global functions
---------------------------------------------------------------------------*/
/** @brief Initialization function for ADC module */
void adc_init(void)
{
    MODULE_SINGLE_INIT;

	dev_adc = DEVICE_DT_GET(ADC_NODE);

	if (!device_is_ready(dev_adc)) {
		LOG_ERR("ADC device not found\n");
		return;
	}
	/* Configure ADC  */
    adc_channel_setup(dev_adc, &channel_cfg);    

	adc_vref = adc_ref_internal(dev_adc);

    gpio_set_multiplexor(MUX_KEYB_1);
    // k_sleep(K_MSEC(2500U));    /* time for stabilization */
}

/** @brief Main function for read ADC channels */
void adc_step(void)
{
    static E_fsm_adcstep adc_step_fsm_status = FSM_ADCSTEP_MMUXED_KB01_ST_00;

    E_mux_id muxCh = E_MUX_ID_MAX;

    if (ARRAY_SIZE(adc_sample_buffer) > ADC_NUM_CHANNELS)
    {
        sequence.channels = BIT(channel_ids[ADC_MUX_CH]);

        muxCh = (E_mux_id) adc_step_fsm_status;

        if (adc_read(dev_adc, &sequence) != 0)
            LOG_ERR("ADC reading failed with error\n");
        else {
            adc_sample_buffer[ADC_MUX_CH + muxCh] = sample_buffer;
            if (adc_sample_triggers_debounce[ADC_MUX_CH + muxCh] == true)
                debounce_refresh(adc2debounce[ADC_MUX_CH + muxCh], STATUS_OK, sample_buffer);
        }

        muxCh++;
        muxCh = (muxCh == E_MUX_ID_MAX) ? MUX_KEYB_1 : muxCh;

        gpio_set_multiplexor(muxCh);
    }

    
    if (adc_step_fsm_status % 2U)
    {
        static uint8_t subST = 1U;

        E_adc_hw_channel E_ADC_ChannelA = 0;
        E_adc_hw_channel E_ADC_ChannelB = 0;
        uint8_t offset = MUXED_ADC_CHKEY8;

        if (subST) {
            E_ADC_ChannelA = ADC_VBATT_AI_CH;
            E_ADC_ChannelB = ADC_DIAG_WASH_LEFT_CH;
        }
        else {
            E_ADC_ChannelA = ADC_DIAG_WASH_RIGHT_CH;
            E_ADC_ChannelB = ADC_DIAG_WASH_REAR_CH;
        }

        sequence.channels = BIT(channel_ids[E_ADC_ChannelA]);

        if (adc_read(dev_adc, &sequence) != 0)
            LOG_ERR("ADC reading failed with error\n");
        else {
            adc_sample_buffer[E_ADC_ChannelA + offset] = sample_buffer;
            if (adc_sample_triggers_debounce[E_ADC_ChannelA + offset] == true)
                debounce_refresh(adc2debounce[E_ADC_ChannelA + offset], STATUS_OK, sample_buffer);
        }
        
        sequence.channels = BIT(channel_ids[E_ADC_ChannelB]);

        if (adc_read(dev_adc, &sequence) != 0)
            LOG_ERR("ADC reading failed with error\n");
        else {
            adc_sample_buffer[E_ADC_ChannelB + offset] = sample_buffer;
            if (adc_sample_triggers_debounce[E_ADC_ChannelB + offset] == true)
                debounce_refresh(adc2debounce[E_ADC_ChannelB + offset], STATUS_OK, sample_buffer);
        }

        LOG_DBG("ADC CH %d: %d\n", channel_ids[E_ADC_ChannelA], adc_sample_buffer[E_ADC_ChannelA + offset]);
        LOG_DBG("ADC CH %d: %d\n", channel_ids[E_ADC_ChannelB], adc_sample_buffer[E_ADC_ChannelB + offset]);

        subST = subST?0:1;
    }

    adc_step_fsm_status++;
    adc_step_fsm_status %= FSM_ADCSTEP_STAT_MAX;
}

RETStatus adc_get(const E_adc_channel adc_channel, int *value)
{
    S_ASSERT(adc_channel < NUM_CHANNELS_MAX_ADC, ASSERT_BOUNDS, "Invalid adc channel");
    S_ASSERT(value != NULL, ASSERT_BOUNDS, "Invalid pointer");

    *value = adc_sample_buffer[adc_channel];

    return STATUS_OK;
}

