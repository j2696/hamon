/**
 * @file   can_uds.h
 * @author Rubén Guijarro
 * @date   April 2022
 * @brief  Public APIs and resources for CAN UDS module.
 */

/* Header of project specific types
---------------------------------------------------------------------------*/
#include "util.h"
#include "can_claas.h"
#include "assert.h"
#include "can_uds.h"
#include "dgn.h"
#include "uds.h"
#include <zephyr.h>
#include <canbus/isotp.h>

/* Private macro
---------------------------------------------------------------------------*/
LOG_MODULE_REGISTER(can_uds);

/* Constant definitions
---------------------------------------------------------------------------*/
#define RX_THREAD_STACK_SIZE 512
#define RX_THREAD_PRIORITY 2

/* project specific types
---------------------------------------------------------------------------*/
const struct isotp_msg_id rx_addr = {
	.ext_id = CANID_Id_Diagnostics_Req_F9,
	.id_type = CAN_EXTENDED_IDENTIFIER,
	.use_ext_addr = 0,
	.use_fixed_addr = 1
};
struct isotp_msg_id tx_addr = {
	.ext_id = CANID_Id_Diagnostics_Resp,
	.id_type = CAN_EXTENDED_IDENTIFIER,
	.use_ext_addr = 0,
    .use_fixed_addr = 1
};


/* Local variables
-----------------------------------------------------------------------------*/
static struct k_thread rx_thread_data;
K_THREAD_STACK_DEFINE(rx_thread_stack, RX_THREAD_STACK_SIZE);

/* External variables
-----------------------------------------------------------------------------*/
extern const struct device *can_dev;

/* Global variables
-----------------------------------------------------------------------------*/
uds_can_frame_info recvFrameDiag;
uint32_t last_test_address = 0;

/* Local functions
---------------------------------------------------------------------------*/
void rx_thread(void *arg1, void *arg2, void *arg3)
{
	ARG_UNUSED(arg1);
	ARG_UNUSED(arg2);
	ARG_UNUSED(arg3);
    const struct isotp_fc_opts fc_opts = {.bs = 2, .stmin = 0};
    static struct isotp_recv_ctx recv_ctx;
    static struct net_buf *buf;
	static int rem_len = 0, received_len;
    static bool istop_binded = false;
    static uint8_t rx_buffer[300];
    static int ret;

    ret = isotp_bind(&recv_ctx, can_dev, &rx_addr, &tx_addr, &fc_opts, K_FOREVER);

    if (ret != ISOTP_N_OK) {
        LOG_DBG("Failed to bind to rx ID %d [%d]\n", rx_addr.std_id, ret);
        return;
    }else istop_binded = true;

	while (istop_binded) {
        received_len = 0;
		do {
			rem_len = isotp_recv_net(&recv_ctx, &buf, K_MSEC(10));
			if (rem_len < 0)
            {
				LOG_DBG("Receiving error [%d]\n", rem_len);
				break;
			}

			last_test_address = recv_ctx.rx_addr.ext_id;
            tx_addr = recv_ctx.tx_addr;
            memcpy(&rx_buffer[received_len], buf->data, buf->len);
            received_len += buf->len;
            if(rem_len == 0)
            {
                istop_binded = false;
                memcpy(recvFrameDiag.data, rx_buffer, received_len);
                recvFrameDiag.length = 1U;
                net_buf_unref(buf);
            }
		} while (rem_len);
	}
    isotp_unbind(&recv_ctx);
}

/** @brief Starte rx thread isotp */
void create_can_uds_rx_thread(void)
{
    k_tid_t tid;

	tid = k_thread_create(&rx_thread_data, rx_thread_stack,
			      K_THREAD_STACK_SIZEOF(rx_thread_stack),
			      rx_thread, NULL, NULL, NULL,
			      5, 0, K_NO_WAIT);
	if (!tid) {LOG_DBG("ERROR spawning rx thread\n");}
}

/* Global functions
---------------------------------------------------------------------------*/
/** @brief send UDS CAN frames
 * @param data - pointer to data
 * @param dlc - valid data bytes
 * @return RETStatus */
RETStatus can_uds_send_frame(const uint8_t *data, const uint16_t dlc)
{
    static struct isotp_send_ctx send_ctx;
    static uint8_t tx_data_large[150];

    memcpy(tx_data_large, data, dlc);

    int ret = isotp_send(&send_ctx, can_dev, tx_data_large, dlc, &tx_addr, &rx_addr, NULL, NULL);

    if (ret != ISOTP_N_OK) LOG_DBG("Error while sending data to ID %d [%d]\n", tx_addr.std_id, ret);

    create_can_uds_rx_thread();

    return STATUS_OK;
}

/** @brief Initialization function for CAN UDS module */
void can_uds_init(void)
{
    MODULE_SINGLE_INIT;
    create_can_uds_rx_thread();
}
