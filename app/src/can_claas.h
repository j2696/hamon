#pragma once
/**
 * @file   can_claas.h
 * @author Abel Tarragó
 * @date   April 2022
 * @brief  Public APIs and resources for CAN module.
 */

/* Header of project specific types
---------------------------------------------------------------------------*/
#include "config.h"
#include "keyb.h"

/* constant definitions
---------------------------------------------------------------------------*/
#define CAN_CYCLIC_500_MS                  0U
#define CAN_CYCLIC_1000_MS                 1U
#define RECEIVED_CANFRAMES_BFF_LEN         75U
#define UDS_RX_MAX_ABSOLUTE_LENGTH         150U

/* Rx */
#define cWcmcBrc_WiperStatus               (0x14200056)
#define cVcmcBrc_CntHours1Total            (0x1424007D)
#define cChtcBrc_ChutePositionDeg          (0x14280068)
#define cVhbtBrc_LocalizationSettings      (0x142B0032)
#define cVhbtBrc_HotkeyStatus              (0x142A0032)
#define CANID_cVhbt_mask                   (0x1FF0FF00)
#define CANID_cVhbt_id                     (0x14200000)

#define cVbccBrc_AlarmMonitoringStatus     (0xC010005)
#define cVbccWcmsAlarmAck                  (0xC075605)
#define CANID_cVbcc_mask                   (0xFF000FF)
#define CANID_cVbcc_id                     (0xC000005)

#define cVhbtWcmcWiperParam                (0x192E5632)
#define cVhbtWcmcWiperCtrl                 (0x112F5632)
#define CANID_cVhbtWcmcWiper_mask          (0x10FFFFFF)
#define CANID_cVhbtWcmcWiper_id            (0x102F5632)

#define cVuicMuicSwitch2B                  (0x105B4D4C)
#define cCoecWcmcStatusLEDs                (0x1CD956F0)
/* Tx */
#define cWcmsBrc_WcmmAlive                 (0xC000056)
#define cWcmsVbbcAlarm                     (0xC01F956)
#define cWcmcVhbtWiperParam                (0x182E3256)
#define cWcmcVhbtWiperCtrl                 (0x102F3256)


/* project specific types
---------------------------------------------------------------------------*/
/** @brief CL_RECVStatus enum
 * Define the status of the message */
typedef enum CL_RECVStatus_t {
    CL_RECV_IDLE = 0U,
    CL_RECV_PUSHING,
    CL_RECV_POPPING,
} CL_RECVStatus;

/** @brief can_rxbuffer enum
 * Define the index of the message in the register */
typedef enum E_can_rxbuffer_t
{
    E_RXBFF_OH = 0,
    E_RXBFF_DTC,
    E_RXBFF_INTERMITTENCE,
    E_RXBFF_AUTO_WIPING,
    E_RXBFF_PWM,
    E_RXBFF_WIPER_REQ,
    E_RXBFF_Acknowledgement,
    E_RXBFF_HotKey,
    E_RXBFF_Switch2B,
    E_RXBFF_cVhbt,
    E_RXBFF_LEDs,
    E_RXBFF_MAX
} E_can_rxbuffer;

/** @brief CAN frame structure. This is mainly
 * used by Socket CAN code.
 * @details Used to pass CAN messages from userspace to the socket CAN and vice
 * versa */
typedef struct can_frame_info_t
{
	uint8_t  data[8];
	uint8_t  length;
}can_frame_info;

/** @brief CAN frame structure. This is mainly
 * used by Socket CAN code.
 * @details Used to pass CAN messages from userspace to the socket CAN and vice
 * versa */
typedef struct buffer_rx_canframes_t
{
	uint8_t depth;
	uint8_t index;
    bool messageProcessed;
	can_frame_info buffer[RECEIVED_CANFRAMES_BFF_LEN];
}buffer_rx_canframes;

/* Global functions declaration
---------------------------------------------------------------------------*/
void can_claas_init(void);
void can_claas_step(uint8_t step);
void can_updateRecv(void);
bool can_requestResync_Led_blink(void);
void can_get_enginerpm(uint16_t *data);
void can_get_error_requested(uint16_t *data);
void can_send_frame(const uint32_t message_id, const uint8_t *data, const uint8_t dlc);
void can_claas_send_alive(void);
bool get_CanFrameIsReceived(void);
void set_CanFrameIsReceived(bool value);
void clearCanErrorFlag(void);
uint8_t getCanErrorFlag(void);
uint8_t getWaCanMcanSend (void);
void setWaCanMcanSend (uint8_t value);
