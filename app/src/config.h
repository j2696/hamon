#pragma once
/**
 * @file   config.h
 * @author Abel Tarragó
 * @date   March 2022
 * @brief  Public APIs and resources for CONFIG module.
 */

/* Header of standard C - libraries
---------------------------------------------------------------------------*/
#include <stdint.h>
#include <stdbool.h> 
#include <drivers/can.h>
#include <kernel.h>
#include <logging/log.h>
#include <zephyr.h>
#include <canbus/isotp.h>
#include <sys/reboot.h>
#include <sys/byteorder.h>
#include <drivers/gpio.h>
#include <drivers/pinmux.h>
#include <dt-bindings/pinctrl/stm32-pinctrl.h>
#include <pinmux/stm32/pinmux_stm32.h>
#include <device.h>
#include <drivers/i2c.h>
#include <inttypes.h>
#include <drivers/pwm.h>
#include <errno.h>
#include <string.h>
#include <drivers/spi.h>
#include <sys/atomic.h>
#include <drivers/watchdog.h>
#include "stdlib.h"

/* constant definitions
---------------------------------------------------------------------------*/
/* Software version */
#define MSB_SW_VER  0
#define MID_SW_VER  3
#define LSB_SW_VER  12

#define STATUS_OK   0
#define STATUS_ERROR    -1
#define STATUS_INTEGRITY_ERROR   -2

/* project specific types
---------------------------------------------------------------------------*/
/** @brief states of machine state for triple working hour saving to eeprom
 */
typedef enum WiperCtrlSet_str_t
{
    ReadParameter = 0x0,
	WriteParameter = 0x1,
	ResetParameter = 0x2,
	CyclicTransmissionOn = 0x7,
	CyclicTransmissionOff = 0x8,
	SignOn = 0xA,
	SignOff = 0xB,
	Ping = 0xC,
	Abortion = 0xD,
    WiperCtrlSetMax,
} WiperCtrlSet_str;

typedef int32_t RETStatus;

typedef union {
    struct {
        uint8_t Bytes[8];
    } bytes;
    struct {
        uint8_t WiperLeftOutput             :3;
        uint8_t PumpLeftOutput              :1;
        uint8_t FL_Left_01                  :1;
        uint8_t FL_Left_02                  :1;
        uint8_t                             :2;
        uint8_t WiperRearOutput             :3;
        uint8_t PumpRearOutput              :1;
        uint8_t FL_Rear_03                  :1;
        uint8_t FL_Rear_04                  :1;
        uint8_t                             :2;
        uint8_t WiperRightOutput            :3;
        uint8_t PumpRightOutput             :1;
        uint8_t FL_Right_05                 :1;
        uint8_t FL_Right_06                 :1;
        uint8_t                             :2;
        uint8_t ManLeftSelected             :2;
        uint8_t                             :1;
        uint8_t ParkPosSelected             :1;
        uint8_t FL_Auto_07                  :1;
        uint8_t FL_Auto_08                  :1;
        uint8_t AutoWipingActive            :2;
        uint8_t ManRearSelected             :2;
        uint8_t                             :2;
        uint8_t FL_Park_09                  :1;
        uint8_t FL_Park_10                  :1;
        uint8_t AutoModeSelected            :2;
        uint8_t ManRightSelected            :2;
        uint8_t                             :6;
        uint8_t IntermittencePausingTimeHigh;
        uint8_t IntermittencePausingTimeLow;
    } individual;
} STATUS;

/* Global functions declaration
---------------------------------------------------------------------------*/
void config_init(void);
void config_set_working_hour(uint8_t *data);
uint32_t config_get_working_hour(void);
uint8_t config_get_ecu_hw_pn(uint8_t byte);
uint8_t config_get_ecu_sn(uint8_t byte);
uint8_t config_get_ecu_hw_version(uint8_t byte);
uint8_t config_get_main_pn(uint8_t byte);
void config_set_main_pn(uint8_t *data);
uint16_t config_get_reset_time(void);
uint16_t config_get_max_pump_time(void);
uint16_t config_get_pause_pump_ratio(void);
uint16_t config_get_present_interval_time(void);
uint16_t config_get_min_interval_time(void);
uint16_t config_get_max_interval_time(void);
uint32_t config_get_wiper_run_time(void);
uint32_t config_get_wiper_pulse_time(void);
uint8_t config_get_c_output_enable(void);
uint16_t config_get_washer_ol(void);
uint16_t config_get_washer_scgnd(void);
uint8_t config_get_pwm_max_limit(void);
uint8_t config_get_pwm_min_limit(void);

uint8_t get_WiperLeftOutput(void);
uint8_t get_PumpLeftOutput(void);
uint8_t get_FL_Left_01(void);
uint8_t get_FL_Left_02(void);
uint8_t get_WiperRearOutput(void);
uint8_t get_PumpRearOutput(void);
uint8_t get_FL_Rear_03(void);
uint8_t get_FL_Rear_04(void);
uint8_t get_WiperRightOutput(void);
uint8_t get_PumpRightOutput(void);
uint8_t get_FL_Right_05(void);
uint8_t get_FL_Right_06(void);
uint8_t get_ManLeftSelected(void);
uint8_t get_ParkPosSelected(void);
uint8_t get_FL_Auto_07(void);
uint8_t get_FL_Auto_08(void);
uint8_t get_AutoWipingActive(void);
uint8_t get_ManRearSelected(void);
uint8_t get_FL_Park_09(void);
uint8_t get_FL_Park_10(void);
uint8_t get_ManRightSelected(void);
uint8_t get_AutoModeSelected(void);
uint32_t get_IntermittencePausingTime(void);
uint32_t get_CANIntermittencePausingTime(uint8_t id);

void set_WiperLeftOutput(uint8_t value);
void set_PumpLeftOutput(uint8_t value);
void set_FL_Left_01(uint8_t value);
void set_FL_Left_02(uint8_t value);
void set_WiperRearOutput(uint8_t value);
void set_PumpRearOutput(uint8_t value);
void set_FL_Rear_03(uint8_t value);
void set_FL_Rear_04(uint8_t value);
void set_WiperRightOutput(uint8_t value);
void set_PumpRightOutput(uint8_t value);
void set_FL_Right_05(uint8_t value);
void set_FL_Right_06(uint8_t value);
void set_ManLeftSelected(uint8_t value);
void set_ParkPosSelected(uint8_t value);
void set_FL_Auto_07(uint8_t value);
void set_FL_Auto_08(uint8_t value);
void set_AutoWipingActive(uint8_t value);
void set_ManRearSelected(uint8_t value);
void set_FL_Park_09(uint8_t value);
void set_FL_Park_10(uint8_t value);
void set_ManRightSelected(uint8_t value);
void set_AutoModeSelected(uint8_t value);
RETStatus set_IntermittencePausingTime(uint16_t value, uint8_t id);
