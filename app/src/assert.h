#pragma once
/**
 * @file   assert.h
 * @author Rubén Guijarro
 * @date   January 2022
 * @brief  Public APIs and resources for ASSERT helper.
 */

/* Header of project specific types
---------------------------------------------------------------------------*/
#include "config.h"

/* Project specific types
---------------------------------------------------------------------------*/
/** @brief assert_code enum
 * List of fatal error codes that a failed assertion could lead to
 */
typedef enum S_assert_code_t
{
    ASSERT_UNKNOWN = -1,
    ASSERT_INTERNAL = -2,
    ASSERT_RUNTIME = -3,
    ASSERT_BOUNDS = -4,
} S_assert_code;

/** @brief macro for assertion checking
 * This automatically points to function and line
 */
#define S_ASSERT(expr, code, message) S_assert(expr, __func__, __LINE__, code, message);

/* Global functions
---------------------------------------------------------------------------*/
void S_assert(bool condition, const char* func, const int line, S_assert_code assert_code, const char *message);
void S_assert_brief(const bool condition, S_assert_code assert_code);
