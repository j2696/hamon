#pragma once
/**
 * @file   util.h
 * @author Rubén Guijarro
 * @date   February 2022
 * @brief  Public APIs and resources for util helper
 */

/* header of project specific types
---------------------------------------------------------------------------*/
#include "config.h"

/* project specific types
---------------------------------------------------------------------------*/
/** @brief macro to atomically ensure a single module init
 * @todo: decide if log message is supporte, this requires invoking module
 * has log already enbled
 */
#define MODULE_SINGLE_INIT \
    do \
    { \
        static ATOMIC_DEFINE(initialized, 1) = {ATOMIC_INIT(0)}; \
        if(atomic_test_and_set_bit(&initialized[0], 0)) \
        { \
            /*LOG_INF("Already initialized");*/ \
            return; \
        } \
    }while(0)
