import numpy, math, os
import sys 
from tkfilebrowser  import askopendirname, askopenfilename, asksaveasfilename
from socketcan      import CanIsoTpSocket, CanRawSocket, CanFrame
from tkinter        import scrolledtext, messagebox, IntVar, DoubleVar, Button, Radiobutton, Spinbox, Label, Menu, Tk, INSERT, ttk, Toplevel, Text, Entry, Canvas, font
from tkinter import *
from tkinter.ttk    import Progressbar
from pathlib        import Path
from time           import sleep

Tkroot = Tk()

Tkroot.title("Test")

Tkroot.geometry('500x500')
fontsize = font.Font( size = 8 )
nb = ttk.Notebook(Tkroot)
nb.pack(fill='both',expand='yes')

p1 = ttk.Frame(nb)
p2 = ttk.Frame(nb)
p3 = ttk.Frame(nb)
p4 = ttk.Frame(nb)

nb.add(p1, text='CAN CLAAS')
nb.add(p3, text='CAN CLAAS')
nb.add(p2, text='CAN UDS')

interface = "can0"
try:
    s1 = CanRawSocket(interface=interface)
except:
    print("CAN not connected")
    label = Label(Tkroot) 
    sel = "CAN not connected" 
    label.config(text = sel)  
    label.pack(anchor=CENTER)
    #sys.exit()

try:    
    s = CanIsoTpSocket(interface=interface,rx_addr=0x0CDAF956,tx_addr=0x0CDA56F9)
except:
    print("CAN not connected")
    label = Label(Tkroot) 
    sel = "CAN not connected" 
    label.config(text = sel)  
    label.pack(anchor=CENTER)
    #sys.exit()

#CAN ID
can_id_param= 0x0CDA56F9
can_id_leds = 0x113c4c4d
can_id_switch = 0x112F5632
can_id_bl = 0x142B0032
can_id_auto_wiping = 0x14280068
can_id_send_pausing = 0x192E5632
can_id_working_hour = 0x1424007D
can_id_alive = 0x0C000056
can_id_receive_pausing = 0x182E3256
can_id_leds_eol = 0x1CD956F0
can_id_hotkey = 0x142A0032
can_id_switch2B = 0x105B4D4C
can_id_alarm =  0xC010005
can_id_ack =  0xC075605

extended=0x03
default=0x01

OFF=0x00
ON=0x01
BLINKSLOW=0x11
BLINKFAST=0x21
GREEN=0x40
RED=0x41
ORANGE=0x42
WHITE=0x43
    
LED1=0x4a
LED2=0xFF
LED3=0x71
LED4=0x70
LED5=0x6F
LED6=0x6E
LED7=0x6B
LED8=0x6a
LED9=0x75
LED10=0x6c
LED11=0x76
LED12=0x6D

PASSWORD_MAXI = 0xE189ABE1


# ---------------- EXTENDED SESSION ---------------- #
def diagnostic_session(session):
    try:
        diag = [0x10,session]
        s.send(bytes(diag))
        frame = s.recv()
        print("ext session")
        if frame[0] != 0x50:
            print("FAILED EXTENDED SESSION")
            sys.exit()
    except:
        print("CAN not connected")
# ---------------- SECURITy ACCES ---------------- #
def security_acces():
        PASSWORD = 0xEF253970
        SHIFT = 16
        REQ = 0x01
        KEY = 0x02
        diagnostic_session(extended)
        sleep(0.2)
        sec_acc=[0x27,REQ]
        s.send(bytes(sec_acc))
        frame = s.recv()
        if frame[0] == 0x67:
            seed = int((frame[2] << 24) | (frame[3] << 16) | (frame[4] << 8) | (frame[5]))
            my_claas_key = ((((seed ^ PASSWORD) << SHIFT)) + (seed ^ PASSWORD) + PASSWORD);
            key_1 = ((0xff000000&my_claas_key) >> 24)
            key_2 = ((0x00ff0000&my_claas_key) >> 16)
            key_3 = ((0x0000ff00&my_claas_key) >> 8)
            key_4 = (0x000000ff&my_claas_key)
            key = [0x27, KEY, key_1, key_2, key_3, key_4]
            sleep(0.2)
            s.send(bytes(key))
            frame2 = s.recv()
        if frame2[0] == 0x67 or frame2[1] == 0x0C:
            print("SECURITY ACCES ENTERED")
        else:
            print("ERROR SEND KEY")

# ---------------- ERASE ---------------- #
def erase():
    diag = [0xAB,0x00]
    s.send(bytes(diag))
    sleep(2)
    print("EEPROM erased")

# ---------------- WRITE BY ID ---------------- #
def write_data_by_id(data, signal,datalength_expected):
    s.send(bytes(data))
    #frame = s.recv()
    '''if frame[0] == 0x6E:
        print("WRITING OK", signal,": {0}".format(" ".join(["{0:02X}".format(b) for b in data[3:] ])))
        #print("RESPO ", signal,": {0}".format(" ".join(["{0:02X}".format(b) for b in frame[3:] ])))      
    else:
        print("ERROR WRITING",signal, ": {:X}".format(frame[0]))
    sleep(0.10)'''

# ---------------- READ BY ID- ---------------- #
def read_data_by_id(data,data_expected,signal,datalength_expected):
    time_sleep = 0.02
    retries = 10
    s.send(bytes(data))
    frame=s.recv()  
    n=0
    if datalength_expected == 1:
        while n<retries:
            n+=1
            sleep(time_sleep)
            s.send(bytes(data))
            frame=s.recv()
            if (frame[0] == 0x62) and (frame[1] == data[1])  and (frame[2] == data[2]):
                data_recevied =  hex(frame[3])
                value_expected = hex(data_expected[0])
                if data_recevied != value_expected:
                    print("WRONG ", signal,"({0:X}{1:X}): {2}".format(data[1], data[2]," ".join(["{0:02X}".format(b) for b in frame[3:]])))       
                    break;
                else:
                    print("READ ", signal,"({0:X}{1:X}): {2}".format(data[1], data[2]," ".join(["{0:02X}".format(b) for b in frame[3:]])))
                    break;
            if n == 499:
                print("ERROR ", signal,"({0:X}{1:X}): {2}".format(data[1], data[2]," ".join(["{0:02X}".format(b) for b in frame[3:]])))  
    elif datalength_expected==2:
        while n<retries:
            n+=1
            sleep(time_sleep)
            s.send(bytes(data))
            frame=s.recv()            
            if (frame[0] == 0x62) and (frame[1] == data[1])  and (frame[2] == data[2]):
                data_recevied =  hex((frame[3] << 8) | (frame[4]))
                value_expected = hex((data_expected[0] << 8) | (data_expected[1]))
                if data_recevied != value_expected:
                    print("WRONG ", signal,"({0:X}{1:X}): {2}".format(data[1], data[2]," ".join(["{0:02X}".format(b) for b in frame[3:]])))       
                    break;
                else:
                    print("READ ", signal,"({0:X}{1:X}): {2}".format(data[1], data[2]," ".join(["{0:02X}".format(b) for b in frame[3:]])))
                    break;
            if n == 499:
                print("ERROR ", signal,"({0:X}{1:X}): {2}".format(data[1], data[2]," ".join(["{0:02X}".format(b) for b in frame[3:]])))  
    elif datalength_expected == 3:
        while n<retries:
            n+=1
            sleep(time_sleep)
            s.send(bytes(data))
            frame=s.recv()
            if (frame[0] == 0x62) and (frame[1] == data[1])  and (frame[2] == data[2]):
                data_recevied =  hex((frame[3] << 16) | (frame[4] << 8) | (frame[5]))
                value_expected = hex((data_expected[0] << 16) | (data_expected[1] << 8) | (data_expected[2]))
                if data_recevied != value_expected:
                    print("WRONG ", signal,"({0:X}{1:X}): {2}".format(data[1], data[2]," ".join(["{0:02X}".format(b) for b in frame[3:]])))       
                    break;
                else:
                    print("READ ", signal,"({0:X}{1:X}): {2}".format(data[1], data[2]," ".join(["{0:02X}".format(b) for b in frame[3:]])))
                    break;
            if n == 499:
                print("ERROR ", signal,"({0:X}{1:X}): {2}".format(data[1], data[2]," ".join(["{0:02X}".format(b) for b in frame[3:]])))  
    elif datalength_expected==4:
        while n<retries:
            n+=1
            sleep(time_sleep)
            s.send(bytes(data))
            frame=s.recv()
            if (frame[0] == 0x62) and (frame[1] == data[1])  and (frame[2] == data[2]):
                data_recevied =  hex((frame[3] << 24) | (frame[4] << 16) | (frame[5] << 8) | frame[6])
                value_expected = hex((data_expected[0] << 24) | (data_expected[1] << 16) | (data_expected[2] << 8) | data_expected[3])
                if data_recevied != value_expected:
                    print("WRONG ", signal,"({0:X}{1:X}): {2}".format(data[1], data[2]," ".join(["{0:02X}".format(b) for b in frame[3:]])))       
                    break;
                else:
                    print("READ ", signal,"({0:X}{1:X}): {2}".format(data[1], data[2]," ".join(["{0:02X}".format(b) for b in frame[3:]])))
                    break;
            if n == 499:
                print("ERROR ", signal,"({0:X}{1:X}): {2}".format(data[1], data[2]," ".join(["{0:02X}".format(b) for b in frame[3:]])))  
    elif datalength_expected == 6:
        while n<retries:
            n+=1
            sleep(time_sleep)
            s.send(bytes(data))
            frame=s.recv()
            if (frame[0] == 0x62) and (frame[1] == data[1])  and (frame[2] == data[2]):
                data_recevied =  hex((frame[3] << 40) | (frame[4] << 32) | (frame[5] << 24) | (frame[6] << 16) | (frame[7] << 8) | frame[8])
                value_expected = hex((data_expected[0] << 40) | (data_expected[1] << 32) | (data_expected[2] << 24) | (data_expected[3] << 16) | (data_expected[4] << 8) | data_expected[5])
                if data_recevied != value_expected:
                    print("WRONG ", signal,"({0:X}{1:X}): {2}".format(data[1], data[2]," ".join(["{0:02X}".format(b) for b in frame[3:]])))       
                    break;
                else:
                    print("READ ", signal,"({0:X}{1:X}): {2}".format(data[1], data[2]," ".join(["{0:02X}".format(b) for b in frame[3:]])))
                    break;
            if n == 499:
                print("ERROR ", signal,"({0:X}{1:X}): {2}".format(data[1], data[2]," ".join(["{0:02X}".format(b) for b in frame[3:]])))  
    elif datalength_expected == 8:
        while n<retries:
            n+=1
            sleep(time_sleep)
            s.send(bytes(data))
            frame=s.recv()
            if (frame[0] == 0x62) and (frame[1] == data[1])  and (frame[2] == data[2]):
                print("READ ", signal,"({0:X}{1:X}): {2}".format(data[1], data[2]," ".join(["{0:02X}".format(b) for b in frame[3:]])))
                break;
            if n == 499:
                print("ERROR ", signal,"({0:X}{1:X}): {2}".format(data[1], data[2]," ".join(["{0:02X}".format(b) for b in frame[3:]])))  
    elif datalength_expected == 20:
        while n<retries:
            n+=1
            sleep(time_sleep)
            s.send(bytes(data))
            sleep(time_sleep)
            if (frame[0] == 0x62) and (frame[1] == data[1])  and (frame[2] == data[2]):
                data_recevied =  hex((frame[3] << 152) | (frame[4] << 144) | (frame[5] << 136) | (frame[6] << 128) | (frame[7] << 120) | (frame[8] << 112) | (frame[9] << 104) | (frame[10] << 96) | (frame[11] << 88) | (frame[12] << 80) | (frame[13] << 72) | (frame[14] << 64) | (frame[15] << 56) | (frame[16] << 48) | (frame[17] << 40) | (frame[18] << 32) | (frame[19] << 24) | (frame[20] << 16) | (frame[21] << 8) | (frame[22]))
                value_expected = hex((data_expected[0] << 152) | (data_expected[1] << 144) | (data_expected[2] << 136) | (data_expected[3] << 128) | (data_expected[4] << 120) | (data_expected[5] << 112) | (data_expected[6] << 104) | (data_expected[7]) << 96 | (data_expected[8] << 88) | (data_expected[9] << 80) | (data_expected[10] << 72) | (data_expected[11] << 64) | (data_expected[12] << 56) | (data_expected[13] << 48) | (data_expected[14] << 8) << 40 | (data_expected[15]) << 32 | (data_expected[16] << 24) | (data_expected[17] << 16) | (data_expected[18] << 8) | (data_expected[19]))
                if data_recevied != value_expected:
                    print("WRONG ", signal,"({0:X}{1:X}): {2}".format(data[1], data[2]," ".join(["{0:02X}".format(b) for b in frame[3:]])))       
                    break;
                else:
                    print("READ ", signal,"({0:X}{1:X}): {2}".format(data[1], data[2]," ".join(["{0:02X}".format(b) for b in frame[3:]])))
                    break;
            if n == 499:
                print("ERROR ", signal,"({0:X}{1:X}): {2}".format(data[1], data[2]," ".join(["{0:02X}".format(b) for b in frame[3:]])))  
        
# ----------------READ BY ID 8 BYTES -------#
def read_data_by_id8(data,signal):
    s.send(bytes(data))
    frame=s.recv()
    n=0
    while n<retries:
        n+=1
        s.send(bytes(data))
        frame=s.recv()
        if (frame[0] == 0x62) and (frame[1] == data[1])  and (frame[2] == data[2]):
            data_recevied =  hex((frame[3] << 56) | (frame[4] << 48) | (frame[5] << 40) | (frame[6] << 32) | (frame[7] << 24) | (frame[8] << 16) | (frame[9] << 8) | frame[10])
            value_expected = hex((data[3] << 56) | (data[4] << 48) | (data[5] << 40) | (data[6] << 32) | (data[7] << 24) | (data[8] << 16) | (data[9] << 8) | data[10])
            if data_recevied != value_expected:
                print("WRONG ", signal,"({0:X}{1:X}): {2}".format(data[1], data[2]," ".join(["{0:02X}".format(b) for b in frame[3:]])))       
                break;
            else:
                print("READ ", signal,"({0:X}{1:X}): {2}".format(data[1], data[2]," ".join(["{0:02X}".format(b) for b in frame[3:]])))
                break;
        if n == 499:
            print("ERROR ", signal,"({0:X}{1:X}): {2}".format(data[1], data[2]," ".join(["{0:02X}".format(b) for b in frame[3:]])))  
        sleep(time_sleep)

# ---------------- READ BY ID (ADCs) ---------------- #
def read_data_by_id2(data, signal):
    s.send(bytes(data))
    frame=s.recv()
    n=0
    while n<retries:
        n+=1
        s.send(bytes(data))
        frame=s.recv()
        if (frame[0] == 0x62) and (frame[1] == data[1])  and (frame[2] == data[2]):
            data_recevied =  hex((frame[3] << 8) | (frame[4]))
            print(signal,"({0:X}{1:X}): {2}".format(data[1], data[2]," ".join(["{0:02X}".format(b) for b in frame[3:]])))
            hexa=hex(frame[3] << 8 |frame[4])
            decimal=int(hexa,16)
            print("Decimal = ",decimal)
            break;
        if n == 499:
            print("ERROR ", signal,"({0:X}{1:X}): {2}".format(data[1], data[2]," ".join(["{0:02X}".format(b) for b in frame[3:]])))  
            #sys.exit()
        sleep(0.05)
    sleep(0.05)
# ---------------- ECU RESET ---------------- #
def ecu_reset():
    #security_acces()
    #sleep(1)
    #diagnostic_session(extended)
    #sleep(0.1)
    rst = [0x11,0x01]
    s.send(bytes(rst))
    '''frame = s.recv()
    if frame[0] != 0x51:
        print("FAILED ECU RESET")
        sys.exit()'''
# -----------LEDS--------------------------------#
def led_frame(data):
    frame1 = CanFrame(can_id=can_id_leds, data=data)
    s1.send(frame1)

# ---- parametrization ----    
def write_slipControl():
    #security_acces()
    time_sleep = 0.1
    write_data_by_id([0x2E, 0xF1, 0x87, 0x00, 0x00, 0x00, 0x00], "ECU_MAIN_PN",3)
    sleep(time_sleep)
    write_data_by_id([0x2E, 0xF1, 0x8C, 0x00, 0x00, 0x00, 0x00], "ECU_SN",3)
    sleep(time_sleep)
    write_data_by_id([0x2E, 0xF1, 0x93, 0x00, 0x00, 0x00, 0x00], "ECU_HW_VERSION",2)
    sleep(time_sleep)
    write_data_by_id([0x2E, 0xF1, 0x91, 0x00, 0x00, 0x00, 0x00], "ECU_HW_PN",4)
    sleep(time_sleep)
    write_data_by_id([0x2E, 0x30, 0x0C, 0x01, 0xF4, 0x00, 0x00], "RESET_TIME",2)                    # 500
    sleep(time_sleep)
    write_data_by_id([0x2E, 0x30, 0x0D, 0x61, 0xA8, 0x00, 0x00], "MAX_PUMP_TIME",2)                 # 3000
    sleep(time_sleep)
    write_data_by_id([0x2E, 0x30, 0x0E, 0x02, 0x58, 0x00, 0x00], "PAUSE_PUMP_RATIO",2)              # 150
    sleep(time_sleep)
    write_data_by_id([0x2E, 0x30, 0x0F, 0x13, 0x88, 0x00, 0x00], "PRESENT_INTERVAL_TIME",2)         # 5000
    sleep(time_sleep)
    write_data_by_id([0x2E, 0x30, 0x10, 0x07, 0xD0, 0x00, 0x00], "MIN_INTERVAL_TIME",2)             # 0
    sleep(time_sleep)
    write_data_by_id([0x2E, 0x30, 0x11, 0x27, 0x10, 0x00, 0x00], "MAX_INTERVAL_TIME",2)             # 10000
    sleep(time_sleep)
    write_data_by_id([0x2E, 0x30, 0x12, 0x09, 0x60, 0x13, 0x89], "WIPER_RUN_TIME",4)                # 2400
    sleep(time_sleep)
    write_data_by_id([0x2E, 0x30, 0x13, 0x03, 0x20, 0x09, 0xC5], "WIPER_PULSE_TIME",4)              # 800
    sleep(time_sleep)
    write_data_by_id([0x2E, 0x30, 0x14, 0x00, 0x00, 0x00, 0x00], "C_OUTPUT_ENABLE",1)               # 0
    sleep(time_sleep)
    write_data_by_id([0x2E, 0x30, 0x15, 0x00, 0xFA, 0x00, 0x00], "WASHER_OL",2)                     # 250
    sleep(time_sleep)
    write_data_by_id([0x2E, 0x30, 0x16, 0x09, 0xC4, 0x00, 0x00], "WASHER_SCGND",2)                  # 2500
    sleep(time_sleep)
    write_data_by_id([0x2E, 0x30, 0x17, 0x64, 0x00, 0x00, 0x00], "PWM_MAX_PERCENTAGE",1)            # 100
    sleep(time_sleep)
    write_data_by_id([0x2E, 0x30, 0x18, 0x32, 0x00, 0x00, 0x00], "PWM_MIN_PERCENTAGE",1)             # 50
    print("WRITE FINISH")


def read_slipControl():
    time_sleep = 0.1
    read_data_by_id([0x22, 0x10, 0x11],[0x00, 0x00, 0x00, 0x00], "WORKING_HOUR",4)
    sleep(time_sleep)
    read_data_by_id([0x22, 0xF1, 0x00],[0x02, 0x20, 0x01, 0x56], "ECU_PIN",4)
    sleep(time_sleep)
    read_data_by_id([0x22, 0xF1, 0x01],[0xC0, 0x6C, 0x84, 0x33], "ECU_CHECKSUM",4)
    sleep(time_sleep)
    read_data_by_id([0x22, 0xF1, 0x03],[0x02, 0x00], "ECU_FBL_QUESTION",2)
    sleep(time_sleep)
    read_data_by_id([0x22, 0xF1, 0x05],[0x00, 0x88, 0x49], "ECU_SC",3)
    sleep(time_sleep)
    read_data_by_id([0x22, 0xF1, 0x80],[0x02, 0x00], "FBL_SW_VERSION",2)
    sleep(time_sleep)
    read_data_by_id([0x22, 0xF1, 0x81],[0x00, 0x00, 0x03, 0x08], "ECU_SOFTWARE",4)
    sleep(time_sleep)    
    read_data_by_id([0x22, 0xF1, 0x87, 0x00, 0x00, 0x00, 0x00],[0x00,0x00,0x00,0x00], "ECU_MAIN_PN",4)
    sleep(time_sleep)
    read_data_by_id([0x22, 0xF1, 0x88],[0x26, 0xE4, 0xE0, 0x00], "ECU_SW_PN",4)
    sleep(time_sleep)
    read_data_by_id([0x22, 0xF1, 0x8C, 0x00, 0x00, 0x00, 0x00],[0x00, 0x00, 0x00, 0x00], "ECU_SN",4)
    sleep(time_sleep)
    read_data_by_id([0x22, 0xF1, 0x93, 0x00, 0x00, 0x00, 0x00],[0x00, 0x00], "ECU_HW_VERSION",2)
    sleep(time_sleep)
    read_data_by_id([0x22, 0xF1, 0x91],[0x00, 0x00, 0x00, 0x00], "ECU_HW_PN",4)
    sleep(time_sleep)
    read_data_by_id([0x22, 0x30, 0x0C, 0x00, 0x00, 0x00, 0x00], [0x01, 0xF4], "RESET_TIME",2)                   #500
    sleep(time_sleep)
    read_data_by_id([0x22, 0x30, 0x0D, 0x00, 0x00, 0x00, 0x00], [0x0b, 0xb8], "MAX_PUMP_TIME",2)                #3000
    sleep(time_sleep)
    read_data_by_id([0x22, 0x30, 0x0E, 0x00, 0x00, 0x00, 0x00], [0x00, 0x96], "PAUSE_PUMP_RATIO",2)             #150
    sleep(time_sleep)
    read_data_by_id([0x22, 0x30, 0x0F, 0x00, 0x00, 0x00, 0x00], [0x13, 0x88], "PRESENT_INTERVAL_TIME",2)        #5000
    sleep(time_sleep)
    read_data_by_id([0x22, 0x30, 0x10, 0x00, 0x00, 0x00, 0x00], [0x00, 0x00], "MIN_INTERVAL_TIME",2)            #0
    sleep(time_sleep)
    read_data_by_id([0x22, 0x30, 0x11, 0x00, 0x00, 0x00, 0x00], [0x27, 0x10], "MAX_INTERVAL_TIME",2)            #10000
    sleep(time_sleep)
    read_data_by_id([0x22, 0x30, 0x12, 0x00, 0x00, 0x00, 0x00], [0x00, 0x00, 0x09, 0x60], "WIPER_RUN_TIME",4)   #2400
    sleep(time_sleep)
    read_data_by_id([0x22, 0x30, 0x13, 0x00, 0x00, 0x00, 0x00], [0x00, 0x00, 0x03, 0x20], "WIPER_PULSE_TIME",4) #800
    sleep(time_sleep)
    read_data_by_id([0x22, 0x30, 0x14, 0x00, 0x00, 0x00, 0x01], [0x00], "C_OUTPUT_ENABLE",1)                    #0
    sleep(time_sleep)
    read_data_by_id([0x22, 0x30, 0x15, 0x00, 0x00, 0x00, 0x00], [0x00, 0xFA], "WASHER_OL",2)                    #250
    sleep(time_sleep)
    read_data_by_id([0x22, 0x30, 0x16, 0x00, 0x00, 0x00, 0x00], [0x09, 0xC4], "WASHER_SCGND",2)                 #2500
    sleep(time_sleep)
    read_data_by_id([0x22, 0x30, 0x17, 0x00, 0x00, 0x00, 0x00], [0x64], "PWM_MAX_PERCENTAGE",1)                 #100
    sleep(time_sleep)
    read_data_by_id([0x22, 0x30, 0x18, 0x00, 0x00, 0x00, 0x00], [0x32], "PWM_MIN_PERCENTAGE",1)                 #50
    print(" READ FINISH " )
        

# ---- ADCs ----
def adcs():        
    #new window
    adcsWindow = Toplevel(Tkroot)
    adcsWindow.title("ADCs")
    adcsWindow.geometry("270x350")
    
    #place buttons
    key1Button = "KEY1"
    key_1 = Button(adcsWindow, text = key1Button, command = key1 ).place \
    ( x = 20,   y = 20,  width = 50,  height = 30 )
    key2Button = "KEY2"
    key_2 = Button(adcsWindow, text = key2Button, command = key2 ).place \
    ( x = 20,   y = 60,  width = 50,  height = 30 )
    key3Button = "KEY3"
    key_3 = Button(adcsWindow, text = key3Button, command = key3 ).place \
    ( x = 20,   y = 100,  width = 50,  height = 30 )
    key4Button = "KEY4"
    key_4 = Button(adcsWindow, text = key4Button, command = key4 ).place \
    ( x = 20,   y = 140,  width = 50,  height = 30 )
    key5Button = "KEY5"
    key_5 = Button(adcsWindow, text = key5Button, command = key5 ).place \
    ( x = 20,   y = 180,  width = 50,  height = 30 )
    key6Button = "KEY6"
    key_6 = Button(adcsWindow, text = key6Button, command = key6 ).place \
    ( x = 20,   y = 220,  width = 50,  height = 30 )
    key7Button = "KEY7"
    key_7 = Button(adcsWindow, text = key7Button, command = key7 ).place \
    ( x = 20,   y = 260,  width = 50,  height = 30 )
    key8Button = "KEY8"
    key_8 = Button(adcsWindow, text = key8Button, command = key8 ).place \
    ( x = 20,   y = 300,  width = 50,  height = 30 )
    ignButton = "IGN_AI"
    ign_ai = Button(adcsWindow, text = ignButton, command = ign ).place \
    ( x = 140,   y = 20,  width = 50,  height = 30 )

    wash_leftButton = "WASH_LFET_DIAG"
    wash_left_adc = Button(adcsWindow, text = wash_leftButton, command = wash_left ).place \
    ( x = 140,   y = 60,  width = 50,  height = 30 )

    wash_rearButton = "WASH_REAR_DIAG"
    wash_rear_adc = Button(adcsWindow, text = wash_rearButton, command = wash_rear ).place \
    ( x = 140,   y = 100,  width = 50,  height = 30 )

    wash_rightButton = "WASH_RIGHT_DIAG"
    wash_right_adc = Button(adcsWindow, text = wash_rightButton, command = wash_right ).place \
    ( x = 140,   y = 140,  width = 50,  height = 30 )

def testerpresent():
        data = bytes([0x02,0x3E,0x00, 0x00, 0x00, 0x00, 0x00])
        frame1 = CanFrame(can_id=can_id_param, data=data)
        s1.send(frame1)
        frame = s1.recv()
        sleep(0.2)
    
def key1():
    data = [0x22, 0x30, 0x00, 0xff, 0xff, 0x00, 0x00] #parking
    s.send(bytes(data))
    sleep(0.5)

def key2():
    data = [0x22, 0x30, 0x01, 0xff, 0xff, 0x00, 0x00] #auto
    s.send(bytes(data))
    sleep(0.5)

def key3():
    data = [0x22, 0x30, 0x02, 0xff, 0xff, 0x00, 0x00] #leftpump
    s.send(bytes(data))
    sleep(0.5)


def key4():
    data = [0x22, 0x30, 0x03, 0xff, 0xff, 0x00, 0x00]
    s.send(bytes(data))
    sleep(0.5)

    
def key5():
    data = [0x22, 0x30, 0x04, 0xff, 0xff, 0x00, 0x00]
    s.send(bytes(data))
    sleep(0.5)


def key6():
    data = [0x22, 0x30, 0x05, 0xff, 0xff, 0x00, 0x00]
    s.send(bytes(data))
    sleep(0.5)
    
def key7():
    data = [0x22, 0x30, 0x06, 0xff, 0xff, 0x00, 0x00]
    s.send(bytes(data))
    sleep(0.5)

def key8():
    data = [0x22, 0x30, 0x07, 0xff, 0xff, 0x00, 0x00]
    s.send(bytes(data))
    sleep(0.5)

def ign():
    data = [0x22, 0x30, 0x08, 0xff, 0xff, 0x00, 0x00]
    s.send(bytes(data))
    sleep(0.5)

def wash_left():
    data = [0x22, 0x30, 0x09, 0xff, 0xff, 0x00, 0x00]
    s.send(bytes(data))
    sleep(0.5)
    
def wash_rear():
    data = [0x22, 0x30, 0x0A, 0xff, 0xff, 0x00, 0x00]
    s.send(bytes(data))
    sleep(0.5)

def wash_right():
    data = [0x22, 0x30, 0x0B, 0xff, 0xff, 0x00, 0x00]
    s.send(bytes(data))
    sleep(0.5)

def read_switch(switch,can_id,data):
    frame1 = CanFrame(can_id=can_id, data=data)
    s1.send(frame1)

def static_vars(**kwargs):
    def decorate(func):
        for k in kwargs:
            setattr(func, k, kwargs[k])
        return func
    return decorate

@static_vars(counter_switch1=0)
def cnt_switch1():
    cnt_switch1.counter_switch1 += 1
    if cnt_switch1.counter_switch1 == 2:
        cnt_switch1.counter_switch1 = 0
   #print "Counter is %d" % foo.counter
@static_vars(counter_switch2=0)
def cnt_switch2():
    cnt_switch2.counter_switch2 += 1
    if cnt_switch2.counter_switch2 == 2:
        cnt_switch2.counter_switch2 = 0
   #print "Counter is %d" % foo.counter
@static_vars(counter_switch3=0)
def cnt_switch3():
    cnt_switch3.counter_switch3 += 1
    if cnt_switch3.counter_switch3 == 2:
        cnt_switch3.counter_switch3 = 0
   #print "Counter is %d" % foo.counter
@static_vars(counter_switch4=0)
def cnt_switch4():
    cnt_switch4.counter_switch4 += 1
    if cnt_switch4.counter_switch4 == 2:
        cnt_switch4.counter_switch4 = 0
   #print "Counter is %d" % foo.counter
@static_vars(counter_switch5=0)
def cnt_switch5():
    cnt_switch5.counter_switch5 += 1
    if cnt_switch5.counter_switch5 == 3:
        cnt_switch5.counter_switch5 = 0
   #print "Counter is %d" % foo.counter
@static_vars(counter_switch6=0)
def cnt_switch6():
    cnt_switch6.counter_switch6 += 1
    if cnt_switch6.counter_switch6 == 3:
        cnt_switch6.counter_switch6 = 0
   #print "Counter is %d" % foo.counter
@static_vars(counter_switch7=0)
def cnt_switch7():
    cnt_switch7.counter_switch7 += 1
    if cnt_switch7.counter_switch7 == 3:
        cnt_switch7.counter_switch7 = 0
   #print "Counter is %d" % foo.counter
@static_vars(counter_switch8=0)
def cnt_switch8():
    cnt_switch8.counter_switch8 += 1
    if cnt_switch8.counter_switch8 == 3:
        cnt_switch8.counter_switch8 = 0
   #print "Counter is %d" % foo.counter


def switch1():
    #print("park")
    cnt_switch1();
    read_switch("switch1",can_id_switch,bytes([0x05,0x10,cnt_switch1.counter_switch1]))
        
def switch2():
    #print("switch2")
    cnt_switch2();
    read_switch("switch2",can_id_switch,bytes([0x06,0x10,cnt_switch2.counter_switch2]))
    
def switch3():
    #print("switch3")
    cnt_switch3();
    read_switch("switch3",can_id_switch,bytes([0x07,0x10,cnt_switch3.counter_switch3]))

def switch4():
    #print("switch4")
    cnt_switch4();
    read_switch("switch4",can_id_switch,bytes([0x08,0x10,cnt_switch4.counter_switch4]))
    
def switch5():
    #print("switch5")
    cnt_switch5();
    read_switch("switch5",can_id_switch,bytes([0x04,0x10,cnt_switch5.counter_switch5]))

def switch6():
    #print("switch6")
    cnt_switch6();
    read_switch("switch6",can_id_switch,bytes([0x01,0x10,cnt_switch6.counter_switch6]))
    
def switch7():
    #print("switch7")
    cnt_switch7();
    read_switch("switch7",can_id_switch,bytes([0x02,0x10,cnt_switch7.counter_switch7]))

def switch8():
    #print("switch8")
    cnt_switch8();
    read_switch("switch8",can_id_switch,bytes([0x03,0x10,cnt_switch8.counter_switch8]))

# ----- BACKLIGHT ----------
def backlight():
    #new window
    backlightWindow = Toplevel(Tkroot)
    backlightWindow.title("Backlight")
    backlightWindow.geometry("300x200")
    
    text=Text(backlightWindow,height=2,width=30)
    text.pack(anchor=CENTER)
    text.insert(END,"Set the backlight percentage:")
    
    
    v = DoubleVar()  
    scale = Scale(backlightWindow, variable = v, from_ = 1, to = 200,orient = HORIZONTAL)
    scale.pack(anchor=CENTER) 
    
    def backlight_frame():
        value = (int(v.get())) * 100
        value1 = ((value & 0xff00) >> 8)
        value2 = (value & 0x00ff)
        data_bl=bytes([0x00,value1,value2,0x00,0x00,0x00, 0x00, 0x00])
        framebl = CanFrame(can_id=can_id_bl, data=data_bl)
        s1.send(framebl)
    
    btnbl = Button(backlightWindow, text="OK", command=backlight_frame)  
    btnbl.pack(anchor=CENTER)  
      
    labelbl = Label(backlightWindow)  
    labelbl.pack() 
    
#--------test issue_tp --------#
time_between_two_emissions = 0.001
s2 = CanIsoTpSocket(interface=interface,rx_addr=0x0CDA4321,tx_addr=0x0CDA1234)
s3 = CanIsoTpSocket(interface=interface,rx_addr=0x0CDA1234,tx_addr=0x0CDA4321)

svar2 = IntVar() # internal variable for spinbox button
svar2.set(1)
spn_2 = Spinbox(p2, from_ = 1, to = 1000, width = 8, textvariable = svar2)
spn_2.place ( x = 140, y = 145 ) 
Label(p2, text="REP.", font = fontsize).place ( x = 260, y = 145 )
def test_issue_tp():
    mstr_info = "Confirm ISO-TP Stress Test"

    if messagebox.askokcancel('Testing in progress', mstr_info) :
            
        canframe1 = [0x3E,0x00,0x00,0x00,0x00,0x00,0x00]
        canframe2 = [0x22,0x10,0x11,0x00,0x00,0x00,0x00]
        canframe3 = [0x22,0xF1,0x81,0x00,0x00,0x00,0x00]
        canframe4 = [0x22,0xF1,0x88,0x00,0x00,0x00,0x00]
        repetitions = svar2.get()
        data1=[0x07,0xBE,0xBA,0xCA,0xFE,0x3E,0x00]
        data2=[0x07,0xDE,0xAD,0xBE,0xEF,0x3E,0x00]
        
                
        counter = 0
        while counter < repetitions :

            s.send(bytes(canframe1))
            sleep(time_between_two_emissions)
            
            s2.send(bytes(data1))
            sleep(time_between_two_emissions)
            
            s3.send(bytes(data2))
            sleep(time_between_two_emissions)
            
            s2.send(bytes(data1))
            sleep(time_between_two_emissions)
            
            s3.send(bytes(data2))
            sleep(time_between_two_emissions)
            
            data=bytes([0x00,0x00,0x00,0x00,0x00])
            framecanclaas = CanFrame(can_id=0x0ABE1234, data=data)
            s1.send(framecanclaas)
            sleep(time_between_two_emissions)
            
            data_bl=bytes([0x00,0x00,0x00,0x00,0x60,0x00,0x00,0x00])
            framebl = CanFrame(can_id=can_id_bl, data=data_bl)
            s1.send(framebl)
            sleep(time_between_two_emissions)
            
            '''s.send(bytes(canframe2))
            sleep(time_between_two_emissions)
            
            sleep(time_between_two_emissions)
            s.send(bytes(canframe3))
            
            sleep(time_between_two_emissions)
            s.send(bytes(canframe4))'''
            
            counter += 1 


# # # Buttons Type Normal ..................
textButton7 = "Write Parameters"
btn_7 = Button(p2, text = textButton7, command = write_slipControl, font = fontsize ).place \
    ( x = 20,   y = 20,  width = 100,  height = 30 )

textButton9 = "Read Parameters"
btn_9 = Button(p2, text = textButton9, command = read_slipControl, font = fontsize ).place \
    ( x = 140,   y = 20,  width = 100,  height = 30 )

textButton11 = "Reset"
btn_11 = Button(p2, text = textButton11, command = ecu_reset, font = fontsize ).place \
    ( x = 20,   y = 100,  width = 100,  height = 30 )

textButton7 = "Security"
btn_7 = Button(p2, text = textButton7, command = security_acces, font = fontsize ).place \
    ( x = 140,   y = 100,  width = 100,  height = 30 )

textButton8 = "Erase"
btn_8 = Button(p2, text = textButton8, command = erase, font = fontsize ).place \
    ( x = 260,   y = 100,  width = 100,  height = 30 )

textButton3 = "ADCs"
btn_3 = Button(p2, text = textButton3, command = adcs, font = fontsize ).place \
    ( x = 20,   y = 60,  width = 100,  height = 30 )

textButton12 = "ISO-TP test"
btn_12 = Button(p2, text = textButton12, command = test_issue_tp, font = fontsize ).place \
    ( x = 20,   y = 140,  width = 100,  height = 30 )

text = "CLEAR DTC"
svar19 = IntVar() # internal variable for spinbox button
svar19.set(0xFFFFFF)
spn_19 = Spinbox(p2, from_ = 0, to = 0xFFFFFF, width = 10, textvariable = svar19, font = fontsize)
spn_19.place ( x = 140, y = 185 )
Label(p2, text="from 0 to 0xFFFFFF",font = fontsize).place ( x = 260, y = 185 )
def clear_dtc():
    diagnostic_session(extended)
    value = svar19.get()
    value1 = ((value & 0xff0000) >> 16)
    value2 = ((value & 0x00ff00) >> 8)
    value3 = (value & 0x0000ff)
    data = [0x14, value1, value2, value3]
    sleep(1)
    s.send(bytes(data))
text= Button(p2, text = text, font = fontsize, command = clear_dtc).place(x = 20, y = 180, width = 100, height = 30)

def switches():
    data = [0x22, 0x10, 0x11, 0x00, 0x00, 0x00, 0x00]
    s.send(bytes(data))
    frame=s.recv()
    time_sleep = 0.01 
    if (frame[0] == 0x62) and (frame[1] == data[1])  and (frame[2] == data[2]):
        print("READ ", "WORKING HOUR","({0:X}{1:X}): {2}".format(data[1], data[2]," ".join(["{0:02X}".format(b) for b in frame[3:]])))
 
textButton5 = "WORK HOUR"
btn_5 = Button(p2, text = textButton5, command = switches, font = fontsize ).place \
    ( x = 140,   y = 60,  width = 100,  height = 30 )

switch1Button = "PARK"
switch_1 = Button(p1, text = switch1Button, font = fontsize, command = switch1 ).place \
( x = 20,   y = 20,  width = 100,  height = 30 )
switch2Button = "LEFT PUMP"
switch_2 = Button(p1, text = switch2Button, font = fontsize, command = switch2 ).place \
( x = 140,   y = 20,  width = 100,  height = 30 )
switch3Button = "REAR PUMP"
switch_3 = Button(p1, text = switch3Button, font = fontsize, command = switch3 ).place \
( x = 260,   y = 20,  width =100,  height = 30 )
switch4Button = "RIGHT PUMP"
switch_4 = Button(p1, text = switch4Button, font = fontsize, command = switch4 ).place \
( x = 380,   y = 20,  width = 100,  height = 30 )

switch5Button = "AUTO"
switch_5 = Button(p1, text = switch5Button, font = fontsize, command = switch5 ).place \
( x = 20,   y = 60,  width = 100,  height = 30 )
switch6Button = "LEFT WIPER"
switch_6 = Button(p1, text = switch6Button, font = fontsize, command = switch6 ).place \
( x = 140,   y = 60,  width = 100,  height = 30 )
switch7Button = "REAR WIPER"
switch_7 = Button(p1, text = switch7Button, font = fontsize, command = switch7 ).place \
( x = 260,   y = 60,  width = 100,  height = 30 )
switch8Button = "RIGHT WIPER"
switch_8 = Button(p1, text = switch8Button, font = fontsize, command = switch8 ).place \
( x = 380,   y = 60,  width = 100,  height = 30 )

text = "Backlight '%'"
svar3 = IntVar() # internal variable for spinbox button
svar3.set(50)
spn_3 = Spinbox(p1, from_ = 1, to = 100, width = 5, textvariable = svar3, font = fontsize)
spn_3.place ( x = 140, y = 120 )
Label(p1, text="from 1 to 100",font = fontsize).place ( x = 190, y = 120 )
def backlight_frame():
    value = svar3.get() * 200
    value1 = ((value & 0xff00) >> 8)
    value2 = (value & 0x00ff)
    data_bl=bytes([0,value1,value2,0x00,0x00,0x00, 0x00])
    framebl = CanFrame(can_id=can_id_bl, data=data_bl)
    s1.send(framebl)
text= Button(p1, text = text, font = fontsize, command = backlight_frame).place(x = 20, y = 120, width = 100, height = 20)

text = "Spout angle 'º'"
svar4 = IntVar() # internal variable for spinbox button
svar4.set(0)
spn_4 = Spinbox(p1, from_ = 0, to = 3, width = 5, textvariable = svar4, font = fontsize)
spn_4.place ( x = 140, y = 180 )
Label(p1, text="0 off | 1 left | 2 right | 3 both",font = fontsize).place ( x = 190, y = 180 )
def autowiping_frame():
    value = svar4.get()
    if value == 1:
        data1=bytes([0x00,0x00,0x00,0x00,0x00,0x00,0x10])
    elif value == 2:
        data1=bytes([0x00,0x00,0x00,0x00,0x00,0x00,0x20])
    elif value == 3:
        data1=bytes([0x00,0x00,0x00,0x00,0x00,0x00,0x30])
    else:
        data1=bytes([0x00,0x00,0x00,0x00,0x00,0x00,0x00])
    frame = CanFrame(can_id=can_id_auto_wiping, data=data1)
    s1.send(frame)
text= Button(p1, text = text, font = fontsize, command = autowiping_frame).place(x = 20, y = 180, width = 100, height = 20)

text = "Working hour"
svar5 = IntVar() # internal variable for spinbox button
svar5.set(100)
spn_5 = Spinbox(p1, from_ = 1, to = 1100000, width = 5, textvariable = svar5, font = fontsize)
spn_5.place ( x = 140, y = 150 )
Label(p1, text="from 1 to 1100000 'hours'",font = fontsize).place ( x = 190, y = 150 )
def working_hour_frame():
    value = svar5.get() * 3601
    value1 = ((value & 0xff000000) >> 24)
    value2 = ((value & 0x00ff0000) >> 16)
    value3 = ((value & 0x0000ff00) >> 8)
    value4 = (value & 0x000000ff)
    data1=bytes([value1,value2,value3,value4,0x00,0x00])
    frame1 = CanFrame(can_id=can_id_working_hour, data=data1)
    s1.send(frame1)
text= Button(p1, text = text, font = fontsize, command = working_hour_frame).place(x = 20, y = 150, width = 100, height = 20)

text = "Send ACK"
def ack_frame():
    data1=bytes([0x00])
    frame1 = CanFrame(can_id=can_id_ack, data=data1)
    s1.send(frame1)
text= Button(p1, text = text, font = fontsize, command = ack_frame).place(x = 20, y = 300, width = 100, height = 20)

text = "Intermittence"
Label(p1, text="WiperParamSel", font = fontsize).place ( x = 140, y = 210 )
svar6 = IntVar() # internal variable for spinbox button
svar6.set(3)
spn_6 = Spinbox(p1, from_ = 1, to = 3, width = 11, textvariable = svar6, font = fontsize)
spn_6.place ( x = 140, y = 230 )
Label(p1, text=" 1 Min \n 2 Max \n 3 Set ", font = fontsize).place ( x = 140, y = 250, width = 104 )
Label(p1, text="  WiperCtrlSet  ",font = fontsize).place ( x = 250, y = 210 )
svar7 = IntVar() # internal variable for spinbox button
svar7.set(1)
spn_7 = Spinbox(p1, from_ = 0, to = 8, width = 11, textvariable = svar7, font = fontsize)
spn_7.place ( x = 250, y = 230 )
Label(p1, text="0 Read \n 1 Write \n 2 Reset \n 7 Cyclic ON \n 8 Cyclic OFF ",font = fontsize).place ( x = 250, y = 250, width = 104 )
Label(p1, text="    WiperPV_X    ",font = fontsize).place ( x = 360, y = 210 )
svar8 = IntVar() # internal variable for spinbox button
svar8.set(5000)
spn_8 = Spinbox(p1, from_ = 0, to = 10000, width = 11, textvariable = svar8, font = fontsize)
spn_8.place ( x = 360, y = 230 )
Label(p1, text="from 0 to 10000",font = fontsize).place ( x = 360, y = 250 )
def intermittence_frame():
    WiperParamSel = (int(svar6.get()))
    WiperCtrlSet = (((int(svar7.get())) << 4) & 0x00F0)
    WiperPV_X = (int(svar8.get()))
    value1 = ((WiperPV_X & 0xff000000) >> 24)
    value2 = ((WiperPV_X & 0x00ff0000) >> 16)
    value3 = ((WiperPV_X & 0x0000ff00) >> 8)
    value4 = (WiperPV_X & 0x000000ff)
    if (WiperParamSel == 1 or WiperParamSel == 2 or WiperParamSel == 3) and (WiperCtrlSet == 0 or WiperCtrlSet == 16 or WiperCtrlSet == 32 or WiperCtrlSet == 112 or WiperCtrlSet == 128):
        data1=bytes([WiperParamSel,WiperCtrlSet,value1,value2, value3, value4])
        frame = CanFrame(can_id=can_id_send_pausing, data=data1)
        s1.send(frame)
text= Button(p1, text = text, font = fontsize, command = intermittence_frame).place(x = 20, y = 210, width = 100, height = 20)

p3_ButtonColor = '#044b7d'
Label(p3, text=" LED status EOL ", font = fontsize).place ( x = 20, y = 20, width = 250 )
text = "Set LEDs"
svar9 = IntVar()
svar9.set(0)
spn_9 = Spinbox(p3, from_ = 0, to = 3, width = 11, textvariable = svar9, font = fontsize)
spn_9.place ( x = 140, y = 40 )
Label(p3, text=" LEFT WIPER ", font = fontsize).place ( x = 140, y = 60, width = 104 )
svar10 = IntVar()
svar10.set(0)
spn_10 = Spinbox(p3, from_ = 0, to = 3, width = 11, textvariable = svar10, font = fontsize)
spn_10.place ( x = 250, y = 40 )
Label(p3, text=" REAR WIPER ", font = fontsize).place ( x = 250, y = 60, width = 104 )
svar11 = IntVar()
svar11.set(0)
spn_11 = Spinbox(p3, from_ = 0, to = 3, width = 11, textvariable = svar11, font = fontsize)
spn_11.place ( x = 360, y = 40 )
Label(p3, text=" RIGHT WIPER ", font = fontsize).place ( x = 360, y = 60, width = 104 )
svar12 = IntVar()
svar12.set(0)
spn_12 = Spinbox(p3, from_ = 0, to = 3, width = 11, textvariable = svar12, font = fontsize)
spn_12.place ( x = 140, y = 80 )
Label(p3, text=" AUTO WIPING ", font = fontsize).place ( x = 140, y = 100, width = 104 )
svar13 = IntVar()
svar13.set(0)
spn_13 = Spinbox(p3, from_ = 0, to = 3, width = 11, textvariable = svar13, font = fontsize)
spn_13.place ( x = 250, y = 80 )
Label(p3, text=" PARK POS. ", font = fontsize).place ( x = 250, y = 100, width = 104 )
def led_frame():
    leftWiperLed = ((int(svar9.get())) << 4)
    rearWiperLed = ((int(svar10.get())) << 4)
    rigthWiperLed = ((int(svar11.get())) << 4)
    autoLed = ((int(svar12.get())) << 4)
    parkLed = ((int(svar13.get())) << 4)
    data1=bytes([leftWiperLed, rearWiperLed, rigthWiperLed, autoLed, parkLed, 0x00, 0x00])
    frame = CanFrame(can_id=can_id_leds_eol, data=data1)
    s1.send(frame)
Label(p3, text=" 0 both OFF \n 1 bottom ON \n 2 top ON \n 3 both ON ", font = fontsize).place ( x = 20, y = 60, width = 100 )
text= Button(p3, text = text, font = fontsize, command = led_frame, bg = p3_ButtonColor).place(x = 20, y = 40, width = 100, height = 20)

Label(p3, text=" Hotkey & Switch2B requests ", font = fontsize).place ( x = 20, y = 130, width = 250 )
text = "Send Hotkey"
def hotkey_frame():
    data1=bytes([0x49, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00])
    frame = CanFrame(can_id=can_id_hotkey, data=data1)
    s1.send(frame)
text= Button(p3, text = text, font = fontsize, command = hotkey_frame, bg = p3_ButtonColor).place(x = 20, y = 150, width = 100, height = 30)
text = "Send switch2B"
svar14 = IntVar()
svar14.set(0)
spn_14 = Spinbox(p3, from_ = 0, to = 1, width = 11, textvariable = svar14, font = fontsize)
spn_14.place ( x = 250, y = 150 )
Label(p3, text=" Switch 1 \n 0-Disable \n 1-Enable ", font = fontsize).place ( x = 250, y = 170, width = 104 )
svar15 = IntVar()
svar15.set(0)
spn_15 = Spinbox(p3, from_ = 0, to = 1, width = 11, textvariable = svar15, font = fontsize)
spn_15.place ( x = 360, y = 150 )
Label(p3, text=" Switch 2 \n 0-Disable \n 1-Enable ", font = fontsize).place ( x = 360, y = 170, width = 104 )
def switch2B_frame():
    switch1 = ((int(svar14.get())) << 4)
    switch2 = ((int(svar15.get())) << 4)
    data1=bytes([0x00, switch1, 0x00, switch2, 0x00, 0x00, 0x00])
    frame = CanFrame(can_id=can_id_switch2B, data=data1)
    s1.send(frame)
text= Button(p3, text = text, font = fontsize, command = switch2B_frame, bg = p3_ButtonColor).place(x = 140, y = 150, width = 100, height = 30)

Label(p3, text="Wiper Control transmission",font = fontsize).place ( x = 20, y = 225, width = 250 )
svar16 = IntVar()
svar16.set(1)
spn_16 = Spinbox(p3, from_ = 1, to = 8, width = 11, textvariable = svar16, font = fontsize)
spn_16.place ( x = 140, y = 245 )
Label(p3, text=" BUTTON ID ", font = fontsize).place ( x = 140, y = 260, width = 104 )
Label(p3, text=" 1-LeftWiper \n 2-RearWiper \n 3-RightWiper \n 4-Auto ", font = fontsize).place ( x = 245, y = 245, width = 110 )
Label(p3, text=" 5-ParkPos \n 6-LeftPump \n 7-RearPump \n 8-RightPump ", font = fontsize).place ( x = 355, y = 245, width = 110 )
svar17 = IntVar()
svar17.set(0)
spn_17 = Spinbox(p3, from_ = 0, to = 2, width = 11, textvariable = svar17, font = fontsize)
spn_17.place ( x = 140, y = 315 )
Label(p3, text=" WiperCtrlSet ", font = fontsize).place ( x = 140, y = 335, width = 104 )
Label(p3, text=" 0-Park Def./Pump off/Wiper off \n 1-Park Alt./Pump on/Wiper Int. \n 2-Wiper normal ", font = fontsize).place ( x = 245, y = 315, width = 220 )
text = "Enable Cyclic"
def wiperCtrlEn_frame():
    buttonID = (int(svar16.get()))
    data1=bytes([buttonID, 0x70, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00])
    frame = CanFrame(can_id=can_id_switch, data=data1)
    s1.send(frame)
text= Button(p3, text = text, font = fontsize, command = wiperCtrlEn_frame, bg = p3_ButtonColor).place(x = 20, y = 245, width = 100, height = 20)
text = "Disable Cyclic"
def wiperCtrlDis_frame():
    buttonID = (int(svar16.get()))
    data1=bytes([buttonID, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00])
    frame = CanFrame(can_id=can_id_switch, data=data1)
    s1.send(frame)
text= Button(p3, text = text, font = fontsize, command = wiperCtrlDis_frame, bg = p3_ButtonColor).place(x = 20, y = 270, width = 100, height = 20)
text = "Read Request"
def wiperCtrlRead_frame():
    buttonID = (int(svar16.get()))
    data1=bytes([buttonID, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00])
    frame = CanFrame(can_id=can_id_switch, data=data1)
    s1.send(frame)
text= Button(p3, text = text, font = fontsize, command = wiperCtrlRead_frame, bg = p3_ButtonColor).place(x = 20, y = 295, width = 100, height = 20)
text = "Write Request"
def wiperCtrlWrite_frame():
    buttonID = (int(svar16.get()))
    value = (int(svar17.get()))
    data1=bytes([buttonID, 0x10, value, 0x00, 0x00, 0x00, 0x00, 0x00])
    frame = CanFrame(can_id=can_id_switch, data=data1)
    s1.send(frame)
text= Button(p3, text = text, font = fontsize, command = wiperCtrlWrite_frame, bg = p3_ButtonColor).place(x = 20, y = 320, width = 100, height = 20)

p1_ButtonColor = '#044b7d'
Label(p1, text=" DTC ENABLE ", font = fontsize).place ( x = 20, y = 350, width = 250 )
text = "Set alarm"
svar20 = IntVar()
svar20.set(0)
spn_20 = Spinbox(p1, from_ = 0, to = 1, width = 11, textvariable = svar20, font = fontsize)
spn_20.place ( x = 140, y = 370 )
Label(p1, text="  Alarm status ", font = fontsize).place ( x = 140, y = 390, width = 104 )
svar18 = IntVar()
svar18.set(0)
spn_18 = Spinbox(p1, from_ = 0, to = 1, width = 11, textvariable = svar18, font = fontsize)
spn_18.place ( x = 250, y = 370 )
Label(p1, text="  Acknowledge ", font = fontsize).place ( x = 250, y = 390, width = 104 )
def alarm_frame():
    alarm_status = ((int(svar20.get())))
    AckUsage = ((int(svar18.get())))
    if (AckUsage == 0):
        data1=bytes([alarm_status])
    elif (AckUsage == 1):
        data1=bytes([alarm_status, 0x00, 0xAB, 0xED, 0x21, 0x45, 0x00])
    frame = CanFrame(can_id=can_id_alarm, data=data1)
    s1.send(frame)
Label(p1, text=" 0 Inactive \n 1 active", font = fontsize).place ( x = 20, y = 390, width = 100 )
text= Button(p1, text = text, font = fontsize, command = alarm_frame, bg = p1_ButtonColor).place(x = 20, y = 370, width = 100, height = 20)

#. MAIN LOOP .
while 1:
    sleep(0.01)
    Tkroot.update_idletasks()
    Tkroot.update()
    '''frame2 = s1.recv()
    if(frame2.can_id == can_id_alive):
        Label(p1, text=frame2.data[0],font = fontsize).place ( x = 360, y = 120 )
        Label(p1, text=frame2.data[5],font = fontsize).place ( x = 400, y = 120 )
        Label(p1, text=frame2.data[6],font = fontsize).place ( x = 420, y = 120 )
        Label(p1, text=frame2.data[7],font = fontsize).place ( x = 440, y = 120 )
    if(frame2.can_id == can_id_receive_pausing):
        Label(p1, text=frame2.data[0], font = fontsize).place ( x = 140, y = 400 )
        Label(p1, text=frame2.data[1], font = fontsize).place ( x = 250, y = 400 )
        value1 = (frame2.data[2] << 24)
        value1 = value1 + (frame2.data[3] << 16)
        value1 = value1 +(frame2.data[4] << 8)
        value1 = value1 + frame2.data[5]
        Label(p1, text=value1, font = fontsize).place ( x = 360, y = 400 )'''


