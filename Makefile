# description: elf images generator & target flash updater
#--------------------------------------------------------#
.PHONY = all flash_all flash_bootloader flash_app clean compile glueimages


all:
	@ echo make flash_app / make flash_bootloader / make flash_all / make clean / make compile / make glueimages

flash_all:
	@openocd -f openocd.cfg -c init -c targets -c 'reset halt' -c 'flash write_image erase build/zephyr/full.bin 0x08000000' -c 'reset halt' -c 'verify_image build/zephyr/full.bin 0x08000000' -c 'reset run' -c shutdown

flash_bootloader:
	@openocd -f openocd.cfg -c init -c targets -c 'reset halt' -c 'flash write_image erase bl/BootLoader.bin 0x0800F800' \
	-c 'reset halt' -c 'verify_image bl/BootLoader.bin 0x0800F800' -c 'reset run' -c shutdown 

flash_app:
	@ openocd -f openocd.cfg -c init -c targets -c 'reset halt' -c 'flash write_image erase build/zephyr/crczephyr.bin 0x0800F800' \
	-c 'reset halt' -c 'verify_image build/zephyr/crczephyr.bin 0x0800F800' -c 'reset run' -c shutdown

clean:
	@ rm -r build

glueimages:
	@ west build -t fullbinary

compile:
	cp -v app/can_mcan.c.workarounded.txt ../zephyr/drivers/can/can_mcan.c
	@ rm -rf ./build/zephyr/crczephyr.bin
	ZEPHYR_TOOLCHAIN_VARIANT=cross-compile CROSS_COMPILE="/usr/bin/arm-none-eabi-" west build -b development app/
	@ echo 
	@ bl/appendconfigblock -i build/zephyr/zephyr.bin -o build/zephyr/crczephyr.bin -c 0x800 -R --od1018_1=0x12345678 \
	--od1018_2=0xAABBCCDD --od1018_3=0x87654321 --swversion=0xDEADBEEF
	@ chmod +777 build/zephyr/crczephyr.bin

nucleo:
	cp -v app/can_mcan.c.workarounded.txt ../zephyr/drivers/can/can_mcan.c
	@ rm -rf ./build/zephyr/crczephyr.bin
	ZEPHYR_TOOLCHAIN_VARIANT=cross-compile CROSS_COMPILE="/usr/bin/arm-none-eabi-" west build -b nucleo_g0b1re app/
	@ echo
	@ bl/appendconfigblock -i build/zephyr/zephyr.bin -o build/zephyr/crczephyr.bin -c 0x800 -R --od1018_1=0x12345678 \
	--od1018_2=0xAABBCCDD --od1018_3=0x87654321 --swversion=0xDEADBEEF
	@ chmod +777 build/zephyr/crczephyr.bin

development:
	cp -v app/can_mcan.c.workarounded.txt ../zephyr/drivers/can/can_mcan.c
	@ rm -rf ./build/zephyr/crczephyr.bin
	ZEPHYR_TOOLCHAIN_VARIANT=cross-compile CROSS_COMPILE="/usr/bin/arm-none-eabi-" west build -b development app/
	@ echo
	@ bl/appendconfigblock -i build/zephyr/zephyr.bin -o build/zephyr/crczephyr.bin -c 0x800 -R --od1018_1=0x12345678 \
	--od1018_2=0xAABBCCDD --od1018_3=0x87654321 --swversion=0xDEADBEEF
	@ chmod +777 build/zephyr/crczephyr.bin
